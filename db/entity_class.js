class Entity {
  constructor(uid, params, creatorNetUID){
    let e = this;
    e.uid = uid;
    e.name = params.name;
    e.nets = {};
    e.nets[creatorNetUID] = true;
    if (params.open) e.open = true;

    e.x = Math.round(params.x);
    e.y = Math.round(params.y);
    e.z = Math.round(params.z);
    e.dx = Math.round(params.dx);
    e.dy = Math.round(params.dy);
    e.dz = Math.round(params.dz);

    /*
    // initialize new entity objects
    e.container = document.createElement("div");
    e.atomPane = document.createElement("div");
    e.atomBox = document.createElement("div");
    e.bondBox = document.createElement("div");
    e.entityHeader = document.createElement("div");

    e.container.style.left = `${e.x || 0}px`;
    e.container.style.top = `${e.y || 0}px`;
    e.container.style.zIndex = e.z;

    let remoteClass = uid[0]===_ ? "" : " REMOTE";
    e.container.setAttribute("class", "entity" + remoteClass);
    e.atomPane.setAttribute("class", "atomPane");
    e.atomBox.setAttribute("class", "atomBox");
    e.bondBox.setAttribute("class", "bondBox");
    e.entityHeader.setAttribute("contentdraggable", "true");

    e.atomPane.onmousedown = function(ev){
      setActiveAtom(undefined);
      if (activeEntityUID !== this.e.uid){
        setActiveEntity(this.e.uid);
        store(this.e, {z:this.e.z}, "Entity.atomPane.onmousedown");
      }
      if (ev.shiftKey){
        nets._.x = this.e.x;
        nets._.y = this.e.y;
        selection = this.e;
        selection.bonding = true;
      }
    }

    e.entityHeader.ondblclick = function(){ toggleEntity(this.e.uid); }
    e.entityHeader.onmousedown = function(ev){
      selection = this.e;
      if (ev.shiftKey){
        nets._.x = this.e.x;
        nets._.y = this.e.y;
        selection.bonding = true;
      }
      selection.offsetX = ev.offsetX;
      selection.offsetY = ev.offsetY;
      selection.atomPane.style.transition = "";
      document.onmousemove = function(ev){
        selection.moved = true;

        if (selection.bonding){
          if (!selection.target) selection.target = nets._;
          // console.log(selection.target);

          drawLink(selection, selection.target);
          nets._.x = ev.clientX/zoom-20 - screenOffsetX;
          nets._.y = ev.clientY/zoom-26 - screenOffsetY;
        }
        else{
          e.x = ev.clientX/zoom - e.offsetX - screenOffsetX - e.x1 + e.entityPaddingX;
          e.y = ev.clientY/zoom - e.offsetY - screenOffsetY - e.y1 + e.entityPaddingY;
          moveEntity(selection);
        }
      }
    }
    e.entityHeader.onmouseup = function(){
      if (this.e.moved) {
        store(this.e, {x:this.e.x, y:this.e.y}, "Entity.entityHeader.onmouseup");
      }
    }

    e.container.onclick = function(e){
      // if (e.target.tagName !== "SPAN" || e.target.getAttribute("class") === "entityHeader"){
      if (activeEntityUID !== this.e.uid && e.target === this){
        setActiveEntity(this.e.uid);
        store(this.e, {z:this.e.z}, "Entity.container.onclick");
      }
    }

    e.container.onmouseover = function(){
      // if (this.e===selection) return;
      if (selection.constructor.name==="Net" || selection.constructor.name==="Entity"){
        drawLink(selection, nets._, true); // erase
        selection.target = this.e;
      }
    }
    e.container.onmouseout = function(){
      // if (this.e===selection) return;
      if (selection.target===this.e){
        drawLink(selection, this.e, true); // erase
        selection.target = undefined;
      }
    }

    e.container.id = e.name;
    e.atomPane.id = `${e.name}.atomPane`;
    e.atomBox.id = `${e.name}.atomBox`;
    e.bondBox.id = `${e.name}.bondBox`;

    entityBox.append(e.container);
    e.container.append(e.atomPane);
    e.atomPane.append(e.entityHeader);

    e.dot = document.createElement("span");
    e.dot.setAttribute("class", "redDot" + e.open ? " greenDot" : "");
    e.dot.onmouseup = function(){ toggleEntity(this.e.uid) }
    e.dot.innerHTML = " ⬤ ";

    e.label = document.createElement("span");
    if (e.name !== "core"){
      e.label.setAttribute("class", "entityLabel");
      e.label.setAttribute("contenteditable", true);
      e.label.onmousedown = function(){ this.moved = false; }
      e.label.onmouseup = function(){
        if (!this.moved) {
          this.focus();
          this.selectAll(true);
        }
        this.moved = false;
      }
      e.label.onmousemove = function(){ this.moved = true; }
      e.label.selectAll = function(mode){
        window.setTimeout(function(el, mode) {
          var sel, range;
          range = document.createRange();
          range.selectNodeContents(el);
          sel = window.getSelection();
          sel.removeAllRanges();
          if(mode == true) sel.addRange(range);
        }, 10, this, mode);
      }
      e.label.onkeydown = function(ev){
        // abort rename if a navigation shortcut was triggered
        if (ev.altKey && shortCutKeys.altKey[ev.code]){
          this.selectAll(false);
          return false;
        }
        // commit the rename on enter key
        if (ev.key == "Enter"){
          this.blur();
          return false;
        }
      }
      e.label.onblur = function(){
        this.selectAll(false)
        renameEntity(this.e, this.innerText);
      }
    }

    e.entityHeader.setAttribute("class", "entityHeader closedHead");
    e.dot.setAttribute("class", "redDot");
    e.label.innerHTML = e.name;

    e.entityHeader.append(e.dot);
    e.entityHeader.append(e.label);
    e.container.append(e.atomBox);
    e.container.append(e.bondBox);

    e.label.e = e;
    e.dot.e = e;
    e.container.e = e;
    e.entityHeader.e = e;
    e.atomPane.e = e;
    */
    setTimeout(function(e){
      checkEntityBounds(e);
    }, 0, this);
  }
}

const fs = {
  readdirSync,
  isdirSync,
  mkdirSync,

  existsSync, // file
  readFileSync,
  writeFileSync,

  // readdir,
  // isdir,
  // mkdir,
  //
  // exists, // file
  readFile,
  writeFile

}

/*async function getfile() {
  async function createFeature(name) {
  if (!features[name] && tree[name]) {
    //RETRIEVE INDEX HTML
    const utf8Decoder = new TextDecoder('utf-8');
    const response = await fetch(tree[name].index);
    const reader = response.body.getReader();
    let { value: html, done: readerDone } = await reader.read();
    html = html ? utf8Decoder.decode(html).split('\n') : [];
    const f = features[name] = features[name] || document.createElement('div');
}*/

function readdirSync(d){
  if(window.debug) debug.report.fsLog = 'readdirSync';
  var xhttp = new XMLHttpRequest();
  xhttp.open("GET", `db/readdir.php?d=${d}`, false); xhttp.send();
  dirList = JSON.parse(xhttp.responseText);
  return Object.values(dirList);
}

function isdirSync(d){
  if(window.debug) debug.report.fsLog = 'isdirSync';
  var xhttp = new XMLHttpRequest();
  xhttp.open("GET", `db/isdir.php?d=${d}`, false); xhttp.send();
  return (xhttp.responseText?true:false);
}

function mkdirSync(d){
  if(window.debug) debug.report.fsLog = 'mkdirSync';
  var xhttp = new XMLHttpRequest();
  xhttp.open("GET", `db/mkdir.php?d=${d}`, false); xhttp.send();
  return (xhttp.responseText);
}

function existsSync(f){
  if(window.debug) debug.report.fsLog = 'existsSync';
  var xhttp = new XMLHttpRequest();
  xhttp.open("GET", `db/exists.php?f=${f}`, false); xhttp.send();
  return (xhttp.responseText?true:false);
}

function readFileSync(f){
  if(window.debug) debug.report.fsLog = 'readFileSync';
  var xhttp = new XMLHttpRequest();
  xhttp.open("GET", `db/readfile.php?f=${f}`, false); xhttp.send();
  return (xhttp.responseText);
}

function writeFileSync(f, data){
  if(window.debug) debug.report.fsLog = 'writeFileSync';
  var xhttp = new XMLHttpRequest();
  xhttp.open("POST", `./db/writefile.php`, false);
  xhttp.setRequestHeader("Content-Type", "application/json");
  xhttp.send( JSON.stringify({f, data}) );
  return (xhttp.responseText);
}

function readFile(f, callback){
  if(window.debug) debug.report.fsLog = 'readFile';
  var xhttp = new XMLHttpRequest();
  xhttp.open("GET", `./db/readfile.php?f=${f}`, true);
  xhttp.onreadystatechange = function() {
    if (this.readyState == 4 && this.status == 200){
      callback( {file:f , result:xhttp.responseText} );
    }
  }
  xhttp.send();
}

function writeFile(f, data, callback){
  if(window.debug) debug.report.fsLog = 'writeFile: '+f;
  var xhttp = new XMLHttpRequest();
  xhttp.open("POST", `./db/writefile.php`, true);
  xhttp.setRequestHeader("Content-Type", "application/json");
  xhttp.onreadystatechange = function() {
    if (this.readyState == 4 && this.status == 200){
      callback( {file:f , result:xhttp.responseText} );
    }
  }
  xhttp.send( JSON.stringify({f, data}) );
}

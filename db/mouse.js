window.mouse = {}

document.onmousemove = function(e) {
  mouse.x = 2 * (e.clientX-window.innerWidth/2) / window.innerWidth;
  mouse.y = -2 * (e.clientY-window.innerHeight/2) / window.innerHeight;
  mouse.moved = true;
  mouse.click = false;
}

document.onmousedown = function(e) {
  mouse.x = 2 * (e.clientX-window.innerWidth/2) / window.innerWidth;
  mouse.y = -2 * (e.clientY-window.innerHeight/2) / window.innerHeight;
  mouse.dragOffsetX = mouse.x;
  mouse.dragOffsetY = mouse.y;
  mouse.buttons = e.buttons;
  mouse.moved = false;
}
document.oncontextmenu = function(e){ return false; }

document.onmouseup = function(e) {
  mouse.click = true;
  mouse.buttons = e.buttons;
  mouse.panning = false;
  if (mouse.touching!="panning") {
    a = mouse.touching;
    if (a) if (a.constructor.name==='Atom') store(a, {x:a.x, y:a.y, z:a.z});
  }
  mouse.touching = undefined;
  // document.body.style.cursor = 'default';
}

document.onmousewheel = function(e) {
  //e: event
  // camera.translateY(-e.deltaY/2016);
  // camera.rotateY(-e.deltaX/1008);
  // localStorage.cameraY = camera.position.y;
  // return;

  if (e.deltaY>0) camera.translateZ(+0.03); // relative coord space
  if (e.deltaY<0) camera.translateZ(-0.03);
  localStorage.cameraZ=camera.position.z;
}

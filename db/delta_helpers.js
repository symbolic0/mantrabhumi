const GP2020 = 1583755037550;  // Gaura Purnima Mar 9 2020 17:27:17 IST

// similar merge routines are getting pretty redundantly implimented (with and without nominus)
// it'll be nice if this can be the single place to call on. supply two objects, get one back
function mergeDeltas(type, a, b){
  // console.log("merging:", {type, a, b});
  // handle minus entities and atoms in here!

  if (!a) a = {};
  if (!b) b = {};

  if (type==="net"){  }
  if (type==="entity") {  }

  return mergeDeep(a, b);
}

function nominus(uid){ return `${uid[0]==="-" ? uid.substr(1) : uid}` }

function calculateDepth(created, modified, now){
  //
  //    created        modified     now
  //       |..............|..........|  // the oldest depth is within recent history,
  //       012 4    8        16         // so we want all deltas < log2(16)
  //
  //---------------------------------------------------------------------------------------
  //
  //    created           modified  now
  //       |....................|....|  // the oldest depth is already merged,
  //       012 4    8        16         // so we need to transform the graph:
  //
  //  (created+16)
  //    merged  modified    now
  //       | . . . | . . . . |          // the now oldest depth is within recent history again,
  //       0 1 2   4       8            // so we want all deltas < log2(8):
  //
  let mergePoint = created;
  let failsafe = 40;
  let deepest = 0;
  while (failsafe){
    failsafe--;
    let age = Math.floor((now - mergePoint)/commitInterval);
    // console.log({created,now,age,mergePoint})
    let oldestDepth = Math.floor(Math.log2(age)); // or should it be ceil??
    let modifiedAge = Math.floor((modified - mergePoint)/commitInterval);

    // console.log({age, modifiedAge, oldestDepth});

    if (Math.pow(2, oldestDepth) > modifiedAge) { // the oldest depth is within recent history
      deepest = oldestDepth;
      break;
    }
    mergePoint = mergePoint + Math.pow(2, oldestDepth)*commitInterval;
  }
  return deepest;
}

// https://stackoverflow.com/users/4903063/jhildenbiddle
function mergeDeep(a, b) {
  const isObject = obj => obj && typeof obj === 'object';

  if (!a) a = {}; //return b;
  if (!b) b = {}; //return a;

  return reconcileMinuses(a, b).reduce((prev, obj) => {
    Object.keys(obj).forEach(key => {
      const pVal = prev[key];
      const oVal = obj[key];

      if (Array.isArray(pVal) && Array.isArray(oVal)) prev[key] = pVal.concat(...oVal);
      else if (isObject(pVal) && isObject(oVal)) prev[key] = mergeDeep(pVal, oVal);
      else prev[key] = oVal;
    });

    return prev;
  }, {});
}

function reconcileMinuses(a, b) {
  if (!a || !b) return [a, b];
  if (a.entities && b.entities)
    Object.keys(b.entities).forEach(key => {
      if (key[0]==="-"){
        if (a.entities) delete a.entities[nominus(key)];
        delete b.entities[nominus(key)];
      }
    });

  return [a, b];
}

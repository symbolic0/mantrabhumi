class Atom {
  constructor(a){
    // initialize elements and properties

    this.entityUID = a.entity || activeEntityUID;
    this.entity = entities[a.entity || activeEntityUID];

    this.id = `${a.id}`;
    this.name = a.name || `#${a.id}`;
    this.x = a.x;//Math.round(a.x);
    this.y = a.y;//Math.round(a.y);
    this.z = a.z;//Math.round(a.z);
    this.rx = a.rx;
    this.ry = a.ry;
    this.rz = a.rz;
    this.editing = a.ed; // these should be made to match
    this.inbox = [];
    this.bonds = {};
    this.code = a.code || "";
    this.text = a.txt || ""; // these should match also
    this.img = a.img || "";

    functionifyScript(this);

    // node.onmousedown

    /*
    this.node.onmousedown = function(e){
      setActiveAtom(this.atom);
      if (e.target !== this) return;
      selection = this.atom;
      selection.offsetX = e.offsetX;
      selection.offsetY = e.offsetY;
      if (e.shiftKey) selection.bonding = true;
      bringAtomToTop(selection);
      if (activeEntityUID !== this.atom.entityUID){
        setActiveEntity(selection.entityUID);
        store(entities[selection.entityUID], {z:entities[selection.entityUID].z}, "Atom.node.onmousedown");
      }
      document.onmousemove = function(e){
        selection.node.moved = true;
        selection.entity.atomPane.style.transition = "";
        if (selection.bonding) drawBond(selection, {
          x:e.clientX/zoom-20 - screenOffsetX - selection.entity.x,
          y:e.clientY/zoom-20 - screenOffsetY - selection.entity.y,
          id:"unlinked"
        }, "detect");
        else{
          moveSelection(
            selection,
            e.clientX/zoom - selection.offsetX - screenOffsetX - selection.entity.x,
            e.clientY/zoom - selection.offsetY - screenOffsetY - selection.entity.y
          );
        }
      };
    };

    // node.onmouseup

    this.node.onmouseup = function(e){
      if (selection.bonding) selection.target = this.atom;
      moveSelection(this.atom, this.atom.x, this.atom.y);
      if (e.target !== selection.node) return;
      if(this.moved){
        entities[activeEntityUID].atomPane.style.transition = `width .25s, height .25s, left .25s, top .25s`;
        store(this.atom, {x:this.atom.x, y:this.atom.y}, "Atom.node.onmouseup");
        checkEntityBounds();
        this.moved = false;
        return;
      }
    }

    // node.ondblclick

    this.node.ondblclick = function(e){
      if (e.target !== this) return;
      this.atom.editing = !this.atom.editing;
      this.atom.open.style.display = this.atom.editing ? "inline" : "none";
      this.atom[this.atom.editing?"open":"node"].append(this.atom.play);
      this.atom.play.setAttribute("class", this.atom.editing?"play":"playMini");
      setTimeout(function(t){ t.atom.text.onmousemove(); }, 0, this);
      store(this.atom, {ed:this.atom.editing}, "Atom.node.ondblclick");
      checkEntityBounds();
    }

    // text mouse

    this.text.onmousedown = function(e){
      this.style.transition = "";
      bringAtomToTop(this.atom); }
    this.text.onmousemove = this.text.onscroll = this.text.onwheel = function(){
      alignEditor(this.atom);
      setTimeout(function(a){ alignEditor(a); }, 100, this.atom);
      if(optionKey || activeAtom !== this.atom) return false;
    }


    this.text.onmousedown = function(){
      this.txtW = this.offsetWidth;
      this.txtH = this.offsetHeight;
    }

    this.text.onmouseup = function(){
      let keys = {};
      let paddingW = 10 * 2; // get it from stylesheet?
      let paddingH = 10 * 2;
      if (this.txtW!==this.offsetWidth) keys.txtW = this.offsetWidth-paddingW;
      if (this.txtH!==this.offsetHeight) keys.txtH = this.offsetHeight-paddingH;
      if (Object.keys(keys).length){
        store(this.atom, keys, "Atom.text.onmouseup");
        setTimeout(function(e){ checkEntityBounds(e) }, 0, this.atom.entity);
        // checkEntityBounds();
      }
    }

    // text keys

    this.text.onkeydown = function(e){
//       console.log(['0','-','='].indexOf(e.key), e.key);// return false;
      if (e.altKey && ['º','–','≠'].indexOf(e.key) != -1) return false;
      if (!e.keyShortcutsComplete) keyShortcuts(e, this);
      setTimeout(function(t){
          t.atom.syntax.innerHTML = highlight(t.value, {a:t.selectionStart, b:t.selectionEnd});
          t.atom.lineNumbers.innerHTML = numberList(t.value);
        }, 0, this
      );
      this.atom.text.onmousemove();
      moveSelection(this.atom, this.atom.x, this.atom.y);
    }

    this.text.onkeyup = function(ev){
//       console.log(['0','-','='].indexOf(ev.key), ev.key);// return false;
      if (ev.altKey && ['º','–','≠'].indexOf(ev.key) != -1) return false;
//       if (ev.altKey) return false; // naive fix for option 0,-,= bug
      if (this.value === this.previousValue) return;
      this.previousValue = this.value;
      functionifyScript(this.atom);
      updateBonds(this.atom);
      moveSelection(this.atom, this.atom.x, this.atom.y);
      setTimeout(function(t){ t.atom.text.onmousemove(); }, 0, this);
      store(this.atom, {txt:this.value}, "Atom.text.onkeyup");
    };

    // play

    this.play.onclick = function(){
      this.setAttribute("class", (this.atom.editing?"play":"playMini") + " playFlash");
      setTimeout(function(el){ el.setAttribute("class", el.atom.editing?"play":"playMini") }, 100, this);
      if (activeEntityUID !== this.atom.entityUID){
        setActiveEntity(this.atom.entityUID);
        store(entities[this.atom.entityUID], {z:entities[this.atom.entityUID].z}, "Atom.play.onclick");
      }
      // updateBonds(this.atom);
      send(this.atom.inbox.shift() || null, this.atom.id, this.atom.entityName);
      inboxTimer = setTimeout(checkInboxes, 0);
    }

    // finalize and append to DOM

    this.lineNumbers.innerHTML = numberList(a.txt);
    setTimeout(function(T){ T.syntax.innerHTML = highlight(T.text.value); }, 250, this);

    this.entity.atomBox.append(this.node);
    this.node.append(this.label);
    this.node.append(this.open);
    this.node.append(this.play);
    this.open.append(this.text);
    this.open.append(this.lineNumbers);
    this.open.append(this.syntax);
    this[this.editing?"open":"node"].append(this.play);
    this.text.onmousemove();
    */

    // bringAtomToTop(this, a.z);
    moveSelection(this, a.x, a.y, a.z);

    // console.log(a.z);
    return this;
  }
}

function getNewEntityUID(){
  let i = 0;
  let testUID = `_.${i}.entity`;
  while(true){
    if(!fs.existsSync(`entities/${testUID}`)){ if (!entities[testUID]) return testUID; }
    else if(!fs.isdirSync(`entities/${testUID}`) && !entities[testUID]) return testUID;
    // else if(!fs.lstatSync(`entities/${testUID}`).isDirectory() && !entities[testUID]) return testUID;
    testUID = `_.${++i}.entity`;
  }
}

function getNewEntityName(){
  let eList = {};
  for (var uid in nets._.entities) eList[nets._.entities[uid].name] = true;
  let i = 0;
  let eName = `untitled${i}`;
  while (eList[eName]) eName = `untitled${++i}`;
  return eName;
}

async function toggleEntity(entityUID){
  // close entity
  if (entities[entityUID].open){
    entities[entityUID].entityHeader.setAttribute("class", "entityHeader closedHead");
    entities[entityUID].dot.setAttribute("class", "redDot");
    // console.log("Closing entity:", entityName);
    clearTimeout(inboxTimer);
    closeEntity(entityUID);
  }

  // open entity
  else{
    entities[entityUID].open = true;
    entities[entityUID].x -= entities[entityUID].dx;
    entities[entityUID].y -= entities[entityUID].dy;
    entities[entityUID].z -= entities[entityUID].dz;

    // entities[entityUID].container.style.left = `${entities[entityUID].x}px`;
    // entities[entityUID].container.style.top = `${entities[entityUID].y}px`;

    await openEntity(entityUID);
    store(entities[entityUID], {
      open:true,
      x:entities[entityUID].x,
      y:entities[entityUID].y,
      z:entities[entityUID].z
    }, "toggleEntity : open");
  }
}

async function openEntity(entityUID){
  let e = entities[entityUID];
  e.open = true;
  // e.entityHeader.setAttribute("class", "entityHeader");
  atoms[entityUID] = {};

  setActiveEntity(entityUID);

  // e.atomPane.style.transition = `width .25s, height .25s`;

  let entityConstructor = await getEntityConstructorFromFile(entityUID);
  let atomConstructors = entityConstructor.atoms;
  if (!entityDeltas[entityUID]) entityDeltas[entityUID] = [entityConstructor];
  if (!atomDeltas[entityUID]) atomDeltas[entityUID] = [atomConstructors];


  for (var ac in atomConstructors){
    atomConstructor = atomConstructors[ac];
    atoms[entityUID][atomConstructor.id] = new Atom(atomConstructor);
  }

  for (var id in atoms[entityUID]) updateBonds(atoms[entityUID][id]);
  for (var id in atoms[entityUID]){
    let a = atoms[entityUID][id];
    // bringAtomToTop(a, a.z);
    moveSelection(a, a.x, a.y, a.z);
  }

  try{
    send("init", "init", entityUID);
    checkInboxes();
  }
  catch(e){}

  checkEntityBounds(e);
}

function closeEntity(entityUID){
  setActiveEntity(undefined);

  // normalize coordinates
  let entityName = entities[entityUID].name;
  let dx = entities[entityUID].dx = 1*entities[entityUID].x1 - atomPanePadding + (entityName.length + 3) * 4;
  let dy = entities[entityUID].dy = 1*entities[entityUID].y1 - atomPanePadding + 11;

  entities[entityUID].x = 1*entities[entityUID].x + dx;
  entities[entityUID].y = 1*entities[entityUID].y + dy;
  // actually changing every atom with normalized coordinates would be destructive
  // and would interfere with aligning coded and non-coded atom positions!

  // save normalized coordinates
  entities[entityUID].atomPane.style.transition = `width .25s, height .25s`;
  entities[entityUID].container.style.left = `${entities[entityUID].x}px`;
  entities[entityUID].container.style.top = `${entities[entityUID].y}px`;
  entities[entityUID].bonds = {};
  // entities = Object.keys(entities);
  // localStorage.entities = entities.join('\n');
  entities[entityUID].open = false;
  entities[entityUID].atomBox.innerHTML = entities[entityUID].bondBox.innerHTML = "";

  store(entities[entityUID], {open:false}, "closeEntity");
  // atoms[entityUID] = {}; // there, problem with saveEntity solved temporarily
  checkEntityBounds(entities[entityUID]);
}

function moveEntity(e, smooth){
  if (smooth) e.container.style.transition = "left .25s, top .25s";
  else e.container.style.transition = "";
  // e.container.style.left = `${e.x}px`;
  // e.container.style.top = `${e.y}px`;
  updateLinks(e);
}

function renameEntity(e, newName){
  if (newName === e.name) return;
  if (!newName){
    e.label.innerText = e.name;
    return;
  }
  e.name = newName;
  store(e, {name:e.name}, "renameEntity");
}

function moveEntityFolder(n, nn){
  try{ fileSystemAccessToken.renameSync(`entities/${n}`, `entities/${nn}`) }
  catch(er){ console.warn(er) }
}

function removeEntity(e){
  if (!e) return
  delete entities[e.uid];
  delete atoms[e.uid];
  entityBox.removeChild(e.container);
  // remove links
}

function setActiveEntity(entityUID){
  // set the dots colors and zIndex
  // for (var i in entities){
  //   entities[i].entityHeader.firstElementChild.setAttribute("class", "redDot"
  //     + (i === entityUID && entities[i].open ? " greenDot" : "")); }

  if (entityUID === activeEntityUID) return;
  if (!entityUID) setActiveAtom(undefined);
  let previousActiveEntity = entities[activeEntityUID];
  activeEntityUID = undefined; updateLinks(previousActiveEntity);

  if (entityUID){
    activeEntityUID = localStorage.activeEntityUID = entityUID;
    bringEntityToTop(entityUID);
    updateLinks(entities[entityUID]);
    checkInboxes();
  }
}

function bringEntityToTop(entityUID){
  // maybe only set localStorage when it's a user interaction.
  // entities[entityUID].container.style.zIndex = ++topEntityIndex;
  entities[entityUID].z = topEntityIndex;
  // store(entities[entityUID], {z:entities[entityUID].z});
}

function updateEntityBounds(a){
  let w = 40;
  let h = 0;
  let e = entities[a.entityUID || activeEntityUID];

  if (!a.blank){
    w = 1;//a.node.offsetWidth + (a.text.offsetWidth ? a.text.offsetWidth+40 : 0) -4;
    h = 1;//Math.max(a.node.offsetHeight, a.open.offsetTop + a.text.offsetTop + a.text.offsetHeight);
    // hasAtoms -20
  } else {
    e.x1 = e.x2 =
    e.y1 = e.y2 = undefined;
  }

  let e_pX = e.entityPaddingX = a.entityPaddingX || atomPanePadding;
  let e_pY = e.entityPaddingY = a.entityPaddingY || atomPanePadding;

  e.x1 = Math.min(a.x, e.x1 || 99999);
  e.y1 = Math.min(a.y, e.y1 || 99999);
  e.x2 = Math.max(a.x+w, e.x2 || -99999);
  e.y2 = Math.max(a.y+h, e.y2 || -99999);
  // e.atomPane.style.left = `${e.x1 - e_pX}px`;
  // e.atomPane.style.top = `${e.y1 - e_pY}px`;
  // e.atomPane.style.width = `${e.x2 - e.x1 + e_pX*2}px`;
  // e.atomPane.style.height = `${e.y2 - e.y1 + e_pY*1.8}px`;

  if (isNaN(e.x1)) e.x1 = 0;
  if (isNaN(e.x2)) e.x2 = 0;
  if (isNaN(e.y1)) e.y1 = 0;
  if (isNaN(e.y2)) e.y2 = 0;

  updateLinks(e);
}

function checkEntityBounds(e){
  if(!e){
    if (!activeEntityUID) return;
    if (!entities[activeEntityUID]) return;
    e = entities[activeEntityUID];
  }

  let atomKeys = [];
  if (atoms[e.uid]) atomKeys = Object.keys(atoms[e.uid]);
  let ts = .25;
  if (e.open){
    if (atomKeys.length===0)
      updateEntityBounds({
        x:0, y:0, z:0,
        blank:true,
        entityPaddingX:Math.max((e.name.length + 3) * 4, atomPanePadding),
        entityPaddingY:atomPanePadding,
        entityUID: e.uid
      });
    else{
      updateEntityBounds({
        x:undefined, y:undefined,
        blank:true,
        // entityPaddingX:Math.max((e.name.length + 3) * 4, atomPanePadding + 20),
        // entityPaddingY:atomPanePadding + 20,
        entityUID: e.uid
      });
    }
    for (var i in atoms[e.uid]){
      a = atoms[e.uid][i];
      moveSelection(a, a.x, a.y, a.z);
    }
  }
  else updateEntityBounds({
    x:0, y:0, z:0,
    blank:true,
    entityPaddingX:0,//(e.name.length + 3) * 4,
    entityPaddingY:11,
    entityUID: e.uid
  });

  for(let t = 0; t<=270; t+=2){
    setTimeout(function(e){ updateLinks(e) }, t, e);
  }
}

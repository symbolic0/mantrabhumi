const commitInterval = 1000; // milliseconds
const commitDelay = 250; // milliseconds
let lastCommit = 0;
let storeCommitTimer = 0;
let internalCommitTimer = false;

function commitDeltas(force){
  // PROCESS THE logarithmic INTERVAL COMMIT QUEUE
  // it's only triggered by new deltas... I guess the point was if you're
  // making many changes consecutively, you don't want to call this repeatedly.
  // but you do want to call it immediately when transitioning from idle to any change.
  // so instead we use a timer that limits its frequency to 1 second between calls.
  // we use another timer when store calls it to prevent it from firing untill after 250 ms of rest

  if (!force) return;
  // only allow commits when forced (via manual button for now)
  // it's because it has such an adverse impact on the VR experience since these
  // are not local DB commits at this point , but rather PHP calls over the router
  // TODO : young commits should go to local DB .
  // older commits can be synced elsewhere and the DB can flush them thereafter

  let now = Date.now();
  if (now - lastCommit < commitInterval && !force){
    // now + X = lastCommit + commitInterval
    // X = lastCommit + commitInterval - now
    if (!internalCommitTimer)
      internalCommitTimer = setTimeout(commitDeltas, lastCommit + commitInterval - now);

    return;
  }
  clearTimeout(internalCommitTimer);
  internalCommitTimer = false;
  lastCommit = now;
  // console.log("committing deltas", now);

  // commit depth.Deltas to file
  // ----------------------------------------------------------------
  // merge entity keys into net keys
  for (eUID in entityDeltas){
    if (entityDeltas[nominus(eUID)][1]){
      for (nUID in entities[nominus(eUID)].nets){
        if (!netDeltas[nUID]) netDeltas[nUID] = [{}];
        if (!netDeltas[nUID][1]) netDeltas[nUID][1] = {entities:{}};

        // if (nominus(eUID) !== eUID) console.log({ n:netDeltas[nUID][1].entities, e:entityDeltas[eUID][1] });
        netDeltas[nUID][1].entities = mergeDeep(netDeltas[nUID][1].entities, {[eUID]:entityDeltas[eUID][1]});
        // { ...netDeltas[nUID][1].entities[eUID], ...entityDeltas[eUID][1] };
        // console.log(stringify(netDeltas[nUID][1]));
      }
    }
  }

  // console.log("netDeltas:",JSON.stringify(netDeltas,null,2));
  // console.log("------------------------------------");
  // console.log("entityDeltas:",JSON.stringify(entityDeltas,null,2));

  // now commit net keys (including ones with merged entity keys)
  for (nUID in netDeltas){
    if (netDeltas[nUID][1]){
      // await
      cascadeCommit("net", nUID);
    }
  }

  // now commit entity keys (including ones with merged atom keys)
  for (eUID in entityDeltas){
    let hasAtoms = false;
    if (atomDeltas[eUID]) if(atomDeltas[eUID][1]) hasAtoms = true;
    if (entityDeltas[eUID][1] || hasAtoms){
      // await
      cascadeCommit("entity", eUID);
    }
  }

  // using setInterval could result in two commit routines overlapping on long file write lists
  // setTimeout(await commitDeltas, commitInterval);
}

async function store(object, keys, explicitCaller){
  // let keys = { ...keysSource }; // avoid overwriting important objects
  // return;
  let objType = undefined;
  if (object.constructor) objType = object.constructor.name; // "Net", "Entity", or "Atom"
  else return;

  // can change this for zoom > 1 if we switch to transform translation instead of (left,top).
  // but gonna go ahead and round positions for now.
  // if (keys.x) keys.x = Math.round(keys.x);
  // if (keys.y) keys.y = Math.round(keys.y);
  // if (keys.z) keys.z = Math.round(keys.z);
  // if (keys.dx) keys.dx = Math.round(keys.dx);
  // if (keys.dy) keys.dy = Math.round(keys.dy);
  // if (keys.dz) keys.dz = Math.round(keys.dz);

  if (objType==="Net"){
    if (!netDeltas[object.uid]) netDeltas[object.uid] = [{}];
    if (!netDeltas[object.uid][1]) netDeltas[object.uid][1] = {};
    netDeltas[object.uid][1] = mergeDeep( netDeltas[object.uid][1], keys );

  }
  if (objType==="Entity"){
    if (!entityDeltas[object.uid]) entityDeltas[object.uid] = [{}];
    if (!entityDeltas[object.uid][1]) entityDeltas[object.uid][1] = {};
    entityDeltas[object.uid][1] = mergeDeep( entityDeltas[object.uid][1], keys );
  }
  if (objType==="Atom"){
    if (!atomDeltas[object.entityUID]) atomDeltas[object.entityUID] = {};
    if (!atomDeltas[object.entityUID][1]) atomDeltas[object.entityUID][1] = {};
    atomDeltas[object.entityUID][1][object.id] = mergeDeep( atomDeltas[object.entityUID][1][object.id], keys );
  }


  clearTimeout(storeCommitTimer); // this is for thread efficiency, not practical results
  if (Date.now() - lastCommit >= commitInterval) await commitDeltas(); // call it immediately if 1 second has elapsed
  else storeCommitTimer = setTimeout(commitDeltas, commitDelay); // otherwise call it after 250 ms of rest
  // we call commitDeltas EVERY time store is called. but commitDeltas will only execute,
  // at most, once per commitInterval (1000ms), and will not execute immediately, but only
  // after 250 ms of rest (or after 1 second elapsed since last commit, whichever comes first).
}

function saveBaseNet(netUID){
  let n = nets[netUID];
  let baseObject = {
    created: Date.now(),
    uid: n.uid,
    name: n.name,
    mode: n.mode,
    x: Math.round(n.x),
    y: Math.round(n.y),
    z: Math.round(n.z),
    entities: n.entities
  };
  let json = stringify(baseObject);
  let netPath = `nets/${netUID}`;
  if (!fs.existsSync(netPath)) fs.mkdirSync(netPath);
  fs.writeFileSync(`${netPath}/base.delta.net.json`, json);

  if (netUID!==_){
    let hashID = insertHashID(netUID);
    if (!md5Table.net[hashID]) md5Table.net[hashID] = {}; // do this for save base
    md5Table.net[hashID].base = md5(json);
  }

  // console.log("Saving Net constructor to file:", netUID);
  return baseObject;
}

function saveBaseEntity(entityUID){
  // only for saving base files / i.e. snapshots
  // ?and only when the atoms object is present?
  let e = entities[entityUID];
  let entityConstructor = {
    created: Date.now(),
    uid: e.uid,
    name: e.name,
    open: e.open,
    x: Math.round(e.x),
    y: Math.round(e.y),
    z: Math.round(e.z),
    dx: Math.round(e.dx) || 0, // because atom offsets shift the baseline coordinates
    dy: Math.round(e.dy) || 0,
    dz: Math.round(e.dz) || 0
    // atoms: generateAtomConstructors(entityUID)
  }

  // console.log("Saving atom constructors to file:", entityUID);
  let json = stringify(entityConstructor);
  let entityPath = `entities/${entityUID}`;
  if (!fs.existsSync(entityPath)){ fs.mkdirSync(entityPath); }

  fs.writeFileSync(`${entityPath}/base.delta.entity.json`, json);

  if (!e.nets[_]){
    let hashID = insertHashID(entityUID);
    if (!md5Table.entity[hashID]) md5Table.entity[hashID] = {}; // do this for save base
    md5Table.entity[hashID].base = md5(json);
  }

}

function loadAllNets(exclude){
  let q = 0;

  // free standing entities are in nets._
  loadNetFromFile(_, "store");

  if (!exclude) exclude = [];
  fs.readdirSync("nets").forEach(netUID => {
    // if (fs.lstatSync(`nets/${netUID}`).isDirectory()) {
    if (fs.isdirSync(`nets/${netUID}`)) {
      if (exclude.indexOf(netUID)===-1 && netUID!==_){
        netLoadCount++;
        setTimeout(function(netUID){
          let netConstructorObj = loadNetFromFile(netUID, "store");
        }, q+=50, netUID);
      }
    }
  });

  return q;
}

async function loadNetFromFile(netUID, mode){
  let netConstructor = [await loadDelta("net", netUID, "base")];
  let newestNetConstructor = await loadDelta("net", netUID, 0);

  if (!netConstructor[0].created) netConstructor = [{created:Date.now(), entities:{}}];
  if (!newestNetConstructor.modified) newestNetConstructor = {modified:Date.now(), entities:{}, ages:{}};

  // do this for each delta in ages.
  if (!newestNetConstructor.ages) newestNetConstructor.ages = {};
  newestNetConstructor.ages[0] = 1; // just cuz
  let ages = Object.keys(newestNetConstructor.ages).sort(function(a,b){ return Number(a)>Number(b)?-1:1; })
  for(var d in ages){
    let depth = ages[d];
    netConstructor[1] = await loadDelta("net", netUID, depth);

    // do something similar to mergeNetEntityDeltas for negative entities.
    for(eUID in netConstructor[1].entities){
      if (eUID[0]==="-"){
        delete netConstructor[0].entities[nominus(eUID)];
        delete netConstructor[1].entities[eUID];
      }else{
        netConstructor[0].entities[eUID] = { ...netConstructor[0].entities[eUID], ...netConstructor[1].entities[eUID] };
      }
    }
    delete netConstructor[1].entities;
    netConstructor[0] = { ...netConstructor[0], ...netConstructor[1] };
    // netConstructor[0] = mergeDeep( netConstructor[0], netConstructor[1] );
  }

  // console.log("getNetConstructorFromFile", netConstructor[0]);
  if (mode==="return") return netConstructor[0];
  if (mode==="store"){
    netDeltas[netUID] = [netConstructor[0]];
    nets[netUID] = new Net(netUID, netConstructor[0]);
  }
}

async function getEntityConstructorFromFile(entityUID){
  if (!entityUID) return false;

  let entityConstructor = [await loadDelta("entity", entityUID, "base")];
  let newestEntityConstructor = await loadDelta("entity", entityUID, 0);
  if (!entityConstructor[0].created) entityConstructor = [{created:Date.now(), atoms:{}}];
  if (!newestEntityConstructor.modified) newestEntityConstructor = {modified:Date.now(), atoms:{}, ages:{}};
  if (!newestEntityConstructor.ages) newestEntityConstructor.ages = {};

  // do this for each delta in ages.
  newestEntityConstructor.ages[0] = 1; // just cuz
  let ages = Object.keys(newestEntityConstructor.ages).sort(function(a,b){ return Number(a)>Number(b)?-1:1; }) // reverse order
  for(var d in ages){
    let depth = ages[d];
    // console.log(entityUID, depth);

    // do something similar to mergeNetEntityDeltas for negative atoms.
    if(depth===0) entityConstructor[1] = newestEntityConstructor;
    else entityConstructor[1] = await loadDelta("entity", entityUID, depth);
    entityConstructor[0] = mergeDeep( entityConstructor[0], entityConstructor[1] );
  }

  let atomConstructors = entityConstructor[0].atoms;
  if (!entityDeltas[entityUID]) entityDeltas[entityUID] = [entityConstructor[0]];
  if (!atomDeltas[entityUID]) atomDeltas[entityUID] = [atomConstructors];
  else if (!atomDeltas[entityUID][0]) atomDeltas[entityUID] = [atomConstructors];
  // console.log(entityConstructor)
  // console.log(atomConstructors)
  // console.log(atomDeltas[entityUID])

  // console.log("getEntityConstructorFromFile", entityConstructor[0]);

  return entityConstructor[0];
}

async function cascadeCommit(type, uid){
  // if(type==="net") return;
  let now = Date.now();

  let typePath = "";
  let created = now;
  let deltaObj = {};

  if (type==="net"){
    deltaObj = netDeltas[uid][1];
    created = netDeltas[uid][0].created;
  }else if (type==="entity"){
    deltaObj = { ...entityDeltas[uid][1] };
    if (atomDeltas[uid][1]) deltaObj.atoms = { ...deltaObj.atoms, ...atomDeltas[uid][1] };
    created = entityDeltas[uid][0].created;
  }else return false;

  deltaObj.modified = now;

  // which depths should have been triggered between youngestDelta.modified and Date.now()?
  let youngestDelta = await loadDelta(type, uid, 0);
  let deepest = 0;
  if (created) deepest = calculateDepth(created, youngestDelta.modified || now, now);

  //
  // cascade commit based on deepest
  // merge newer deltas into older Deltas
  //
  // In the following scenario, the deltas need to merge and cascade across a gap in time:
  //
  //  0 d:2  ...  0 d:3
  //  1 d:1  ...  1
  //              2
  //              3
  //              4
  //              5 d:2
  //
  // we can use the ages dictionary to determine which delta files need to be opened and merged into which others,
  // and minimize calls to fileRead/Writes and JSON.parse/stringify
  //
  //    a new entity will have a single entry in ages:
  //         ages:{0:now}
  //
  //    over time, this will update to include the modified date of every delta file that gets saved.
  //
  //  before the merge,
  //    deltaObj is similar to depth -1
  //    youngestDelta is at depth 0
  //
  //  youngestDelta has a record of the previous ages
  //

  // tranfer the ages dictionary to the new (soon to be depth 0) deltaObj
  if(!youngestDelta.ages) deltaObj.ages = {0:now};
  else deltaObj.ages = { 0:youngestDelta.modified, ...youngestDelta.ages };

  // if (type==="entity") console.log("ages:", Object.keys(deltaObj.ages));

  // cascade checklist:
  // 1) for each entry<=deepest in ages, calculate the new target depth.
  // 2) invert that dictionary to lookup sources by targets.
  // 3) efficiently merge down each target, oldest to newest.
  //    && clear unused delta files (was a source to a non-self target)
  // 4) update deltaObj.ages before merging it to 0.delta

  // 1)
  let sourceDepths = {}; // key:source, value:target
  for(var depth=0; depth<=deepest; depth++){
    if (!deltaObj.ages[depth]) continue;
    let ageAtDepth = now - deltaObj.ages[depth];
    sourceDepths[depth] = ageAtDepth>0 ? Math.floor(Math.log2(ageAtDepth/commitInterval)) : 0;
    if (sourceDepths[depth] === -1) sourceDepths[depth] = 0;
  }

  // 2)
  let targetDepths = {}; // key:target, value:sources
  for (depth in sourceDepths){
    if (!targetDepths[sourceDepths[depth]]) targetDepths[sourceDepths[depth]] = [];
    targetDepths[sourceDepths[depth]].push(depth);
  }

  // 3)
  var a, b;
  var newAges = { ...deltaObj.ages };
  let targetDepthsSorted = Object.keys(targetDepths).sort(function(a,b){ return Number(a)>Number(b)?-1:1; });
  // if (type==="entity") console.log("targetDepths", JSON.stringify(targetDepthsSorted,null,2));

  for (t in targetDepthsSorted){
    let target = Number(targetDepthsSorted[t]);
    // if (Math.pow(2, target)<=(now-created)/commitInterval) a = await loadDelta(type, uid, target);
    // else a = {};
    a = await loadDelta(type, uid, target);

    // if (type==="net") console.log("a before merge:", JSON.stringify(a,null,2));
    let sourceList = [];
    targetDepths[target].sort(function(a,b){ return Number(a)>Number(b)?-1:1; });
    for(t in targetDepths[target]){
      let source = Number(targetDepths[target][t]);
      // console.log({source});
      if (source===target) continue; // no need to merge into self
      b = source===0 ? youngestDelta : await loadDelta(type, uid, source)
      if (source===0) youngestDelta = {};
      a = mergeDeep(a, b);
      saveDelta(type, uid, source, {}); // clear the source delta (it got merged down)
      delete newAges[source];
      sourceList.push(source);
    }
    delete a.ages;
    saveDelta(type, uid, target, a); // save the target delta now that all sources are merged
    // if (type==="net") console.log("save newAges:", {target}, {sourceList:sourceList.join()}, JSON.stringify(a,null,2));
    newAges[target] = a.modified;
  }

  // 4)
  deltaObj.ages = newAges;
  Object.keys(deltaObj.ages).forEach((age) => {
    // check for negative ages
    if (age<0) delete deltaObj.ages[age];
  });
  // if (type==="net") console.log("deltaObj:",JSON.stringify(deltaObj,null,2), "b:",JSON.stringify(b,null,2));
  // console.log("youngestDelta:", stringify(youngestDelta));
  a = mergeDeep(youngestDelta, deltaObj);
  // if (type==="net") console.log("a:",JSON.stringify(a,null,2));
  saveDelta(type, uid, 0, a);
  // if (type==="net") console.log("save newAges:", {target:0}, JSON.stringify(a,null,2));
  // if (type==="net") console.log("\n------------------\n");
  // console.log({deepest, ages:deltaObj.ages, newAges, sourceDepths, targetDepths});

  if (type==="net") delete netDeltas[uid][1];
  if (type==="entity"){
    delete entityDeltas[uid][1];
    delete atomDeltas[uid][1];
  }
}

let md5Table = {net:{}, entity:{}}
let syncTimer = {net:{}, entity:{}};
function saveDelta(type, uid, depth, obj){
  try{
    let typePath = "";
    if (type==="net") typePath = "nets";
    else if (type==="entity") typePath = "entities";
    else return false;

    let json = stringify(obj);
    let path = `${typePath}/${uid}`;
    let filename = `./${path}/${depth}.delta.${type}.json`;
    if (!fs.existsSync(path)) fs.mkdirSync(path);
    // fs.writeFileSync(filename, json);
    // if (!fs.exists(path, function(){
    //   fs.mkdirSync(path);
    //   } )
    // )
    debug.report.syncing = "SYNCING...\n@" + Date.now();
    setTimeout(function(){ debug.report.syncing = "complete.\n@" + Date.now(); }, 1000);
    fs.writeFile(filename, json, function(res){ /*console.log(res) ; debug.report.res=res;*/ } );

    let md5Include = false
    if (type==="net" && uid!==_) md5Include = true;
    if (type==="entity" && !entities[uid].nets._) md5Include = true;
    if (md5Include) {
      let hashID = insertHashID(uid);
      if (!md5Table[type][hashID]) md5Table[type][hashID] = {}; // do this for save base
      md5Table[type][hashID][depth] = md5(json);
    }
    // console.log("single sync:", {type, uid, depth});
    syncData(type, uid, depth, obj);
    // schedule a full sync at most every 5s while this object is being modified
    clearTimeout(syncTimer[type][uid]);
    syncTimer[type][uid] = setTimeout(function(params){ fullSync(params.type, params.uid) }, 1000, {type, uid});

  }
  catch(e){
    console.error(e);
    return false;
  }
}

async function loadDelta(type, uid, depth){
  let obj = {};
  try{
    let typePath = "";
    if (type==="net") typePath = "nets";
    else if (type==="entity") typePath = "entities";
    else return false;

    let json = await fs.readFileSync(`${typePath}/${uid}/${depth}.delta.${type}.json`);
    obj = JSON.parse(json);
    let md5Include = false
    if (type==="net" && uid!==_) md5Include = true;
    if (type==="entity" && !entities[uid].nets._) md5Include = true;
    if (md5Include) {
      let hashID = insertHashID(uid);
      if (!md5Table[type][hashID]) md5Table[type][hashID] = {}; // do this for save base
      md5Table[type][hashID][depth] = md5(json);
    }
  }
  catch(e){
    // try downloading it?
    // console.error(e);
    return {};
  }
  return obj;
}

class Net {
  constructor(uid, params){
    this.uid = uid;
    this.name = params.name;
    this.entities = params.entities || {};
    this.x = Math.round(params.x) || 0;
    this.y = Math.round(params.y) || 0;
    this.mode = params.mode || "-listBox";

    let remoteClass = uid[0]===_ ? "" : " REMOTE";

    this.addEntityHash = function() {
      // insert remote hash in place of _ if the uid is remote
      if (uid[0]!==_){
        let remoteUID = uid.substr(0,32);
        for (var eUID in this.entities){
          let hUID = insertHashID(eUID, remoteUID);
          this.entities[hUID] = this.entities[eUID];
          delete this.entities[eUID];
        }
      }
    };
    this.addEntityHash();
    // console.log(this.uid, JSON.stringify(this.entities, null,2));

    let eList = [];
    for (var i in this.entities) eList.push( this.entities[i].name );
    this.label = this.name===_ ? "" : this.name;

    // load entities
    if (this.mode === "+basic" || this.mode === "+listBox") loadEntitiesFromList(this);
    if (this.mode === "+isolated" || this.mode === "-isolated"){ netLoadCount--; }

    if (this.name===_){
      loadEntitiesFromList(this);
      return;
    }

    /*
    this.container.onmouseover = function(ev){
      if (selection.constructor.name==="Net" || selection.constructor.name==="Entity"){
        drawLink(selection, nets._, true); // erase
        selection.target = this.net;
        return;
      }
      if ([this.net.dot0, this.net.dotN, this.net.dotSW, this.net.dotSE, this.net.label].indexOf(ev.target)!==-1){
        let dS = .3/(zoom<1?zoom:1);
        clearTimeout(this.dotZoomTimer);
        this.net.dot0.style.transition = "transform .25s ease 0s, background .5s";
        this.net.dot0.style.transform = `scale3d(${[dS,dS,dS]}) rotate(${Math.PI/24}rad)`;
      }
    }
    this.container.onmouseout = function(){
      if (selection.target===this.net){
        drawLink(selection, this.net, true); // erase
        selection.target = undefined;
        return;
      }
      let dS = .125;
      // this.net.dot0.style.transition = "transform .25s ease 1s, background .5s";
      clearTimeout(this.dotZoomTimer);
      this.dotZoomTimer = setTimeout(function(params){
        params.el.style.transform = `scale3d(${[params.dS,params.dS,params.dS]}) rotate(${0*Math.PI/24}rad)`;
      }, 1250, {el:this.net.dot0, dS});
    }
    this.container.onmousedown = function(ev){
      selection = this.net;
      selection.container.style.transition = "";
      selection.offsetX = ev.clientX/zoom - this.offsetLeft - screenOffsetX;
      selection.offsetY = ev.clientY/zoom - this.offsetTop - screenOffsetY;

      document.onmousemove = function(ev){
        selection.moved = true;
        selection.x = ev.clientX/zoom - selection.offsetX - screenOffsetX;
        selection.y = ev.clientY/zoom - selection.offsetY - screenOffsetY;
        selection.container.style.left = `${selection.x}px`;
        selection.container.style.top = `${selection.y}px`;
        updateLinks(selection);
      }
    }

    this.container.onmouseup = function(){
      if (this.net.moved){
        this.net.moved = false;
        store(this.net, {x:this.net.x, y:this.net.y}, "Net.container.onmouseup");
      }
    }

    this.label.ondblclick = function(){
      this.net.dot0.onclick({target:this.net.dot0});
      this.selectAll(false);
    }
    this.label.onmousedown = function(){ this.moved = false; }
    this.label.onmouseup = function(){
      if (!this.moved) {
        this.focus();
        this.selectAll(true);
      }
      this.moved = false;
    }
    this.label.onmousemove = function(){ this.moved = true; }
    this.label.selectAll = function(mode){
      window.setTimeout(function(el, mode) {
        var sel, range;
        range = document.createRange();
        range.selectNodeContents(el);
        sel = window.getSelection();
        sel.removeAllRanges();
        if(mode == true) sel.addRange(range);
      }, 10, this, mode);
    }
    this.label.onblur = function(){
      this.selectAll(false);
      renameNet(this.net, this.innerText);
    }
    this.label.onkeydown = function(ev){
      // abort rename if a navigation shortcut was triggered
      if (ev.altKey && shortCutKeys.altKey[ev.code]){
        this.selectAll(false);
        return false;
      }
      // commit the rename on enter key
      if (ev.key == "Enter"){
        this.blur();
        return false;
      }
    }

    // toggle whatever mode was last used
    this.dot0.onclick = function(ev){
      // console.log(this.net.mode);
      if (ev.target!==this) return;
      let modeState = this.net.mode[0];
      let modeName = this.net.mode.substr(1);
      if (modeName==="listBox") this.net.dotN.onclick();
      if (modeName==="basic") this.net.dotSE.onclick();
      if (modeName==="isolated") this.net.dotSW.onclick();
    }

    // toggle "show/hide a simple list of entities" mode
    this.dotN.onclick = function(){
      if (this.net.mode === "+isolated") loadAllNets([this.net.uid]);
      this.net.mode = this.net.mode==="+listBox" ? "-listBox" : "+listBox";
      this.net.listBox.style.opacity = this.net.mode==="+listBox" ? 1 : 0;
      if (this.net.mode[0] === "+"){
        this.setAttribute("class", "node node-net node-net-active");
        this.net.dotSE.setAttribute("class", "node node-net");
        this.net.dotSW.setAttribute("class", "node node-net");
        loadEntitiesFromList(this.net);
      }
      else unloadEntitiesFromList(this.net);
      store(this.net, {mode:this.net.mode}, "Net.dotN.onclick");
    }

    // toggle "show/hide this net's entities among the rest" mode
    this.dotSE.onclick = function(){
      if (this.net.mode === "+isolated") loadAllNets([this.net.uid]);
      this.net.mode = this.net.mode==="+basic" ? "-basic" : "+basic";
      this.net.listBox.style.opacity = 0;
      if (this.net.mode[0] === "+"){
        this.net.dotN.setAttribute("class", "node node-net");
        this.setAttribute("class", "node node-net node-net-active");
        this.net.dotSW.setAttribute("class", "node node-net");
        loadEntitiesFromList(this.net);
      }
      else unloadEntitiesFromList(this.net);
      store(this.net, {mode:this.net.mode}, "Net.dotSE.onclick");
    }

    // toggle "show/hide exclusively this net's entities" mode
    this.dotSW.onclick = function(){
      this.net.mode = this.net.mode==="+isolated" ? "-isolated" : "+isolated";
      this.net.listBox.style.opacity = 0;
      // to-do: also blackout everything else...
      if (this.net.mode[0] === "+"){
        this.net.dotN.setAttribute("class", "node node-net");
        this.net.dotSE.setAttribute("class", "node node-net");
        this.setAttribute("class", "node node-net node-net-active");
        let q = 0; // stagger unloading by 50ms
        for (var n in nets){
          if (nets[n] !== this.net){
            unloadEntitiesFromList(nets[n]);
            setTimeout(function(name){
              if (nets[name] !== nets._) removeNet(nets[name]);
            }, 250 + q, n);
            q+=50;
            nets[n].container.style.transition = "opacity .5s";
            nets[n].container.style.opacity = 0;
          }
        }
        loadEntitiesFromList(this.net);
      }else{
        unloadEntitiesFromList(this.net);
        loadAllNets([this.net.uid]);
      }
      store(this.net, {mode:this.net.mode}, "Net.dotSW.onclick");
    }
    */

    if (this.mode === "+isolated"){
      isolateNetOnLoad = this;
      // console.log(this.name, isolateNetOnLoad);
    }
  }
}

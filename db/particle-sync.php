<?php

// $timezone = "Asia/Calcutta"; date_default_timezone_set($timezone);
$timezone = "US/Central"; date_default_timezone_set($timezone);
$time = date("M d, Y, h:i:sa", time());
$logEntry = "\n_______________\nDATE & TIME: $time ($timezone)";

if (!file_exists("nets")) { $logEntry .= "\ncreated ./nets/"; mkdir("nets", 0777, true); }
if (!file_exists("entities")) { $logEntry .= "\ncreated ./entities/"; mkdir("entities", 0777, true); }

$post = json_decode(file_get_contents('php://input'), true);
$type = $post["type"];
$logEntry .= "\nTYPE: $type";

function logCommit(){
  global $logEntry;
  if (filesize("log.col")>1024*16) file_put_contents("log.col", $logEntry); // 16kb overwrite threshold (vicinity of 500 lines)
  else file_put_contents("log.col", $logEntry, FILE_APPEND);
}


// md5 Sync check
if ($type==="md5Single"){
  $remoteItem = $post["md5Table"];
  $itemType = $remoteItem["type"];
  $itemUID = $remoteItem["uid"];
  $itemTable = $remoteItem["table"];

  if ($itemType==="net") $typePath = "nets";
  if ($itemType==="entity") $typePath = "entities";

  // build response table
  foreach($itemTable as $depth => $md5){
    $delta = "$depth.delta.$itemType.json";
    if (!file_exists("$typePath/$itemUID/$delta")) $content = "";
    else $content = file_get_contents("$typePath/$itemUID/$delta");
    if (strlen($content) < 2) unset($itemTable[$depth]);
    if ($itemTable[$depth]==md5($content)) unset($itemTable[$depth]);
  }

  // report mismatches:
  $md5Table = array( 'net' => array(), 'entity' => array() );
  $md5Table[$itemType][$itemUID] = $itemTable;
  $report = array( 'mismatches' => $md5Table);
  $jsonString = json_encode($report, JSON_PRETTY_PRINT);
  echo $jsonString;
  $logEntry .= "\n(SINGLE) MISMATCH REPORT: $jsonString";
  logCommit();
}

if ($type==="md5Full"){
  $md5TableREMOTE = $post["md5Table"];
  $md5TableLOCAL = array( 'net' => array(), 'entity' => array() );

  // echo "[force md5 debug]\n\n";

  // TO-DO: CACHE THIS TABLE AS A FILE, then READ/update/WRITE instead of rebuilding each time.
  // add force rebuild-cache option?

  // md5Single won't need to scan EVERYTHING if we cache this. !!!!!!!!!!!!!!!!!!! do this

  // build local md5 table for nets
  $nets = scandir("nets");
  foreach($nets as $key => $net){
    if ($net==="." || $net==="..") continue;
    $netDeltas = scandir("nets/$net");
    foreach ($netDeltas as $key => $delta) {
      if ($delta==="." || $delta==="..") continue;
      $depth = str_replace(".delta.net.json","", $delta);
      $content = file_get_contents("nets/$net/$delta");
      if (strlen($content) > 2){
        $md5TableLOCAL['net'][$net][$depth] = md5($content);;

        if ($md5TableLOCAL['net'][$net][$depth]==$md5TableREMOTE['net'][$net][$depth]){
          unset($md5TableREMOTE['net'][$net][$depth]);
        }else $md5TableREMOTE['net'][$net][$depth] = 0; // no need to send the same data back
      }else unset($md5TableREMOTE['net'][$net][$depth]);
    }
  }

  // build local md5 table for entities
  $entities = scandir("entities");
  foreach($entities as $key => $entity){
    if ($entity==="." || $entity==="..") continue;
    $entityDelta = scandir("entities/$entity");
    foreach ($entityDelta as $key => $delta) {
      if ($delta==="." || $delta==="..") continue;
      $depth = str_replace(".delta.entity.json","", $delta);
      $content = file_get_contents("entities/$entity/$delta");
      if (strlen($content) > 2){
        $md5TableLOCAL['entity'][$entity][$depth] = md5($content);

        if ($md5TableLOCAL['entity'][$entity][$depth]==$md5TableREMOTE['entity'][$entity][$depth]){
          unset($md5TableREMOTE['entity'][$entity][$depth]);
        }else $md5TableREMOTE['entity'][$entity][$depth] = 0; // we only need to know the key, save 31 bytes per key
      }else unset($md5TableREMOTE['entity'][$entity][$depth]);

    }
  }

  // save $md5TableLOCAL – we cached it, now what? don't neglect race conditions
  file_put_contents("md5Cache.col", json_encode($md5TableLOCAL, JSON_PRETTY_PRINT));

  // report mismatches:
  $report = array( 'mismatches' => $md5TableREMOTE);
  $jsonString = json_encode($report, JSON_PRETTY_PRINT | JSON_FORCE_OBJECT);
  echo $jsonString;
  $logEntry .= "\nMISMATCH REPORT: $jsonString";
  logCommit();
  // file_put_contents("log.col", $logEntry, FILE_APPEND);
}

// fresh delta to save; or remote delta to load
if ($type==="net" || $type==="entity") {
  $mode = $post["mode"];
  $delta = $post["delta"];
  $uid = $post["uid"];
  $depth = $post["depth"];

  if ($type==="net") $typePath = "nets";
  else if ($type==="entity") $typePath = "entities";

  if ($mode==="save") {
    if (!file_exists("$typePath/$uid")) { mkdir("$typePath/$uid", 0777, true); }
    $path = "$typePath/$uid/$depth.delta.$type.json";
    $jsonString = json_encode($delta, JSON_PRETTY_PRINT | JSON_FORCE_OBJECT);

    echo $path;
    file_put_contents($path, $jsonString);
    $logEntry .= "\nSAVED: $path";
    logCommit();
    // file_put_contents("log.col", $logEntry, FILE_APPEND);
  }

  if ($mode==="load") {
    $md5 = $post["md5"]; // <--supplied from this php script, now coming back
    $logEntry .= "\nMD5: [$md5]";
    $path = "$typePath/$uid/$depth.delta.$type.json";

    if (!file_exists("$path")) { echo null; }
    else {
      $content = file_get_contents($path);
      $localMd5 = md5($content);
      if ($md5 == $localMd5) echo $localMd5.'{"noChange":true}';
      else {
        if (!file_exists("$typePath/$uid")) { echo null; }
        echo "$localMd5$content";
        $logEntry .= "\nLOADED: $path";
        logCommit();
        // file_put_contents("log.col", $logEntry, FILE_APPEND);
      }
    }
  }
}

// file_put_contents("log.col", $logEntry, FILE_APPEND);

?>

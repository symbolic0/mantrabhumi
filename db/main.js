// v1.0
// const fs = window.require('fs');
// const md5 = window.require('md5');
// const translate = window.require('translate'); // Old school

let netLoadCount = 1; // _ by default, then ++ for each one after that
let isolateNetOnLoad = undefined;
let activeEntityUID = undefined;
let activeAtom = undefined;
let optionKey = false;
let ctrlKey = false;
let shortCutKeys = {altKey:{Digit0:true, Equal:true, Minus:true}} // used to avoid conflicts when entering text in a text field

let screenOffsetX = Number(localStorage.screenOffsetX) || 108;
let screenOffsetY = Number(localStorage.screenOffsetY) || 108;
let zoom = Number(localStorage.zoom) || 1;
let zoomX = undefined;
let zoomY = undefined;
let zoomInProgress = false;
let zoomTimer = undefined;

var stage, entityBox, linkBox;

let topEntityIndex = 0;

let _uid = "";
const _ = "_"; // replace with pub_key hash on upload
const atoms = {};
const entities = {};
const nets = {};
const atomDeltas = {};
const entityDeltas = {};
const netDeltas = {};

let inboxTimer = 0;

function initDB(){
  // lightModeCSS = document.getElementById("light-mode-css");
  // if (localStorage.lightmode!=="true") toggleLightMode();
  // stage = document.createElement("div");
  // entityBox = document.createElement("div");
  // linkBox = document.createElement("div");
  // stage.setAttribute("id", "stage");
  // entityBox.setAttribute("id", "entityBox");
  // linkBox.setAttribute("id", "linkBox");
  // document.body.append(stage);
  // stage.append(linkBox);
  // stage.append(entityBox);
  // bakeZoomTransform(0, 0);
  // stage.style.transform = `scale3d(${[zoom,zoom,zoom]})`;

  // buildDefaultDirectories();
  _uid = getPubKeyHash();

  // networks of entities
  let q = loadAllNets();

  // now trigger whichever net called +isolated mode last
  // setTimeout(function(){
  //   if (isolateNetOnLoad){
  //     isolateNetOnLoad.mode = "-isolated"; // cuz we are about to simulate a click
  //     isolateNetOnLoad.dotSW.onclick();
  //   }
  // }, q, localStorage.activeEntityUID);

  performTasksAfterNetsAreLoaded();
  // setTimeout(commitDeltas, 1000);
}

function performTasksAfterNetsAreLoaded(){
  // console.log({netLoadCount});
  // make sure nothing is still loading. if it is try again in 50ms
  if (netLoadCount>0) {
    setTimeout(performTasksAfterNetsAreLoaded, 50);
    return;
  }

  // now perform the tasks
  // trigger whichever net called +isolated mode last (if any)
  if (isolateNetOnLoad){
    isolateNetOnLoad.mode = "-isolated"; // cuz we're about to simulate a click
    isolateNetOnLoad.dotSW.onclick();
  }

  md5_SyncCheck();
  syncAllRemoteFacets();

  if (!fs.isdirSync("entities/_.0.entity")) {
    newEntity(0,0,0);
  }
}

/*
function buildDefaultDirectories(){
  if (!fs.existsSync("keys")) fs.mkdirSync("keys");
  if (!fs.existsSync("entities")) fs.mkdirSync("entities");
  if (!fs.existsSync("nets")) fs.mkdirSync("nets");
  if (!fs.existsSync("nets/_")){
    fs.mkdirSync("nets/_");
    fs.writeFileSync(`nets/_/base.delta.net.json`, stringify({
      created: Date.now(),
      uid: _,
      name: _,
      mode: "-listBox",
      x: 0,
      y: 0,
      entities: {}
    }));
  }
}
*/

function checkInboxes(){
  if (!activeEntityUID) return;
  clearTimeout(inboxTimer);
  // message flow over time        without processQue:                    with processQue:
  //  t=0      t=1      t=2         id │ msgs  id │ m     id │ m           id │ m     id │ m     id │ m
  //  ─────────────────────         ───┼───0   ───┼───1   ───┼───2         ───┼───0   ───┼───1   ───┼───2
  //   a ──┬──▶ a        a           a │        a │ 1      a │              a │        a │ 1      a │
  //   b   └──▶ b ──┐    b           b │ 1      b │        b │              b │        b │ 1      b │
  //   c        c   └──▶ c           c │ 1      c │        c │              c │        c │        c │ 1
  //   d        d   ┌──▶ d           d │        d │        d │ 1            d │        d │        d │ 1
  //   e   ┌──▶ e ──┘    e           e │        e │ 1      e │              e │        e │ 1      e │
  //   f ──┴──▶ f        f           f │        f │ 1      f │              f │        f │ 1      f │

  const processCyles = 1; // iterations per function call
  for (var c=0; c<processCyles; c++){
    let processQue = [];
    for (var i in atoms[activeEntityUID]){
      if (!activeEntityUID) return;
      if (!atoms[activeEntityUID][i].inbox) continue;
      if (!atoms[activeEntityUID][i].inbox.length) continue;
      msg = atoms[activeEntityUID][i].inbox.shift();
      processQue.push({
        self: atoms[activeEntityUID][i],
        script: atoms[activeEntityUID][i].script,
        msg: msg
      });
    }

    if (!processQue.length) return;
    for (var i=0; i<processQue.length; i++){
      try{
        processQue[i].script(processQue[i].self, processQue[i].msg);
      } catch(e){
        let scriptString = undefined;
        if (processQue[i].script) scriptString = processQue[i].script.toString();
        console.error("Script error:", {
          entity:processQue[i].self.entityUID,
          atom:processQue[i].self.id,
          script:scriptString },
          e
        );
      }
    }
  }
  // checkEntityBounds();
  inboxTimer = setTimeout(checkInboxes, 0);
}

function newAtom(atomConstructor){ //x,y,z, rx,ry,rz){
  // create new atom
  if (activeEntityUID){
    let newAtomID = getNewAtomID();
    // let atomName = getNewAtomName();
    // let atomConstructor = { id: newAtomID, x, y, z, rx, ry, rz };
    atomConstructor.id = newAtomID;
    a = atoms[activeEntityUID][newAtomID] = new Atom(atomConstructor);
    store(a, atomConstructor, "document.ondblclick : new Atom");
    checkEntityBounds()
    return a;
  }
}

async function newEntity(x,y,z){  // create new entity
  let newEntityUID = getNewEntityUID();
  let eName = getNewEntityName();
  let entityConstructor = {
    created: Date.now(),
    name: eName,
    open: "true",
    x,
    y,
    z,
    dx: 0,
    dy: 0,
    dz: 0,
  };
  entities[newEntityUID] = new Entity(newEntityUID, entityConstructor, _);
  entityDeltas[newEntityUID] = [entityConstructor];
  activeEntityUID = newEntityUID; // hmm... it works, but...
  await openEntity(newEntityUID);
  nets._.entities[newEntityUID] = entityConstructor;
  store(nets._, {entities:{[newEntityUID]:entityConstructor}}, "new Entity");
  // store(entities[newEntityUID], entityConstructor, "document.ondblclick : new Entity");
  saveBaseEntity(newEntityUID);
  // store(entities[newEntityUID], entityConstructor, "document.ondblclick : new Entity"); // redundant?
}

/*
document.ondblclick = function(e){
  // create new atom
  if (activeEntityUID && !e.target.net){
    if (e.target === entities[activeEntityUID].atomPane) {
      let newAtomID = getNewAtomID();
      // let atomName = getNewAtomName();
      let atomConstructor = {
        id: newAtomID,
        // name: atomName,
        // entity: activeEntityUID,
        x: e.clientX/zoom - 20 - screenOffsetX - entities[activeEntityUID].x,
        y: e.clientY/zoom - 20 - screenOffsetY - entities[activeEntityUID].y
      };
      a = atoms[activeEntityUID][newAtomID] = new Atom(atomConstructor);
      store(a, atomConstructor, "document.ondblclick : new Atom");
      checkEntityBounds()
    }
  }

  // create new entity
  if (e.target === document.documentElement && !e.target.net) {
    let newEntityUID = getNewEntityUID();
    let eName = getNewEntityName();
    let entityConstructor = {
      created: Date.now(),
      name: eName,
      open: "true",
      x: Math.round(e.clientX/zoom - screenOffsetX),
      y: Math.round(e.clientY/zoom - screenOffsetY),
      dx: 0,
      dy: 0
    };
    entities[newEntityUID] = new Entity(newEntityUID, entityConstructor, _);
    entityDeltas[newEntityUID] = [entityConstructor];
    activeEntityUID = newEntityUID; // hmm... it works, but...
    await openEntity(newEntityUID);
    nets._.entities[newEntityUID] = entityConstructor;
    store(nets._, {entities:{[newEntityUID]:entityConstructor}}, "document.ondblclick : new Entity");
    // store(entities[newEntityUID], entityConstructor, "document.ondblclick : new Entity");
    saveBaseEntity(newEntityUID);
    // store(entities[newEntityUID], entityConstructor, "document.ondblclick : new Entity"); // redundant?
  }
}

document.onmouseup = function document_onmouseup(){
  if (selection.bonding) {
    if (!selection.target) drawBond(selection, {x:0, y:0, id:"unlinked"}, "erase");
    else{
      let selectionType = undefined;
      let targetType = undefined;
      if (selection.constructor) selectionType = selection.constructor.name; // "Net", "Entity", or "Atom"
      if (selection.target.constructor) targetType = selection.target.constructor.name;

      // Atom-to-Atom Bond
      if (targetType==="Atom") {
        drawBond(selection, {x:0, y:0, id:"unlinked"}, "erase");
        drawBond(selection, selection.target, "create");
        notEmpty = selection.text.value;
        selection.text.value += `${notEmpty?"\n":""}send this to "${selection.target.id}"`;
        // selection.text.value += `${notEmpty?"\n":""}send {src:this, txt:""} to "${selection.target.id}";`;
        functionifyScript(selection);
        updateBonds(selection);
        moveSelection(selection, selection.x, selection.y);
        setTimeout(function(t){ t.onkeydown({target:t}); }, 0, selection.text);
        store(selection, {txt:selection.text.value}, "document.onmouseup : Atom bond")
        checkEntityBounds();
      }

      // Entity-to-Net Link
      if (selectionType==="Entity" && targetType==="Net") {
        if (selection.target===nets._){
          // create a new Net
          let netUID = getNewNetUID();
          let netName = getNewNetName();
          let netConstructor = {
            created: Date.now,
            name:netName,
            entities:getEntityListConstructors([selection]),
            x:nets._.x,
            y:nets._.y,
            mode:"+basic"
          }
          nets[netUID] = new Net(netUID, netConstructor);
          selection.nets[netUID] = true;
          delete selection.nets._;
          delete nets._.entities[selection.uid];
          drawLink(selection, selection.target, true); // erase
          drawLink(selection, nets[netUID]);
          saveBaseNet(netUID);
          // store(nets[netUID], netConstructor, "document.ondblclick : new Net");
          store(nets._, {entities:{[`-${selection.uid}`]:{}}}, "document.onmouseup : new Net");
        }else{
          // add the selection to the target Net
          selection.nets[selection.target.uid] = true;
          delete selection.nets._;
          delete nets._.entities[selection.uid]
          selection.target.entities[selection.uid] = getEntityConstructor(selection);
          store(selection.target, {entities:{[selection.uid]:selection.target.entities[selection.uid]}}, "document.onmouseup : existing Net");
          store(nets._, {entities:{[`-${selection.uid}`]:{}}}, "document.onmouseup : existing Net");
        }
        nets._.container.style.display = "none";
      }

      // Entity-to-Net-to-Entity double Link
      if (selectionType==="Entity" && targetType==="Entity") {
        if (selection!==selection.target){
          // relink both Entities to a newly created Net at the link's midpoint
          drawLink(selection, selection.target, true); // erase
          let midPoint = drawLink(selection, selection.target);
          drawLink(selection, selection.target, true); // erase
          let netUID = getNewNetUID();
          let netName = getNewNetName();
          let netConstructor = {
            created: Date.now,
            name:netName,
            entities:getEntityListConstructors([selection, selection.target]),
            x:midPoint.x,
            y:midPoint.y,
            mode:"+basic"
          }
          nets[netUID] = new Net(netUID, netConstructor);
          // console.log(selection, selection.target);
          selection.nets[netUID] = true;
          selection.target.nets[netUID] = true;
          delete selection.nets._;
          delete selection.target.nets._;
          delete nets._.entities[selection.uid];
          delete nets._.entities[selection.target.uid];
          drawLink(selection, nets[netUID]);
          drawLink(selection.target, nets[netUID]);
          saveBaseNet(netUID);
          // store(nets[netUID], netConstructor, "document.ondblclick : new (midpoint) Net");
          store(nets._, {entities:{[`-${selection.uid}`]:{}}}, "document.ondblclick : new Net");
          store(nets._, {entities:{[`-${selection.target.uid}`]:{}}}, "document.ondblclick : new Net");
          // store(nets._, {entities:nets._.entities}, "document.ondblclick : new (midpoint) Net");
        }
        else drawLink(selection, selection.target, true); // erase
        nets._.container.style.display = "none";
      }
    }
    selection.target = undefined;
    selection.bonding = false;
  }
  // store(selection);
  selection = {};
  document.onmousemove = null;
};

document.onkeydown = function(e){
  if(e.key === "Alt" ) optionKey = true;
  if(e.key === "Ctrl" ) ctrlKey = true;

  if ((MAC?e.metaKey:e.altKey) && e.key==="Enter" && activeAtom) {
    activeAtom.play.onclick();
  }

  if ((MAC?e.metaKey:e.ctrlKey) && e.key==="r") {
    commitDeltas("force");
    location.href = location.href;
    return false;
  }

  if(e.code === "Digit0" && e.altKey) zoomTo(1); // zoom to 100%
  if(e.code === "Equal" && e.altKey){
    if (!activeEntityUID) return;
    // zoom to fit active Entity
    a = entities[activeEntityUID];
    let mx = (a.container.offsetLeft + a.atomPane.offsetLeft + a.atomPane.offsetWidth/2 + screenOffsetX) * zoom; // entity midpoints in screen coords
    let my = (a.container.offsetTop + a.atomPane.offsetTop + a.atomPane.offsetHeight/2 + screenOffsetY) * zoom

    let wx = window.innerWidth/2;
    let wy = window.innerHeight/2;

    let zx = window.innerWidth / a.atomPane.offsetWidth;
    let zy = window.innerHeight / a.atomPane.offsetHeight;
    let z = Math.min(zx, zy) * .9

    entityBox.style.transition = "top .25s ease, left .25s ease";
    linkBox.style.transition = "top .25s ease, left .25s ease";
    moveBy(mx-wx, my-wy);
    setTimeout(function(z){
      entityBox.style.transition = "";
      linkBox.style.transition = "";
      zoomTo(z) // zoom to fit active entity's atomPane
    }, 270, z);
  }
  if(e.code === "Minus" && e.altKey){
    if (!Object.keys(entities).length) return;
    // zoom to fit all entities
    let left = 99999;
    let top = 99999;
    let right = -99999;
    let bottom = -99999;
    for(i in entities){
      let a = entities[i];
      if (isolateNetOnLoad && isolateNetOnLoad.mode === "+isolated" && !a.nets[isolateNetOnLoad.uid]) continue;
      left = Math.min(left, a.container.offsetLeft + a.atomPane.offsetLeft);
      top = Math.min(top, a.container.offsetTop + a.atomPane.offsetTop);
      right = Math.max(right, a.container.offsetLeft + a.atomPane.offsetLeft + a.atomPane.offsetWidth);
      bottom = Math.max(bottom, a.container.offsetTop + a.atomPane.offsetTop + a.atomPane.offsetHeight);
    }
    for(i in nets){
      if (i === _) continue;

      let a = nets[i];
      if (isolateNetOnLoad && isolateNetOnLoad.mode === "+isolated" && a!==isolateNetOnLoad) continue;

      let offsetW_label = a.label.offsetLeft+a.label.offsetWidth;
      let offsetH_label = a.label.offsetTop+a.label.offsetHeight;
      let offsetW_listBox = a.mode === "+listBox" ? a.listBox.offsetLeft+a.listBox.offsetWidth : 0;
      let offsetH_listBox = a.mode === "+listBox" ? a.listBox.offsetTop+a.listBox.offsetHeight : 0;
      left = Math.min(left, a.container.offsetLeft);
      top = Math.min(top, a.container.offsetTop);
      right = Math.max(right, a.container.offsetLeft+offsetW_label, a.container.offsetLeft+offsetW_listBox);
      bottom = Math.max(bottom, a.container.offsetTop+offsetH_label, a.container.offsetTop+offsetH_listBox);
    }
    let totalWidth = right - left;
    let atotalHeight = bottom - top;

    let mx = (left + totalWidth/2 + screenOffsetX) * zoom; // all entities' Bounding Box midpoints in screen coords
    let my = (top + atotalHeight/2 + screenOffsetY) * zoom

    let wx = window.innerWidth/2;
    let wy = window.innerHeight/2;

    let zx = window.innerWidth / totalWidth;
    let zy = window.innerHeight / atotalHeight;
    let z = Math.min(zx, zy) * .9; // 90% zoomed to fit, so there's 5% padding on each edge

    entityBox.style.transition = "top .25s ease, left .25s ease";
    linkBox.style.transition = "top .25s ease, left .25s ease";
    moveBy(mx-wx, my-wy);
    setTimeout(function(z){
      entityBox.style.transition = "";
      linkBox.style.transition = "";
      zoomTo(z) // zoom to fit all entities' atomPanes
    }, 270, z);
  }
}

document.onkeyup = function(e){
  // console.log(e);
  if(e.key === "Alt") optionKey = false;
  if(e.key === "Ctrl") ctrlKey = false;
}

document.documentElement.onmousedown = function(e){ if (e.target === this) setActiveEntity(undefined); }
document.documentElement.onmousemove = function(e){
  // console.log(e.clientX, e.clientY);
  clearTimeout(zoomTimer);
  stage.style.cursor = "";
  bakeZoomTransform(0, 0);
  zoomX = zoomY = 0;
  zoomInProgress = false;
}

function prune(array, value){
  let newArray = [];
  for (var i=0; i<array.length; i++){
    if (array[i]!==value) newArray.push(array[i]);
  }
  return newArray;
}

document.onmousewheel = function(e){
  let dx = e.deltaX;
  let dy = e.deltaY;
  let noScroll = true;
  if (e.target.tagName === "TEXTAREA")
    noScroll = (e.target.scrollWidth <= e.target.offsetWidth
            && e.target.scrollHeight <= e.target.offsetHeight)
            || e.target.atom != activeAtom;

  if (optionKey || zoomInProgress){
    if (!zoomInProgress){
      bakeZoomTransform(e.clientX, e.clientY);

      zoomStart = zoom;
      zoomX = e.clientX;
      zoomY = e.clientY;
      zoomInProgress = true;
    }
    zoom *= (1 + dy/1008);
    localStorage.zoom = zoom;
    clearTimeout(zoomTimer);
    zoomTimer = setTimeout(function(){
      stage.style.cursor = "";
      bakeZoomTransform(0, 0);
      zoomX = zoomY = 0;
      zoomInProgress = false;
    }, 200);
    stage.style.cursor = `zoom-${dy>0 ? "in" : "out"}`;
    stage.style.transform = `scale3d(${[zoom,zoom,zoom]})`;
   }

  else if (
  // (MAC ? metaKey : ctrlKey) ||
   noScroll){
    moveBy(dx, dy);
  }
}

document.onselectionchange = function(){
  if (selection.moved) {
    selection.label.selectAll(false);
    return false;
  }
}
document.onselectstart = function(e){
  let className = "";
  if (e.target.parentElement) className = e.target.parentElement.getAttribute("class");
  if (e.target.type !== "textarea"
    && className!=="entityLabel"
    && className!=="net-label") return false;
}

function moveBy(dx, dy){
  screenOffsetX -= dx/zoom;
  screenOffsetY -= dy/zoom;
  moveTo(screenOffsetX, screenOffsetY);
}

function moveTo(x, y){
  screenOffsetX = x;
  screenOffsetY = y;
  entityBox.style.left = `${screenOffsetX}px`;
  entityBox.style.top = `${screenOffsetY}px`;
  linkBox.style.left = `${screenOffsetX}px`;
  linkBox.style.top = `${screenOffsetY}px`;
  localStorage.screenOffsetX = screenOffsetX;
  localStorage.screenOffsetY = screenOffsetY;
  // bakeZoomTransform(-x,-y);
  // setTimeout(document.onmousemove,500);
}

function zoomTo(z){
  bakeZoomTransform(window.innerWidth/2, window.innerHeight/2);
  zoomX = window.innerWidth/2;
  zoomY = window.innerHeight/2;
  localStorage.zoom = zoom = z;
  stage.style.transition = "transform .25s ease";
  stage.style.transform = `scale3d(${[z,z,z]})`;
  setTimeout(function(){
    document.documentElement.onmousemove();
    stage.style.transition = ""; }, 250);
}

function bakeZoomTransform(x, y){
  // calculate the perceived travel and bake it into the new screenOffsets
  // such that setting transform-origin to new clientXY results in no translation
  if (isNaN(zoom)) zoom = 1;
  if (isNaN(screenOffsetX)) screenOffsetX = 0;
  if (isNaN(screenOffsetY)) screenOffsetY = 0;
  if (zoomX === undefined) zoomX = x;
  if (zoomY === undefined) zoomY = y;
  let ox = (1/zoom - 1) * (zoomX - x); // change in scale * change in anchor-x
  let oy = (1/zoom - 1) * (zoomY - y); // change in scale * change in anchor-y
  screenOffsetX = screenOffsetX + ox;
  screenOffsetY = screenOffsetY + oy;
  entityBox.style.left = `${screenOffsetX}px`;
  entityBox.style.top = `${screenOffsetY}px`;
  linkBox.style.left = `${screenOffsetX}px`;
  linkBox.style.top = `${screenOffsetY}px`;
  localStorage.screenOffsetX = screenOffsetX;
  localStorage.screenOffsetY = screenOffsetY;
stage.style["transform-origin"] = `${x}px ${y}px`;
}

function toggleLightMode(){
  lightModeCSS.disabled = !lightModeCSS.disabled;
  localStorage.lightmode = !lightModeCSS.disabled;
  colors = getCSScolors(lightModeCSS.disabled ? 0 : 1);
}
*/
function stringify(obj){
  // return JSON.stringify(obj);
  return JSON.stringify(obj, null, 4);
}

// upload entities and nets we own
// const syncPHP = "http://localhost/remote-test/sync.php";
const syncPHP = "http://goodsoftwaredevelopment.com/presentville/deer/db/sync.php";

function syncData(type, uid, depth, obj){
  return 0;
  
  // don't upload the local net
  if (uid===_) return;
  if (type==="entity") if (entities[uid]) if (entities[uid].nets[_]) return;

  // only upload uid's we ownz
  if (uid[0]!="_") return;
  console.log("syncing...");
  uid = insertHashID(uid);

  fetch(syncPHP, {
    method: 'post',
    mode: 'no-cors',
    headers: {
      'Accept': 'application/json, text/plain, */*',
      'Content-Type': 'application/json'
    },
    body: JSON.stringify( {type, mode:"save", uid, depth, delta:obj} )
  }).then(
    res => console.log(res)
  )
}

function retrieveData(type, uid, md5, depth, callback){
  return 0;

  var xhttp = new XMLHttpRequest();
  xhttp.onreadystatechange = function() {
    if (this.readyState == 4 && this.status == 200){
      // document.write(this.responseText);
      // console.log("retrieveData response:", this.responseText);
      try{
        // console.log(this.responseText.substr(0,32));
        // console.log(this.responseText.substr(32));
        let obj = {md5:this.responseText.substr(0,32), delta:JSON.parse(this.responseText.substr(32))};
        callback(obj);
      } catch(e){
        // console.error(e, this.responseText);
        // document.write(`<pre>${this.responseText}</pre>`);
      }
    }
  }
  xhttp.open("POST", syncPHP);
  xhttp.setRequestHeader("Content-Type", "application/json");
  xhttp.send( JSON.stringify( {type, mode:"load", uid, md5, depth} ) );
}

// full sync-check using md5's
function fullSync(type, uid){
  return 0;

  md5_SyncCheck();
  md5_SyncCheck(type, uid);
  return;
}

function md5_SyncCheck(type, uid){
  return 0;

  // actually we probably don't want to do syncCheck on the entire md5 table...
  // i.e closed entities or isolated net (excludes all other nets)

  var xhttp = new XMLHttpRequest();
  xhttp.onreadystatechange = async function() { if (this.readyState == 4 && this.status == 200){
    // use the response object to initiate appropriate calls to syncData
    // console.log(this.responseText);
    // console.log("response from md5_SyncCheck:");
    let mismatches = false;
    try{ mismatches = JSON.parse(this.responseText).mismatches; } catch(e){
      console.error(e); /* document.write(`<pre>${this.responseText}</pre>`); */ }
    if (mismatches){
      for (var HASHnUID in mismatches.net){
        let nUID = HASHnUID.replace(_uid, _);
        // console.log(_uid,nUID);
        if (nUID[0] === _)
          for (depth in mismatches.net[HASHnUID]) syncData("net", nUID, depth, await loadDelta("net", nUID, depth));
      }
      for (var HASHeUID in mismatches.entity){
        let eUID = HASHeUID.replace(_uid, _);
        // console.log(eUID);
        if (eUID[0] === _)
          for (depth in mismatches.entity[HASHeUID]) syncData("entity", eUID, depth, await loadDelta("entity", eUID, depth));
      }
    }
    // console.log("mismatches:", mismatches);
  }}
  xhttp.open("POST", syncPHP);
  xhttp.setRequestHeader("Content-Type", "application/json");
  if (type && uid){
    let hUID = insertHashID(uid);
    xhttp.send( JSON.stringify( {type:"md5Single", md5Table:{ type, uid:hUID, table:md5Table[type][hUID] }} ) );
  }else xhttp.send( JSON.stringify( {type:"md5Full", md5Table } ) );
}



// we're going to need to introduce the compass:
// paste a url into the compass to track a remote facet
// create a method here to take that url and set up local facets
// we'll start with strict master-slave (one saves, the other loads, no onionskin layers)
// 77b00b94fce953480552dbfdaa67bcc9.0.net
//
function syncAllRemoteFacets(){
  return 0;

  for( uid in nets ) syncRemoteFacet("net", uid);
  for( uid in entities ) syncRemoteFacet("entity", uid);
  setTimeout(remoteSyncCycle, 1000);
}

const remoteFacets = {net:{}, entity:{}};

// sync the local frozen-facet to the remote source-facet
function syncRemoteFacet(type, uid){
  return 0;

  if (uid[0]===_) return;
  if (uid.substr(0,32)===_uid) return;
  if (!remoteFacets[type][uid]) remoteFacets[type][uid] = {init:Date.now(), md5:{}, ready:true};

  // supply the md5 you got back last time.
  retrieveData(type, uid, remoteFacets[type][uid].md5["base"], "base", function(obj){
    if (obj.delta.noChange) return;

    remoteFacets[type][uid].md5["base"] = obj.md5;
    let baseDeltaSync = obj.delta;
    saveDelta(type, uid, "base", baseDeltaSync);
    // console.log({baseDeltaSync});
  });

  retrieveData(type, uid, remoteFacets[type][uid].md5["0"], "0", function(obj){
    if (obj.delta.noChange){
      remoteFacets[type][uid].ready = true;
      return;
    }

    remoteFacets[type][uid].md5["0"] = obj.md5;
    let newestDeltaSync = obj.delta;
    saveDelta(type, uid, "0", newestDeltaSync);

    // do this for each delta in ages.
    newestDeltaSync.ages[0] = 1; // just cuz
    let ages = Object.keys(newestDeltaSync.ages).sort(function(a,b){ return Number(a)>Number(b)?-1:1; })
    remoteFacets[type][uid].ready = ages.length;

    for(var d in ages){
      let depth = ages[d];

      retrieveData(type, uid, remoteFacets[type][uid].md5[depth], depth, function(obj){
        remoteFacets[type][uid].ready--;

        if (!obj.delta.noChange){
          remoteFacets[type][uid].md5[depth] = obj.md5;
          let currentDeltaSync = obj.delta;
          saveDelta(type, uid, depth, currentDeltaSync);
        }

        if (remoteFacets[type][uid].ready===0){
          remoteFacets[type][uid].ready = true;
          // modify the existing object with the constructor object
          if (type==="net") modifyNet(uid, loadNetFromFile(uid, "return"))
          if (type==="entity") modifyEntity(uid, getEntityConstructorFromFile(uid));
        }
        // console.log({currentDeltaSync});
      });
    }
  });
}

function remoteSyncCycle(){
  return 0;

  for (var type in {net:null, entity:null}) {
    for (var uid in remoteFacets[type]) {
      if (remoteFacets[type][uid].ready===true) {
        remoteFacets[type][uid].ready = false;
        delay = 0;//Date.now()-remoteFacets[type][uid].init; // fibonacci
        setTimeout(function(params){ syncRemoteFacet(params.type, params.uid) }, delay, {type, uid});
        // console.log(`updating remotefacet: ${uid}, in ${Math.round(delay/100)/10} seconds.`)
      }
    }
  }
  setTimeout(remoteSyncCycle, 1000);
}

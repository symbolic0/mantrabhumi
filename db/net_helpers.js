function loadEntitiesFromList(parentNet){
  let q = 0; // stagger loading by 50ms

  const ordered = {};
  Object.keys(parentNet.entities).sort(function(a, b){
    return parentNet.entities[a].z > parentNet.entities[b].z ? 1 : -1;
  }).forEach(function(key) { ordered[key] = parentNet.entities[key]; });

  if (!Object.keys(ordered).length){ netLoadCount--; return; }
  for(var i in ordered){
    setTimeout(function(params){
      // console.log({creator:params.n.uid});
      // console.log({uid:params.uid});
      // if (params.n.uid[0]!==_){
      //   params.uid = insertHashID(params.uid, params.n.uid.substr(32));
      //   console.log({uid:params.uid});
      // }

      // "first-come, first-create" for entities in multiple nets
      if (!entities[params.uid]){
        entities[params.uid] = new Entity(
          params.uid,
          params.params,
          params.n.uid
        );
        // if (params.uid==="_.2") console.log(params);
        if (entities[params.uid].open) openEntity(params.uid);
        else if (!entityDeltas[params.uid]) entityDeltas[params.uid] = [params.params];

      }
      // netLoadCount--;
      if (params.n.name!=_) drawLink(params.n, entities[params.uid]);
      setTimeout(function(e){ updateLinks(entities[e]) }, 0, params.uid);
    }, q+=50,{n:parentNet, uid:i, params:parentNet.entities[i]});
  }
  setTimeout(function(){ netLoadCount-- }, q);
}

function unloadEntitiesFromList(parentNet){
  let q = 0; // stagger loading by 50ms
  for(var i in parentNet.entities){
    if (!entities[i]) continue;
    setTimeout(function(uid){
      removeEntity(entities[uid]);
    }, 250 + q, i);
    q+=50;
    entities[i].container.style.transition = "opacity .5s";
    entities[i].container.style.opacity = 0;
  }
  // setTimeout(function(n){ clearLinks(n) }, 250 + q, parentNet);
  clearLinks(parentNet);
}

function renameNet(n, newName){
  if (newName === n.name) return;
  if (!newName){
    n.label.innerText = n.name;
    return;
  }

  n.name = newName;
  store(n, {name:newName}, "renameNet");
}

function removeNet(n){
  let n_n = `${n.name}`;

  delete nets[n.name];
  entityBox.removeChild(n.container);
  // remove links
}

function getEntityListConstructors(selection){
  let result = {};
  for (var i in selection) result[selection[i].uid] = getEntityConstructor(selection[i]);
  // console.log(result);
  return result;
}

function getEntityConstructor(e){
  if (!e) return {};
  return {
    name: e.name,
    open: e.open,
    x:  Math.round(e.x),
    y:  Math.round(e.y),
    dx: Math.round(e.dx),
    dy: Math.round(e.dy)
  };
}

function drawLink(a, b, erase /*, reverse*/, glow){
  // console.log(drawLink.caller, a.uid, b.uid);
  if (!a || !b) return;
  if (a.uid[0] !== b.uid[0]) return;
  // if (a===b && !erase) return;

  let aType = undefined;
  let bType = undefined;
  if (a.constructor) aType = a.constructor.name; // "Net" or "Entity"
  if (b.constructor) bType = b.constructor.name;

  if ( (aType === "Entity" && a.uid === activeEntityUID)
    || (bType === "Entity" && b.uid === activeEntityUID) ) glow = 100;

  // handle dynamic linking case

  // if (reverse){ let c = a; a = b; b = c; }
  if (aType === "Entity" && bType === "Net"){
    let c = a; a = b; b = c;
    aType = "Net";
    bType = "Entity";
  }
  if (!a || !b) return;
  let id = `${a.uid || a.name}.${b.uid || b.name}`;

  if (!a.links) a.links = {};
  if (!b.links) b.links = {};
  let line = a.links[id];
  if(!erase && !line) {
    line = a.links[id] = b.links[id] = document.createElement("div");
    a.links[id].a = a;
    a.links[id].b = b;
    b.links[id].a = a;
    b.links[id].b = b;
    let remoteClass = (a.uid[0]===_ || b.uid[0]===_) ? "" : " REMOTE";
    line.setAttribute("class", "line" + remoteClass);
    linkBox.append(line);
  }

  if (erase===true){
    try{
      if(typeof line != "undefined") linkBox.removeChild(line);
      delete a.links[id];
      delete b.links[id];
      // nets._.container.style.display = "none";
      nets._.dot0.style.opacity = 0;
      setTimeout(function(){ nets._.container.style.display = "" }, 1250);
    } catch(er){
      console.warn("Couldn't remove line element:", {line:line});
    }
    return;
  }

  // adjust entity coordinates with atomPane offsets
  let ax = a.x-9;
  let ay = a.y-8;
  let bx = b.x-9;
  let by = b.y-8;

  if (aType==="Entity"){
    ax += a.atomPane.offsetLeft;
    ay += a.atomPane.offsetTop;
  }

  if (bType==="Entity"){
    bx += b.atomPane.offsetLeft;
    by += b.atomPane.offsetTop;
  }

  let dx = (bx-ax);
  let dy = (by-ay);
  let theta = Math.atan2(dy, dx);
  let w = Math.hypot(bx - ax, by - ay);
  // midPointBias shall reflect the amount of atom pane in front of it.
  //
  // if one is encompassed by the other's pane, it defaults to .5:
  //    _________________
  //   │⬤_entA__________│
  //   │ \               │
  //   │  *              │
  //   │   \___________  │
  //   │  │⬤_entB_____│ │
  //   │                 │
  //   └─────────────────┘
  //
  //
  // bias will be high (towards b):
  //    __________       __________
  //   │⬤_entA___│--*--│⬤_entB___│
  //
  // bias will be low (towards a):
  //    __________       __________
  //   │⬤_entB___│--*--│⬤_entA___│
  //
  // bias will be centered at .5:
  //    __________
  //   │⬤_entA___│
  //    |
  //    *
  //    |_________
  //   │⬤_entB___│
  //

  // |<------------dx----------->|
  // |<---w--->|        *        |
  // |<------------m--->|

  // mx = w + (dx - w)/2
  // bias = m/dx

  let midPointBias = .5;
  if (a.atomPane && ax < bx && ay < by + 44){
    let m = a.atomPane.offsetWidth + (dx - a.atomPane.offsetWidth)/2 - 8;
    midPointBias = m/dx;
  }
  if (b.atomPane && bx < ax && by < ay + 44){
    let m = b.atomPane.offsetWidth + (dx - b.atomPane.offsetWidth)/2 - 8;
    midPointBias = m/dx;
  }

  let mx = ax+dx*midPointBias;
  let my = ay+dy*midPointBias;

  if (!glow){ glow = 0 }

  line.id = id;
  line.style.left = `${ax+20}px`;
  line.style.top = `${ay+20}px`;
  line.style.width = `${w}px`;
  line.style.transform = `rotate(${theta}rad)`;
  line.style["border-color"] = `rgba(${[108,108,108,.2+.7*glow/108]})`;

  nets._.container.style.display = "none";
  if (aType==="Net" && a.name===_) {
    nets._.container.style.display = "";
    nets._.container.style.left = `${ax+7}px`;
    nets._.container.style.top = `${ay+10}px`;
    nets._.dot0.style.opacity = 1;
  }
  else if (bType==="Net" && b.name===_) {
    nets._.container.style.display = "";
    nets._.container.style.left = `${bx+7}px`;
    nets._.container.style.top = `${by+10}px`;
    nets._.dot0.style.opacity = 1;
    return {x:bx+7, y:by+10};
  }
  else if (aType!==bType){
    nets._.dot0.style.opacity = 0;
  }
  else if (aType==="Entity" && bType==="Entity") {
    nets._.container.style.display = "";
    nets._.container.style.left = `${mx+7}px`;
    nets._.container.style.top = `${my+10}px`;
    nets._.dot0.style.opacity = 1;
    return {x:mx+7, y:my+10};
  }
}

function updateLinks(obj){
  if (!obj) return;
  if (obj.constructor) objType = obj.constructor.name; // "Net" or "Entity"

  if (objType === "Net") {
    for (i in obj.links) {
      let l = obj.links[i];
      // console.log(l.a.uid[0], l.b.uid[0], obj.uid[0]);
      if (l.a.uid[0]===l.b.uid[0] && l.a.uid[0]===obj.uid[0]) drawLink(l.a, l.b);
    }
  }else if (objType === "Entity") {
    for (var i in obj.nets) updateLinks(nets[i]);
  }
}

function clearLinks(net){
  if (!net) return;
  // erase dead links. i.e. on net-hide
  for (i in net.links) {
    let l = net.links[i];
    linkBox.removeChild(l);
    delete net.links[i];
  }
}

function getNewNetUID(){
  let i = 0;
  let testUID = `_.${i}.net`;
  while(true){
    if(!fileSystemAccessToken.existsSync(`nets/${testUID}`)) return testUID;
    else if(!fileSystemAccessToken.lstatSync(`nets/${testUID}`).isDirectory()) return testUID;
    testUID = `_.${++i}.net`;
  }
}

function getNewNetName(){
  let nameTemplate = "untitledNet";
  let count = 0;
  let takenNumbers = [];
  for(var i in nets){
    netName = nets[i].name;
    if (netName.substr(0, nameTemplate.length)===nameTemplate){
      postFix = netName.substr(nameTemplate.length);
      if (!isNaN(postFix)) takenNumbers[Number(postFix)] = true;
    }
  }

  for (var i=0; i<takenNumbers.length; i++){
    if (takenNumbers[i]) count++;
    else break;
  }
  return nameTemplate + (count);
}

async function modifyEntity(eUID, c){ // c for constructor
  const e = entities[eUID];
  for ( key in c ){
    if (key==="uid") continue;
    if (key==="open"){
      if (e.open != c.open) await toggleEntity(eUID);
      // if (!e.open && c.open) loadEntitiesFromList(n);
    }
    if (key==="atoms"){
      for ( aID in c.atoms ){
        if (!entities[eUID]) continue;
        entities[eUID].atomPane.style.transition = `width .25s, height .25s, left .25s, top .25s`;
        if (!atoms[eUID][aID].node) continue;
        atoms[eUID][aID].node.style.transition = "top .25s, left .25s";
        atoms[eUID][aID].text.style.transition = `width .25s, height .25s`;
        moveSelection(atoms[eUID][aID], c.atoms[aID].x, c.atoms[aID].y);
        atoms[eUID][aID].text.style.width = `${c.atoms[aID].txtW}px`;
        atoms[eUID][aID].text.style.height = `${c.atoms[aID].txtH}px`;
        // if (atoms[eUID][aID].ed !== c.atoms[aID].ed) atoms[eUID][aID].node.ondblclick({target:atoms[eUID][aID].node});

        for (let delay=50; delay<=300; delay+=50)
          setTimeout(function(params){ alignEditor(atoms[params.eUID][params.aID]) }, delay, {eUID, aID});
        setTimeout(function(e){ checkEntityBounds(e) }, 350, e);
      }
    }
    e[key] = c[key];
  }
  moveEntity(e, "smooth");
  e.label.innerText = e.name;

  updateLinks(e);
  checkEntityBounds(e);
}

function modifyNet(nUID, c){ // c for constructor
  const n = nets[nUID];
  const hUID = nUID.substr(0,32);
  for ( key in c ){
    if (key==="uid") continue; // { n[key] = insertHashID(c[key], hUID); continue; }
    if (key==="open"){
      // if (n.open && !c.open) unloadEntitiesFromList(n);
      // if (!n.open && c.open) loadEntitiesFromList(n);
    }
    if (key==="entities"){
      n.entities = {};
      for ( eUID in c.entities ) n.entities[insertHashID(eUID), hUID] = c.entities[eUID];
      // console.log(
      continue;
    }
    n[key] = c[key];
  }
  n.container.style.transition = "left .25s, top .25s";
  n.container.style.left = `${n.x}px`;
  n.container.style.top = `${n.y}px`;
  n.label.innerText = n.name;

  updateLinks(n);
}

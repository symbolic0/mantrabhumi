<?php
  $d = $_GET["d"];
  $nets = array_filter(scandir($d), function($item) {
    return $item !== '.' && $item !== '..';
  });
  $jsonString = json_encode($nets);//, JSON_PRETTY_PRINT | JSON_FORCE_OBJECT);
  echo $jsonString;
?>

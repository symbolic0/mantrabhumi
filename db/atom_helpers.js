let topAtomIndex = 0;
let MAC = navigator.platform.toUpperCase().indexOf('MAC')>=0;;
let clipBoard = undefined;
const atomPanePadding = 72;

// function saveAtoms(entityUID){
//   if (!entityUID) entityUID = activeEntityUID
//   if (!atoms[entityUID]) return;
//   if (!entities[entityUID].open) return;
//   if (!Object.keys(atoms[entityUID]).length) return;
//   store(entities[entityUID], {atoms:atoms[entityUID]});
// }

function getNewAtomID(){
  return Object.keys(atoms[activeEntityUID]).length;

  // let testID = 0;
  // while(true){
  //   if(!atoms[testID]) return testUID;
  //   else testUID++;
  // }
}

function getNewAtomName(){
  let aList = {};
  for (var id in atoms[activeEntityUID]) aList[atoms[activeEntityUID][id].name] = true;
  let i = 0;
  let aName = `#${i}`;
  while (aList[aName]) aName = `#${++i}`;
  return aName;
}

function generateAtomConstructors(entityUID){
  let atomsObject = atoms[entityUID];
  if (!atomsObject) return false;
  if (!Object.keys(atomsObject).length) return false;
  let constructors = {};
  for (var id in atomsObject){
    let a = atomsObject[id];
    constructors[id] = {
      id:a.id,
      name:a.name,
      x:a.x,
      y:a.y,
      z:a.z,
      rx:a.rx,
      ry:a.ry,
      rz:a.rz,
      ed:a.editing,
      code:a.code,
      txt:a.text,
      img:a.img
      // txtW:a.text.style.width,
      // txtH:a.text.style.height
    };
  }
  return constructors;
}

function setActiveAtom(a){
  if (activeAtom === a) return;
  if (activeAtom){
    let previousActiveAtom = activeAtom;
    activeAtom = undefined;
    moveSelection(previousActiveAtom, previousActiveAtom.x, previousActiveAtom.y);
    // previousActiveAtom.node.setAttribute("class", "node");
  }
  if (!a) { activeAtom = undefined; return; }
  activeAtom = a;
  // activeAtom.node.setAttribute("class", "node active-node");
}

function keyShortcuts(e, txt){
  let commandKey = ( MAC? (e.metaKey && !e.altKey) : (e.altKey && !e.metaKey) );

  // run script
  if (commandKey && e.key==="Enter") {
    // activeAtom.play.onclick();
  }

  // copy/cut/paste/duplicate line
  if (
    txt.selectionStart == txt.selectionEnd &&
    ( (commandKey && ["c", "x", "v", "d"].indexOf(e.key)!=-1)
    || (e.ctrlKey && ["ArrowUp","ArrowDown"].indexOf(e.key)!=-1) )
  ) {
    let lines = txt.value.split('\n');
    let b = txt.selectionStart;
    let a = 0;
    let l = 0;
    while (a<=b && l<lines.length) a+=lines[l++].length+1;
    l--;
    let clip = lines[l];

    if (commandKey && !e.ctrlKey) {
      if (e.key == "c"){
        clipBoard = clip;
        navigator.clipboard.writeText(clip + "\n");
      }
      if (e.key == "v" && clipBoard) txt.selectionStart = txt.selectionEnd = a - clip.length - 1;
      if (e.key == "d"){
        lines = lines.slice(0,l).concat(clip).concat(lines.slice(l));
        txt.value = lines.join('\n');
        txt.selectionStart = txt.selectionEnd = b + clip.length + 1;
      }
      if (e.key == "x"){
        clipBoard = clip;
        navigator.clipboard.writeText(clip + "\n");
        lines = lines.slice(0,l).concat(lines.slice(l+1));
        txt.value = lines.join('\n');
        txt.selectionStart = txt.selectionEnd = a - clip.length - 1;
      }
    }

    else if (e.ctrlKey) {
      if (e.key == "ArrowUp" && l>0){
        clip = lines[l-1];
        lines = lines.slice(0,l-1).concat(lines[l]).concat(lines[l-1]).concat(lines.slice(l+1));
        txt.value = lines.join('\n');
        txt.selectionStart = txt.selectionEnd = b - clip.length - 1;
      }
      if (e.key == "ArrowDown" && l<lines.length-1){
        clip = lines[l+1];
        lines = lines.slice(0,l).concat(lines[l+1]).concat(lines[l]).concat(lines.slice(l+2));
        txt.value = lines.join('\n');
        txt.selectionStart = txt.selectionEnd = b + clip.length + 1;
      }
    }
  }
  else if ((MAC?e.metaKey:e.ctrlKey) && "cx".indexOf(e.key)!=-1) clipBoard = undefined;

  // comment/uncomment single line (EASILY CAN MAKE THIS WORK ON A SELECTION RANGE)
  // break into lines
  // advance one line at a time adding line-length to p
  // push lines and prepend "//" if...
  //   if ( (p > s && s < p+line-length) || (s < p && p <= e) ) lines[l] = `// ${lines[l]}`;
  if ((MAC?e.metaKey:e.ctrlKey) && e.key==="/") {
    t = txt.value
    let lines = txt.value.split("\n");
    let s = txt.selectionStart;
    let e = txt.selectionEnd;
    let offset = 0;
    let l=0;
    let p=0;
    while (p<=s){ p+=lines[l++].length+1; }
    p-=lines[--l].length-1;
    if (t.slice(p-2,p+1) === "// "){ offset=-3; txt.value = t.slice(0, p-2) + t.slice(p+1); }
    else{ offset=3; txt.value = t.slice(0, p-2) + "// " + t.slice(p-2); }
    txt.selectionStart = s+offset;
    txt.selectionEnd = e+offset;
  }
  setTimeout(function(t){ t.onkeydown({keyShortcutsComplete:true}); }, 0, txt);
}

function removeComments(s){
  let lines = s.split("\n");
  let output = [];
  for (let i=0; i<lines.length; i++){
    if (lines[i].trim().substr(0,2) == "//") continue;
    output.push(lines[i]);
  }
  return output.join("\n");
}

function moveSelection(a, x, y, z){
  if (a === undefined) a = selection;

  if (!a) return;
  // a.node.style.left = `${x}px`;
  // a.node.style.top  = `${y}px`;
  a.x = x;
  a.y = y;
  a.z = z;

  for (var i in a.bonds){
    let target = atoms[a.entityUID][a.bonds[i].id]; // again ignoring inter-entity bonds
    drawBond(a, target, "update", a.bonds[i].reverse, false, a.entityUID); // glow=false
  }

  updateEntityBounds(a);
}

function bringAtomToTop(a, z){
  a.z = z;
  return;

  // z = z || ++topAtomIndex;
  // // a.node.style.zIndex = z;
  // a.z = z;
  // topAtomIndex = Math.max(z, topAtomIndex);
}

function alignEditor(a){
  a.lineNumbers.style.height =
  a.syntax.style.height = `${a.text.offsetHeight - 22}px`;
  a.syntax.style.width = `${a.text.offsetWidth - 22}px`;
  a.lineNumbers.scrollTop =
  a.syntax.scrollTop = a.text.scrollTop;
  a.syntax.scrollLeft = a.text.scrollLeft;
}

function drawBond(a, b, mode, reverse, glow, entityUID){
  let REMOTE = false;
  if (entityUID) REMOTE = entityUID[0]!==_;
  if (!entityUID) entityUID = activeEntityUID;
  if (!REMOTE && entityUID!==activeEntityUID) return; // prevent timed action potential after entity close
  if (reverse){ let c = a; a = b; b = c; }
  if (!a || !b) return;
  if (!mode) mode = "update";
  let id = `${a.id}.${b.id}`;
  var lineA;
  var lineB;

  let e = entities[entityUID];
  if (!e.bonds) e.bonds = {};

  if (mode==="create"
  || (mode==="detect" && (!e.bonds[id+".a"] || !e.bonds[id+".b"]))) {
    if (!e.bonds[id+".a"]) {
      e.bonds[id+".a"] = document.createElement("div");
      e.bonds[id+".a"].setAttribute("class", "line");
      e.bondBox.append(e.bonds[id+".a"]);
    }
    if (!e.bonds[id+".b"]) {
      e.bonds[id+".b"] = document.createElement("div");
      e.bonds[id+".b"].setAttribute("class", "line");
      e.bondBox.append(e.bonds[id+".b"]);
    }
    mode = "update";
  }

  if (mode==="erase") {
    try{
      e.bondBox.removeChild(e.bonds[id+".a"]);
      e.bondBox.removeChild(e.bonds[id+".b"]);
      delete e.bonds[id+".a"];
      delete e.bonds[id+".b"];
      if (b.bonds) if (b.bonds[a.id]) delete b.bonds[a.id];
    } catch(er){
      console.warn("Couldn't remove line element:", {entity:entityUID, id}, `${a.name}—>${b.name}`);
    }
    return;
  }

  if (mode==="update" || mode==="detect") {
    lineA = e.bonds[id+".a"];
    lineB = e.bonds[id+".b"];
  }

  if (!lineA || !lineB) return;

  let dx = (b.x-a.x);
  let dy = (b.y-a.y);
  let theta = Math.atan2(dy, dx) + Math.PI/2;
  let mx = a.x+dx/2- Math.cos(theta)*10;
  let my = a.y+dy/2- Math.sin(theta)*10;
  let ay = a.y;
  let by = b.y;

  if (a.x == b.x && a.y == b.y){
    mx = a.x-40;
    my = a.y;
    ay = a.y-10;
    by = b.y+10;
  }

  let w = Math.hypot(mx - a.x, my - ay);
  let thetaA = Math.atan2(my - ay, mx - a.x);
  let thetaB = Math.atan2(by - my, b.x - mx);

  if (!glow){ glowA = glowB = .1 }
  else{ glowA = glow.a; glowB = glow.b; }

  let rgba_A = [];
  let rgba_B = [];
  if (a===activeAtom){
    rgba_A = [255, 155, 55, .5+.5*glowA/108];
    rgba_B = [255, 155, 55, .2+.8*glowB/108];
  }else{
    rgba_A = [55+glowA*2, 55+glowA, 155-glowA, .5+.5*glowA/108];
    rgba_B = [55+glowB*2, 55+glowB, 155-glowB, .2+.8*glowB/108];
  }

  lineA.id = id+".a";
  lineA.style.left = `${a.x+20}px`;
  lineA.style.top = `${ay+20}px`;
  lineA.style.width = `${w}px`;
  lineA.style.transform = `rotate(${thetaA}rad)`;
  lineA.style["border-color"] = `rgba(${rgba_A})`;
  // lineA.style["box-shadow"] = `${Math.cos(thetaA+Math.PI/2)*8}px ${Math.sin(thetaA+Math.PI/2)*8}px 16px rgba(15,0,45,1)`;

  lineB.id = id+".b";
  lineB.style.left = `${mx+20}px`;
  lineB.style.top = `${my+20}px`;
  lineB.style.width = `${w}px`;
  lineB.style.transform = `rotate(${thetaB}rad)`;
  lineB.style["border-color"] = `rgba(${rgba_B})`;
  // lineB.style["box-shadow"] = `${Math.cos(thetaB+Math.PI/2)*8}px ${Math.sin(thetaB+Math.PI/2)*8}px 16px rgba(45,0,15,1)`;
}

function updateBonds(a){
  // erase and delete all outgoing bonds and delete corresponding reverse bonds in targets
  for (var i in a.bonds){
    if (a.bonds[i].reverse) continue;
    let target = atoms[a.entityUID][a.bonds[i].id]; // again ignoring inter-entity bonds
    drawBond(a, target, "erase");
    delete target.bonds[a.id];
    delete a.bonds[i];
  }

  // recreate outgoing bonds from text
  let bondStrings = removeComments(a.text).match(/(^|\s|\n)send\s+.+\s+to\s+.+/g);
  for (var i in bondStrings){
    let bStr = bondStrings[i];
    if (!bStr) continue;
    try{ // we're not handling inter-entity bonds yet (so "in" is ignored)
      // let b = eval( bStr.match(/(?:^|\s*|\n)send\s+.+\s+to\s+(.+)/)[1].trim() ); // remove explicit quote marks
      let b = bStr.match(/(?:^|\s*|\n)send\s+.+\s+to\s+(.+)/)[1].trim();
      if (b[0]==='"') b = b.substr(1);
      if (b[b.length-1]==='"') b = b.substr(0,b.length-1);
      a.bonds[b] = {id:b, reverse:false};
    } catch(e){ console.warn("Bond id wasn't computed yet:\n", {bond:bStr, entity:a.entityName, atom:a.id}); }
  }

  // redraw all bonds from bond dictionary and add/replace reverse bonds to bond targets
  for (var i in a.bonds){
    let target = atoms[a.entityUID][a.bonds[i].id]; // again ignoring inter-entity bonds
    if (!target){ delete a.bonds[i]; continue; }
    if (!a.bonds[i].reverse) target.bonds[a.id] = {id:a.id, reverse:true};
    drawBond(a, target, "detect", a.bonds[i].reverse, false, a.entityUID);
  }
}

function send(msg, atom, ent, src){
  if (!ent) ent = activeEntityUID
  let dst = atoms[ent][atom];
  if (!dst) return;
  dst.inbox.push(msg); // should include src!
  if (src){
    src.bonds[`${dst.id}`] = {id:dst.id, reverse:false};
    dst.bonds[`${src.id}`] = {id:src.id, reverse:true};
    let me = ent===activeEntityUID
    if (me) drawBond(src, dst, "detect", false, {a:50,b: 0}, ent);
    setTimeout(function(src, dst, ent){ if (me) drawBond(src, dst, "update", false, {a:100,b: 20}, ent); }, 100, src, dst, ent);
    setTimeout(function(src, dst, ent){ if (me) drawBond(src, dst, "update", false, {a: 20,b:100}, ent); }, 200, src, dst, ent);
    setTimeout(function(src, dst, ent){ if (me) drawBond(src, dst, "update", false, {a:  0,b:  0}, ent); }, 300, src, dst, ent);
  }
}

function parse(s){
  // console.log(s.substr(0,5));
  if (s.substr(0,5)==="abort") return;

  if(!s) return " ";
  // s = s.replace(/require/g, "r e q u i r e");
  // s = s.replace(/fileSystemAccessToken/g, "f i l e S y s t e m A c c e s s T o k e n");

  // fileSystemAccessToken should be rolling through HASHes
  //   then somehow share them like a lease to an entity with adoption rules
  // s = s.replace(/fs\./g, "fileSystemAccessToken.");

  s = removeComments(s).split("\n");
  for (var i in s){
    s[i] = (s[i]
      .replace(/(^|\W)(\s*)send\s+(.+)\s+to\s+(.+)\s+in\s+(.+)/g,
        function(m, _0, pad, a, b, c, _){ return `${_0}${pad}send(${a}, ${b}, ${c}, $SELF);` } )
      .replace(/(^|\W)(\s*)send\s+(.+)\s+to\s+(.+)/g,
        function(m, _0, pad, a, b, _){ return `${_0}${pad}send(${a}, ${b}, "$SELFENTITYNAME", $SELF);` } )
      .replace(/this/g, "self")
    );
  } return s.join("\n");
}

function functionifyScript(a){
  if (a.code.substr(0,5)==="abort") return;
  // console.log(a.code.value.substr(0,5));

  let selfString = `atoms["${a.entityUID}"]["${a.id}"]`;
  try{
    a.script = new Function(
      "self, msg",
      parse(a.code)
        .replace(/\$SELFENTITYNAME/g, a.entityUID)
        .replace(/\$SELF/g, selfString)
    );
  }
  catch(e){ console.warn(e, "@", {entity:a.entityUID, atom:a.id}); }
}

function numberList(txt){
  let n = 1;
  let newLines = false;
  if(txt) newLines = txt.match(/\n/g);
  if (newLines) n = newLines.length + 1;
  let ln = [];
  // for(var i=0; i<n; i++) ln.push(`${i<10?" ":""}${i<100?" ":""}${i}`)
  for(var i=1; i<=n; i++) ln.push(`${i<10?" ":""}${i}`)
  return ln.join("\n");
}

function highlight(s){
  // if (s.substr(0,5)==="abort") return s;

  if(!s) return "";

  let o=""; // output
  let q=[]; // quotes
  let p=0;  // position
  for (var i=0; i<s.length; i++){ if(s[i].match(/'|"|`/)) q.push(i); } // ` treated specially. now '/" end each other
  for (var qi=0; qi<q.length; qi+=2){
    o+=formatSegment(s.slice(p,q[qi])) + '~GREEN~' + s.slice(q[qi],q[qi+1]+1).replace(/\/\//g, "~DBLSLASH~") + '~~';
    p = q[qi+1]+1;
  } if(!(q.length % 2)) o+=formatSegment(s.slice(p));

  // other stuff
  function formatSegment(s){
    // s = s.replace(/([a-z]+\_*\d*)+(\s*)\:/ig, function(m,a,b){ return '~RED~'+a+'~~'+b+':'; } );
    // return s;
    return (s
      .replace(/(?<!\/)\/(?!\/)/g, function(m,a){ return '~CYAN~'+m+'~~'; } )
      .replace(/(\W)([0-9]*\.{0,1}[0-9]+|true|false|undefined|null)/g, function(m,a,b){ return a+'~GOLD~'+b+'~~'; } )
      .replace(/(\W|^)(function|return|for|to|in|continue|break|if|else|do|while|switch|case|var|let|const|delete|try|catch|throw|new|typeof)(\W)/g,
        function(m,a,b,c){ return a+'~MAGENTA~'+b+'~~'+c; } )
      .replace(/(\W|^)(send|inherit from)(\W)/g, function(m,a,b,c){ return a+'~BLUE~'+b+'~~'+c; } )
      .replace(/([a-z]+_*\d*)+(\s*)\(/ig, function(m,a,b){ return '~BLUE~'+a+'~~'+b+'('; } )
      // .replace(/([a-z]+_*\d*)+(\s*)\:/ig, function(m,a,b){ return '~RED~'+a+'~~'+b+':'; } )
      .replace(/\.([a-z]+_*\d*)+/ig, function(m,a){ return '.~RED~'+a+'~~'; } )
      .replace(/(\W|^)(document|this|abort)(\W)/g, function(m,a,b,c){ return a+'~RED~'+b+'~~'+c; } )
      .replace(/(\W|^)(?<!~)(([A-Z]+([a-z]*_*\d*)*))/g, function(m,a,b){ return a+'~YELLOW~'+b+'~~'; } )
      .replace(/(\-|\=|\+|\:|\!|\?|\<|\>|\\|\||\&|\%|\*)+/g, function(m,a){ return '~CYAN~'+m+'~~'; } )
      .replace(/(\(|\[|\{|\}|\]|\)|\.)/g, function(m,a){ return '~WHITE~'+a+'~~'; } )
    );
  }

  function killColors(s){ return s.replace(/(~[A-Z]*~|~~)/g, ""); }

  return (o
    .replace(/</g, "&lt;").replace(/>/g, "&gt;")
    .replace(/(\/\/.*)/g, function(m,a){ return '~GRAY~'+killColors(a)+'~~'; } )
    .replace(/~GREEN~/g, color(colors.green))
    .replace(/~CYAN~/g, color(colors.cyan))
    .replace(/~GOLD~/g, color(colors.gold))
    .replace(/~MAGENTA~/g, color(colors.magenta))
    .replace(/~BLUE~/g, color(colors.blue))
    .replace(/~RED~/g, color(colors.red))
    .replace(/~YELLOW~/g, color(colors.yellow))
    .replace(/~WHITE~/g, color(colors.white))
    .replace(/~GRAY~/g, color(colors.gray))
    .replace(/~DBLSLASH~/g, "//")
    .replace(/~~/g, uncolor())
  ) + " ";
}
function color(c){ return `<span style="color:rgba(${c},.8)">`; }
function uncolor(){ return `</span>`; }
function getCSScolors(sheet){
  colorSheet = document.styleSheets[sheet].rules;
  colorValues = {
    green:true,
    cyan:true,
    gold:true,
    magenta:true,
    blue:true,
    red:true,
    yellow:true,
    white:true,
    gray:true
  }
  for (var i=0; i<colorSheet.length; i++){
    let selector = colorSheet[i].selectorText.substr(1);
    if ( colorValues[selector] ) {
      colorValues[selector] = colorSheet[i].style.color.replace(/rgb|\(|\)/g, "");
    }
  }
  return colorValues;
}

let colors = getCSScolors(localStorage.lightmode==="true" ? 1 : 0);

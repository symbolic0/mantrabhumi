/*
import { Pointcloud } from './pointcloud.js';

window.kbCanvas=false;
window.keyRegistry = {};
window.floof=true;

const singleKeys = [
  null,
  "QWERTYUIOP",
  "ASDFGHJKL",
  "ZXCVBNM"
];

const dualKeys = [
  ["`1234567890-=", "~!@#$%^&*()_+"],
  ["[]\\", "{}|"],
  [";'", ":\""],
  [",./", "<>?"]
];

const fontSize = 20;
const keySize = fontSize*3;
const keyGap = keySize * .1;
const keyRadius = 5;
// let kbScale = 2;

const click_hi = new Audio('wav/click-hi.wav');
const click_lo = new Audio('wav/click-lo.wav');
const click_mid = new Audio('wav/click-mid.wav');

function drawKeyboard(kb){
  if (!kbCanvas){
    console.log("creating keyboard canvas");
    kbCanvas = document.createElement('canvas');
    window.kbCTX = kbCanvas.getContext('2d');

    let h = 8, v = 3;
    kbCanvas.width=128*h;
    kbCanvas.height=128*v;

    if(false){ // true = visualize kb canvas
      document.body.append(kbCanvas);
      kbCanvas.style.position="absolute";
      kbCanvas.style.left="0px";
      kbCanvas.style.top="0px";
      kbCanvas.style.border="solid rgba(255,255,255,.1) 1px";
    }

    kbCTX.fillStyle = "rgba(255,255,255,.5)";
    kbCTX.strokeStyle = "rgba(255,255,255,.1)";

    let x, y, tx;
    const d = keySize;
    const r = keyRadius;
    const kbScale = 1.5  //vr? 3 : 1;

    // convert this to generalized rounded rectangle function
    function drawKey(k, x,y, w, align){
      if (!align) align = "center";
      kbCTX.textAlign = align;

      x = (0.5+x+y/2) * (keySize+keyGap);
      y = (1.5+  y  ) * (keySize+keyGap);
      w *= d;
      kbCTX.beginPath();
      kbCTX.arc(x+r  , y  -r, r, Math.PI/2  , Math.PI    ); kbCTX.lineTo(x    , y-d+r);
      kbCTX.arc(x+r  , y-d+r, r, Math.PI    , Math.PI/2*3); kbCTX.lineTo(x+w-r, y-d  );
      kbCTX.arc(x+w-r, y-d+r, r, Math.PI/2*3, Math.PI*2  ); kbCTX.lineTo(x+w  , y  -r);
      kbCTX.arc(x+w-r, y  -r, r, Math.PI*2  , Math.PI/2*r); kbCTX.lineTo(x  +r, y    );
      kbCTX.stroke();
      if (align=="left"  ) tx =  keySize/3;
      if (align=="center") tx =  keySize/2;
      if (align=="right" ) tx = -keySize/3 + w;

      if (k.length == 2){
        kbCTX.font = `${fontSize*.75}px Helvetica`;
        kbCTX.fillText( k[1], x+tx, y-keySize/2-fontSize/3 );
        // kbCTX.font = `${fontSize*.75}px Helvetica`;
        kbCTX.fillText( k[0], x+tx, y-keySize/2+fontSize*.8 );
      }else{
        kbCTX.font = `${fontSize}px Helvetica`;
        kbCTX.fillText( k[0], x+tx, y-keySize/2+fontSize/3 );
      }

      // x/=128;
      // y/=128;
      // x-=h;
      // y-=v;
      let offset = -1;
      let data = {x,y,w,offset}
      keyRegistry[k[0].toUpperCase()] = data;
      if (k[1]) keyRegistry[k[1].toUpperCase()] = data;
      if (k[0]=="return") keyRegistry["ENTER"] = data;
      if (k[0]=="delete") keyRegistry["BACKSPACE"] = data;
      if (k[0]=="play script") keyRegistry["PLAY"] = data;
      if (k[0]=="left") keyRegistry["LEFT"] = data;
      if (k[0]=="right") keyRegistry["RIGHT"] = data;
      if (k[0]=="up") keyRegistry["UP"] = data;
      if (k[0]=="down") keyRegistry["DOWN"] = data;
      if (k[0]=="commit") keyRegistry["COMMIT"] = data;
      // if (k[0]=="←") keyRegistry["LEFT"] = data;
      // if (k[0]=="→") keyRegistry["RIGHT"] = data;
      // if (k[0]=="↑") keyRegistry["UP"] = data;
      // if (k[0]=="↓") keyRegistry["DOWN"] = data;

      if (k[0]=="shift" && align=="left") keyRegistry["LSHIFT"] = data;
      if (k[0]=="shift" && align=="right") keyRegistry["RSHIFT"] = data;
    }

    for (var l = 1; l<singleKeys.length; l++)
      for (var c = 0; c<singleKeys[l].length; c++)
        drawKey([singleKeys[l][c]], c+1, l, 1);

    for (var l = 0; l<dualKeys.length; l++)
      for (var c = 0; c<dualKeys[l][0].length; c++)
        drawKey([dualKeys[l][0][c], dualKeys[l][1][c]], c+[0,11,10,8][l], l, 1.0);

    let kGap = keyGap/keySize/2;

    drawKey(["commit", "✓"], -0.5, 1, 1.5 + kGap   , "left");
    drawKey(["play script", "▶"], -1.0, 2, 2.0 + kGap*2 , "left");
    drawKey(["shift",  ""], -1.5, 3, 2.5 + kGap*3 , "left");
    drawKey(["delete", ""], 13, 0, 1.5 , "right");
    drawKey(["return", ""], 12, 2, 1.5 , "right");
    drawKey(["shift",  ""], 11, 3, 2.0 , "right");
    drawKey([" "], 2.5, 4, 5 + kGap*8 , "center");
    drawKey(["left",  "←"],  7.6, 4, 1.2 , "center");
    drawKey(["right", "→"],  8.8, 4, 1.2 , "center");
    drawKey(["up",    "↑"], 10.0, 4, 1.2 , "center");
    drawKey(["down",  "↓"], 11.2, 4, 1.2 , "center");
    drawKey(["reload", "⟳"], -2.0, 4, 2.3 , "left");
    drawKey([`sync ${debug.report.version}`,  "⤒"],  0.2, 4, 2.3 , "left");

    h/=(32/kbScale);
    v/=(32/kbScale);

    const geometry = new THREE.BufferGeometry();
    const vertices = new Float32Array( [
       h, v, 0,
      -h, v, 0,
      -h,-v, 0,
       h,-v, 0
    ] );

    const uvs = new Float32Array( [
      0,0,
      1,0,
      1,1,
      0,1
    ] );

    const indices = new Uint32Array( [ 0, 2, 1, 0, 3, 2 ] );
    geometry.setAttribute( 'position' , new THREE.BufferAttribute( vertices, 3 ) );
    geometry.setAttribute( 'uv' , new THREE.BufferAttribute( uvs, 2 ) );
    geometry.setIndex( new THREE.BufferAttribute( indices, 1 ) );
    const texture = new THREE.Texture(kbCanvas);
    texture.needsUpdate = true;
    texture.wrapS = texture.wrapT = THREE.ClampToEdgeWrapping;
    texture.flipY = false;
    // texture.repeat.set(2,2);
    // texture.offset.set(.5,.2);
    const material = new THREE.MeshBasicMaterial( { map:texture, transparent:true } );
    material.side = THREE.DoubleSide;
    const mesh = new THREE.Mesh( geometry, material );
    mesh.rotation.x = Math.PI*3/4;
    mesh.rotation.y = Math.PI*0;
    mesh.rotation.z = Math.PI*1;
    // mesh.position.x = -.3*kbScale;

    window.kb = {
      mesh,
      affinity : new Pointcloud( {max: 1008, texture: 'h.png', blend:2, fog:[.005,.005,.005]} ),
      width : h*2,
      height : v*2,
      offsetX : 0,
      offsetY : 0,
      offsetZ : 0,
      scale : kbScale
    }
    // window.kb.mesh.add(debugMesh);
    // debugMesh.position.x = .4;
    // debugMesh.position.y = .7;

    // if(vr)
    window.kb.affinity.setpoint_preprocess_callback=function(params, affinity){
      if (!vr) return params;
      let a = affinity.getcol(params.i).a || 0;
      let fadeIncrement = .05;
      if(a < params.rgba[3]) params.rgba[3] = Math.min(params.rgba[3], a + fadeIncrement);
      if(a > params.rgba[3]) params.rgba[3] = Math.max(params.rgba[3], a - fadeIncrement/2);
      return params;
    }

    // if (!vr) {
    //   otherpoint[0].p.position.x = -.3;
    //   otherpoint[0].p.position.y = -.22;
    //   otherpoint[0].p.position.z = 0;
    //   otherpoint[0].p.rotation.x = -Math.PI/2;
    //   otherpoint[0].p.rotation.y = Math.PI;
    //   otherpoint[0].p.rotation.z = 0;
    // }

    // otherpoint[0].p.add(window.kb.mesh);
    zeropoint.p.add(window.kb.mesh);
    // otherpoint[0].run = KeyboardPointlcoudProgram
    // otherpoint[0].p.position.x=.2;
    window.kb.mesh.add(window.kb.affinity.p);
  }
  // other stuff ...
  // if you make changes to the wordCTX, call:
  // mesh.material.map.needsUpdate = true


  // we need to activate keys if a key is pressed on the real keyboard or the VR one.
  if (!document.onfocus){
    document.onfocus = function(){ activateKeyboard( "meta", 'deactivate' ); }
  }

  if (!document.onkeydown) {
    document.onkeydown = function(e){
      // console.log(e);
      let key = e.key;
      if (e.code=="ShiftLeft") key = "LShift";
      if (e.code=="ShiftRight") key = "RShift";
      if (e.code=="ArrowLeft") key = "Left";
      if (e.code=="ArrowRight") key = "Right";
      if (e.code=="ArrowUp") key = "Up";
      if (e.code=="ArrowDown") key = "Down";
      activateKeyboard( key );

      if (e.metaKey && e.key=='s') return false;
    }
  }

  if (!document.onkeyup) {
    document.onkeyup = function(e){
      let key = e.key;
      if (e.code=="ShiftLeft") key = "LShift";
      if (e.code=="ShiftRight") key = "RShift";
      if (e.code=="ArrowLeft") key = "Left";
      if (e.code=="ArrowRight") key = "Right";
      if (e.code=="ArrowUp") key = "Up";
      if (e.code=="ArrowDown") key = "Down";
      activateKeyboard( key , "deactivate" );

      if (e.metaKey && e.key=='s') return false;
    }
  }

  let FingerTip, TipLocal, k, d;
  let jointIndex = ['index-finger-tip']; // only index finger
  let threshold = keySize/2;

  const clist = [controller1, controller2];
  for (var c=0; c<=1; c++){
    if (!clist[c].userData.isHolding) continue;
    FingerTip = clist[c];
    // !!!!!!!!!!!!!!!!!!!!!!!!!
    TipLocal = window.kb.affinity.p.worldToLocal(
      new THREE.Vector3(
        FingerTip.position.x,
        FingerTip.position.y,
        FingerTip.position.z
      )
    );
    // project FingerTip onto kb :
    if ( (Math.abs(TipLocal.z-.06) < .05) // we've pierced the board
      && (Math.abs(TipLocal.x) < window.kb.width/2)
      && (Math.abs(TipLocal.y) < window.kb.height/2) // we're on the board
    ){

      TipLocal.x=(window.kb.width/2-TipLocal.x)*128*(32/window.kb.scale)/2;
      TipLocal.y=(window.kb.height/2-TipLocal.y)*128*(32/window.kb.scale)/2;

      for ( var key in keyRegistry ){
        k = keyRegistry[key];
        d = Math.hypot( k.x - TipLocal.x , k.y - TipLocal.y );
        if ( k.x > TipLocal.x - k.w && k.x < TipLocal.x
          && k.y > TipLocal.y       && k.y < TipLocal.y + keySize ){
          // debug.report[key]={press:true, offset:k.offset};
          activateKeyboard( key );
        }
      }
    }
  }

  if (!hands[0].joints['wrist']) return;

  for ( var key in keyRegistry ){
    // debug.report[key]={press:false, offset:keyRegistry[key].offset};
    activateKeyboard( key , "deactivate" );
  }

  // window.kb.affinity.setpoint( 1000 , [0,0,0], .1, [255,0,0, 1] );

  for (var h=0; h<=1; h++){
    for (var f=0; f<jointIndex.length; f++){
      FingerTip = hands[h].joints[jointIndex[f]];
      // !!!!!!!!!!!!!!!!!!!!!!!!!
      TipLocal = window.kb.affinity.p.worldToLocal(
        new THREE.Vector3(
          FingerTip.position.x,
          FingerTip.position.y,
          FingerTip.position.z
        )
      );
      // project FingerTip onto kb : -.1<z<0
      if ( -.01<TipLocal.z && TipLocal.z<.04 // we've pierced the board
        && (Math.abs(TipLocal.x) < window.kb.width/2)
        && (Math.abs(TipLocal.y) < window.kb.height/2) // we're on the board
      ){

        TipLocal.x=(window.kb.width/2-TipLocal.x)*128*(32/window.kb.scale)/2;
        TipLocal.y=(window.kb.height/2-TipLocal.y)*128*(32/window.kb.scale)/2;

        for ( var key in keyRegistry ){
          k = keyRegistry[key];
          d = Math.hypot( k.x - TipLocal.x , k.y - TipLocal.y );
          if ( k.x > TipLocal.x - k.w && k.x < TipLocal.x
            && k.y > TipLocal.y       && k.y < TipLocal.y + keySize ){
            // debug.report[key]={press:true, offset:k.offset};
            activateKeyboard( key );
          }
        }
      }
    }
  }
}

let DeleteRepeat = {on:false , t:0, delay:150, threshold:550};
function activateKeyboard( k , deactivate ){

  // convert : f(k) = (x,y)
  k = k.toUpperCase();
  console.log(k);

  if (!keyRegistry[k]) keyRegistry[k] = {x:0,y:0,w:1};
  let x = keyRegistry[k].x;
  let y = keyRegistry[k].y;
  let w = keyRegistry[k].w;
  let o = keyRegistry[k].offset;

  if (k==='META') keyRegistry[k].active = !deactivate;

  if (o == -1 && !deactivate) keyRegistry[k].offset = o = kb.affinity.len;

  let h = kb.width;
  let v = kb.height;

  x = h/2 - (x+keySize/2)/128/(32/window.kb.scale)*2 ;
  y = v/2 - (y-keySize/2)/128/(32/window.kb.scale)*2 ;
  let one = keySize/128/(32/window.kb.scale)*2;

  for ( var f=0 ; f<w/keySize*2-1.5 ; f++ ){
    // if (deactivate) kb.affinity.vizpoint( f+o , false );
    if (deactivate)
         kb.affinity.setpoint( f+o, [x-f*one/2,y,0], vr?.2*kb.scale:1, [128,128,256, 0] );
    else kb.affinity.setpoint( f+o, [x-f*one/2,y,0], vr?.2*kb.scale:1, [128,128,256, 0.5] );
  }

  if (!vr) DeleteRepeat.delay = 50;
  if (keyRegistry["BACKSPACE"].active || keyRegistry["DELETE"].active){
    if ( !DeleteRepeat.s ) DeleteRepeat.s = Date.now();
    if ( Date.now() > DeleteRepeat.s + DeleteRepeat.threshold
         && !DeleteRepeat.on
         && !DeleteRepeat.timer ){
      DeleteRepeat.timer = setTimeout( function(){ DeleteRepeat.on = true; }, DeleteRepeat.delay );
    }
  }else{ DeleteRepeat.s = false; }

  if (kb.affinity.getcolor(o).a > .25
      && (!keyRegistry[k].active || DeleteRepeat.on) ){
    DeleteRepeat.on = DeleteRepeat.timer = false;
    keyRegistry[k].active = true;
    if (!_txt) _txt="\n";

    if (k==="✓") k="COMMIT";
    if (k==="←") k="LEFT";
    if (k==="→") k="RIGHT";
    if (k==="↑") k="UP";
    if (k==="↓") k="DOWN";
    if (k==="⟳") k="RELOAD";

    if (k==="RELOAD"){
      k='';
      if (renderer.xr.getSession()) renderer.xr.getSession().end();
      // location.href=location.href;
      return;
    }
    if (k==="ENTER") k='\n';
    if (k==="LSHIFT" || k==="RSHIFT" || k==="SHIFT" || k==="TAB") k='';
    if (k==="LEFT" || k==="RIGHT" || k==="UP" || k==="DOWN") {
      _cx += 1*(k==="RIGHT") - 1*(k==="LEFT" && _cx>0);
      _cy += 1*(k==="DOWN") - 1*(k==="UP" && _cy>0);
      k='';
    }
    if (k==="COMMIT") { k=''; commitDeltas('force'); }
    if (k==="PLAY") { k='';
      // debug.report.PLAY = Date.now();
      try{
        delete debug.report.error;
        const tempScript = new Function("", `
          try{ ${_txt} }
          catch(e){ debug.report.scriptError = e; }
        `);
        setTimeout(tempScript, 0);
      }
      catch(e){ debug.report.functionifyError = e; }
    }

    if (k==="BACKSPACE" || k==="DELETE"){ k='';
      _txt = keyboardInsert("DELETE");
      // _txt.substr(0,_txt.length-1);
    }
    _txt = keyboardInsert(keyboardShift(k));
    if (selection)
     if (selection.constructor)
      if (selection.text!=_txt) {
        selection.text = _txt;
        store (selection, {txt : selection.text} )
      }

    // PLAY THE SCRIPT
    if (!!false) {
      try{
        delete debug.report.error;
        const tempScript = new Function("", `
          try{ ${_txt} }
          catch(e){ debug.report.scriptError = e; }
        `);
        setTimeout(tempScript, 0);
      }
      catch(e){ debug.report.functionifyError = e; }
    }
  }
  else if (kb.affinity.getcolor(o).a < .1) keyRegistry[k].active = false;
}

function keyboardInsert(c){
  click_mid.cloneNode(true).play();
  if (keyRegistry['META']) if (keyRegistry['META'].active) {
    if (c== 's') {
      activateKeyboard('commit');
      activateKeyboard('commit', 'deactivate');
      activateKeyboard('s', 'deactivate');
    }
    if (c=='\n') {
      activateKeyboard('play');
      activateKeyboard('play', 'deactivate');
      // activateKeyboard('enter', 'deactivate');
    }
    return _txt;
  }

  let p = _txt;

  let lines = p.split('\n');
  let ipy = Math.min(Math.max(0, _cy), lines.length-1);

  let words = lines[ipy];
  let ipx = Math.min(Math.max(0, _cx), words.length);

  if (c==="DELETE") {
    if (words.length && ipx>0) {
      words = words.substr(0,ipx-1)+words.substr(ipx);
      ipx--;
    }else if (ipy>0) {
      ipx = lines[ipy-1].length;
      lines[ipy-1]+=words;
      lines = lines.slice(0,ipy).concat(lines.slice(ipy+1))
      ipy--;
      words = lines[ipy];
    }
  } else if (c) {
    words=words.substr(0,ipx)+c+words.substr(ipx);
    ipx++;
  }
  lines[ipy] = words;
  if (c==='\n') { ipx=0; ipy++; }
  _cx = ipx;
  _cy = ipy;
  return lines.join('\n');
}

function keyboardShift(k){
  if (k.length>1) return k;

  for (var e in singleKeys){
    if (!singleKeys[e]) continue;
    if (singleKeys[e].indexOf(k)===-1) continue;
    if (keyRegistry["LSHIFT"].active || keyRegistry["RSHIFT"].active) return k;
    return k.toLowerCase();
  }

  for (var e in dualKeys){
    if (!dualKeys[e]) continue;
    let p1 = dualKeys[e][0].indexOf(k);
    let p2 = dualKeys[e][1].indexOf(k);
    if (p1===-1 && p2===-1) continue;

    if (p1===-1) p1=p2;

    if (keyRegistry["LSHIFT"].active || keyRegistry["RSHIFT"].active){
      return dualKeys[e][1][p1];
    }
    else return dualKeys[e][0][p1];
  }

  return k;
}

function KeyboardPointlcoudProgram(){
  let affinity = this;
  if (!affinity) return;

  let color = [108,108,108,.4];
  let a = .03;
  for ( var d = 0 ; d < 54 ; d ++ ){
    let x = d/54*kb.width + kb.mesh.position.x - kb.width/2 +(Date.now() % 29)/1000;
    let y = 0;
    let z = kb.mesh.position.y - kb.height/2;
    affinity.setpoint( 2+d, [x,y,z], a , color );
    affinity.setpoint( 2+d+54, [x,y,z+kb.height], a , color );
  }
}

export { drawKeyboard };
*/

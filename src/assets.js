var dropZone = document.body;

// Optional.   Show the copy icon when dragging over.  Seems to only work for chrome.
dropZone.addEventListener('dragover', function(e) {
  e.stopPropagation();
  e.preventDefault();
  e.dataTransfer.dropEffect = 'copy';
});

// Get file data on drop
dropZone.addEventListener('drop', function(e) {
  e.stopPropagation();
  e.preventDefault();
  var files = e.dataTransfer.files; // Array of all files

  for (var i=0, file; file=files[i]; i++) {
    // IMAGES
    if (file.type.match(/image.*/)) {
      var reader = new FileReader();
      reader.onload = function(e2) {
        // finished reading file data.
        var img = document.createElement('img');
        img.src= e2.target.result;
        // selection.affinity.wordCTX.drawImage(img,0,0);
        // store(selection, {img:img.src});
        // img.style.position = "absolute";
        // img.style.top = "0px";
        // img.style.left = "0px";
        // document.body.appendChild(img);

        // todo: generate thumbnail spritesheet for GPU vertex shader
        // when it's close / focused use texture on plane ?

        let a = newAtom({img:img.src, x:1, y:.5, z:-4});
        vizq.setpoint(a.id, [a.x,a.y,a.z], .1, [[80,100,200,100],[100,100,100,100]][a.editing?0:1]);
        const imgMesh = createTextureMesh(img);
        imgMesh.position.x = a.x;
        imgMesh.position.y = a.y;
        imgMesh.position.z = a.z;
        zeropoint.p.add(imgMesh);
        a.imgMesh = imgMesh;
        // zeropoint.p.children = zeropoint.p.children.slice(3).concat( zeropoint.p.children.slice(0,3) );

      }

      reader.readAsDataURL(file); // start reading the file data.
    }
  }
});

function createTextureMesh(img){
  h=1; v=1;
  const vertices = new Float32Array([ 0,0,0, -h,0,0, -h,-v,0, 0,-v,0 ]);
  const uvs      = new Float32Array([ 0,0,   1,0,   1,1,    0,1 ]);
  const indices  = new Uint32Array( [ 0, 2, 1, 0, 3, 2 ] );
  const geometry = new THREE.BufferGeometry();
  geometry.setAttribute( 'position' , new THREE.BufferAttribute( vertices, 3 ) );
  geometry.setAttribute( 'uv' , new THREE.BufferAttribute( uvs, 2 ) );
  geometry.setIndex( new THREE.BufferAttribute( indices, 1 ) );

  const texture = new THREE.Texture( img );
  texture.needsUpdate = true;
  texture.wrapS = texture.wrapT = THREE.ClampToEdgeWrapping;
  texture.flipY = false;

  const material = new THREE.MeshBasicMaterial( { map:texture, transparent:true } );
  material.side = THREE.DoubleSide;

  const mesh = new THREE.Mesh( geometry, material );
  return mesh;
}

function drawSymbol(a, w, s, rgba){
  w = w===undefined? "ॐ" : String(w);

  if (!a.symbolCanvas){
    a.symbolCanvas = document.createElement('canvas');
    a.wordCTX = a.symbolCanvas.getContext('2d');

    let h = 12, v = 8;
    a.symbolCanvas.width=128*h;
    a.symbolCanvas.height=128*v;

    if(false){ // true = visualize symbol canvas
      document.body.append(a.symbolCanvas);
      a.symbolCanvas.style.position="absolute";
      a.symbolCanvas.style.left="0px";
      a.symbolCanvas.style.top="0px";
      a.symbolCanvas.style.border="solid rgba(255,255,255,1) 1px";
    }

    canvasText(w, s, rgba);

    a.setpoint( 1, [0,0,0], 1/16, [255,255,255,.1] );
    // a.setpoint( 2, [-1/16,0,0], 1/16, [255,255,255,.1] );
    // a.setpoint( 3, [0,-1/16,0], 1/16, [255,255,255,.1] );

    h/=16; v/=16;
    const vertices = new Float32Array([ 0,0,0, -h,0,0, -h,-v,0, 0,-v,0 ]);
    const uvs = new Float32Array([ 0,0,   1,0,   1,1,    0,1 ]);
    const indices = new Uint32Array( [ 0, 2, 1, 0, 3, 2 ] );
    const geometry = new THREE.BufferGeometry();
    geometry.setAttribute( 'position' , new THREE.BufferAttribute( vertices, 3 ) );
    geometry.setAttribute( 'uv' , new THREE.BufferAttribute( uvs, 2 ) );
    geometry.setIndex( new THREE.BufferAttribute( indices, 1 ) );

    const texture = new THREE.Texture(a.symbolCanvas);
    texture.needsUpdate = true;
    texture.wrapS = texture.wrapT = THREE.ClampToEdgeWrapping;
    texture.flipY = false;

    const material = new THREE.MeshBasicMaterial( { map:texture, transparent:true } );
    material.side = THREE.DoubleSide;

    const symbolMesh = new THREE.Mesh( geometry, material );
    symbolMesh.visible = false;
    symbolMesh.position.x+=.01;
    symbolMesh.position.y+=.015;
    // symbolMesh.position.z+=h/2;
    a.p.add(symbolMesh);
    a.symbolMesh = symbolMesh;
  }

  function canvasText(w, s, rgba){
    if (w===a.wordCTX.w || !w) return;
    if (!s) s = 25;
    if (!rgba) rgba = [108, 108, 108, .8];
    let fontSize = s;

    a.wordCTX.clearRect(0,0,a.symbolCanvas.width,a.symbolCanvas.height);
    a.wordCTX.fillStyle = `rgba(16,16,16,.2)`;
    a.wordCTX.fillRect(0,0,a.symbolCanvas.width,a.symbolCanvas.height);

    a.wordCTX.w = w;
    a.wordCTX.s = s;
    a.wordCTX.font = `${fontSize}px Consolas, DejaVu Sans Mono, monospace`;
    a.wordCTX.fillStyle = `rgba(${rgba})`;

    let lines = w.split("\n");
    for (var l = 0 ; l < lines.length ; l ++ ) { a.wordCTX.fillText( lines[l], 10, 10+(1+l)*fontSize*1.2 ); }
  }

  if (a.atom.img!=a.wordCTX.img){
    a.wordCTX.img = a.atom.img;
    var img = document.createElement('img');
    img.src= a.atom.img;
    // setTimeout(function(a,img){ a.wordCTX.drawImage(img,0,0); }, 100, a, img);
    setTimeout(function(a,img){
      let aw = a.symbolCanvas.width;
      let ah = a.symbolCanvas.height;
      let iw = img.width;
      let ih = img.height;
      let aspect = iw/ih;

      if(iw>aw){ iw=aw; ih=aw/aspect; }
      if(ih>ah){ ih=ah; iw=ah*aspect; }
      a.wordCTX.drawImage(img,0,0,iw,ih);
      a.wordCTX.img_w = img.width;
      a.wordCTX.img_h = img.height;
      a.wordCTX.img_iw = iw;
      a.wordCTX.img_ih = ih;
    }, 100, a, img);
  }

  // if you make changes to the wordCTX, call:
  if (w!==a.wordCTX.w || s!==a.wordCTX.s || rgba!==a.wordCTX.rgba){
    canvasText(w, s, rgba); a.symbolMesh.material.map.needsUpdate = true;
  }
}

export { drawSymbol }

import { Pointcloud } from './pointcloud.js';
import { BodyPlot , HandPointCloudProgram } from './body.js';
import { Vector3 } from 'https://cdn.jsdelivr.net/gh/mrdoob/three.js/build/three.module.js';
// import { Vector3 } from '/vendor/three.module.js';

window.GrabThreshold=.02;
window.selection = {};

function initAffinities(){
  window.zeropoint =   new Pointcloud( {max: 108, texture: 'h.png', blend:2, fog:[0,0,0]} );
  window.body      =   new Pointcloud( {max: 1008, texture: 'h.png', blend:2, fog:[0,0,0]} );
  window.handpoint = [
    new Pointcloud( {max: 25, texture: 'h.png', blend:2, fog:[0,0,0]} ),
    new Pointcloud( {max: 25, texture: 'h.png', blend:2, fog:[0,0,0]} )
  ];
  window.keyboard = new Pointcloud( {max:100, texture: 'a.png', blend:2} );

  zeropoint.run    = DefaultPointlcoudProgram;
  body.run         = BodyPlot;
  handpoint[0].run = HandPointCloudProgram;
  handpoint[1].run = HandPointCloudProgram;

  zeropoint.p.position.x =  0.0;
  zeropoint.p.position.y =  0.5;
  zeropoint.p.position.z =  0.0;

  handpoint[0].hand = 0;
  handpoint[1].hand = 1;

  subs.h0 = handpoint[0];
  subs.h1 = handpoint[1];
  subs.body = body;

  scene.add( zeropoint.p );
  scene.add( keyboard.p );
  scene.add( handpoint[0].p );
  scene.add( handpoint[1].p );
  zeropoint.p.add( body.p ); // better on scene?

  // camera.position.set( zeropoint.p.position.x, zeropoint.p.position.y-.1, zeropoint.p.position.z+1.5 );
  camera.position.set( Number(localStorage.cameraX) || zeropoint.p.position.x,
                       Number(localStorage.cameraY) || zeropoint.p.position.y-.1,
                       Number(localStorage.cameraZ) || zeropoint.p.position.z+1.5);

  camera.rotation.x = Number(localStorage.cameraRX) || 0;
  camera.rotation.y = Number(localStorage.cameraRY) || 0;

  // glyphCloud = new Pointcloud( { texture:'glyphsheet.png', sheet:true, blend:2, fog:[0,0,0] } );
  // glyphCloud = new Pointcloud( { texture:generateGlyphSheet(), blend:2, fog:[0,0,0] } );
  glyphCloud = new Pointcloud( { texture:'a.png', blend:2, fog:[0,0,0] } );
  window.vizq = new Pointcloud( { max:4096, texture:'h.png', blend:2, fog:[0,0,0] } );
  setTimeout( ()=>{
    keyboard.g.boundingSphere.radius = 10;
    glyphCloud.g.boundingSphere.radius = 10;
    vizq.g.boundingSphere.radius = 10;
  }, 1000 );
  zeropoint.p.add( vizq.p );
  zeropoint.p.add( glyphCloud.p );
  glyphCloud.p.material.uniforms.texture_.value = generateGlyphSheet();
  glyphCloud.p.material.uniforms.texture_.value.needsUpdate = true;

  keyboard.p.material.uniforms.texture_.value = generateGlyphSheet();
  keyboard.p.material.uniforms.texture_.value.needsUpdate = true;
  atoms.keyboard = {};
  Object.keys(alpha).forEach( (c,i) => {
    atoms.keyboard[c] = { id:i, text:c, x:-1+i/40, y:.5, z:-.5 };
    const a = atoms.keyboard[c];
    keyboard.setpoint( a.id, [a.x, a.y, a.z], .25, [100,100,100,1] );
    keyboard.setsheet( a.id, i );

  });

  Object.keys(entities).forEach(e=>{
    let atomSet = atoms[e];
    Object.values(atomSet).forEach(a=>{
      if (isNaN(a.x)) a.x=0;
      if (isNaN(a.y)) a.y=0;
      if (isNaN(a.z)) a.z=0;
      vizq.setpoint(a.id, [a.x,a.y,a.z], .1, [[80,100,200,100],[100,100,100,100]][a.editing?0:1]);
      if(a.editing) text(a.text, {block:a.id, code:true, density:vr?1:4, xyz:vizq.getposition(a.id)});
    });
  });

  // atoms["_.0.entity"][2].inbox.push({});
  // checkInboxes();
}

function drawAffinity(affinity) {
  const clist = [controller1, controller2];
  let tp = new Vector3(); // target position
  let screenVector = new Vector3();

  function jointLength(){
    try{
      const topDigit =  [9, 14, 19, 24].indexOf(affinity.holder.joint);
      const midDigit =  [8, 13, 18, 23].indexOf(affinity.holder.joint);
      const baseDigit = [7, 12, 17, 22].indexOf(affinity.holder.joint);

      if (topDigit !==-1) return 2;
      if (midDigit !==-1) return 2;
      if (baseDigit!==-1) return 3;
      return 4;
    }catch(e){ return .1; }
  }

  let fingerTouched = false;

  affinity.p.getWorldPosition( tp ); // target
  affinity.p.getWorldPosition( screenVector );
  screenVector.project(camera);

  // CHECK FOR MOUSE INTERACTIONS
  let mouseTouch = Math.hypot( Math.abs(screenVector.x - mouse.x), Math.abs(screenVector.y - mouse.y) )<GrabThreshold
  if (mouse.buttons && mouseTouch) {
    // redundant code
    if (selection!==affinity.atom && affinity.atom){
      selection = affinity.atom;
      _txt = affinity.atom.text;
      debug.report.selected = affinity.atom.id;
    }else if(affinity.a>=2){
      // affinity.g.setDrawRange(0,1);
    }
    else{
      // affinity.p.position.x = mouse.x;
      // affinity.p.position.y = mouse.y;
      // console.log((mouse.x-mouse.dragOffsetX)/10000000);
      affinity.p.translateX(-(mouse.x-mouse.dragOffsetX)/3);
      affinity.p.translateY( (mouse.y-mouse.dragOffsetY)/3);
      mouse.dragOffsetX = mouse.x;
      mouse.dragOffsetY = mouse.y;
      mouse.touching="moving";
      console.log(mouse.touching);
    }
  }
  else if(mouse.buttons && selection===affinity.atom){

    // set insertion point XY by mouse position
    var vector = new Vector3( mouse.x-screenVector.x, mouse.y-screenVector.y, screenVector.z ).unproject( camera );

    let dx = vector.x-camera.position.x;
    let dy = vector.y-camera.position.y;
    // let dz = -camera.position.z + (affinity.p.position.z+zeropoint.p.position.z);

    let size = .0015;
    let w = 7*size//affinity.symbolMesh.geometry.getAttribute('position').array[3*2+1] //v
    let h = 16*size//affinity.symbolMesh.geometry.getAttribute('position').array[3*2+0] //h

    _cx = Math.max( 0, Math.round(dx/w) );
    _cy = Math.max( 0, Math.round(-dy/h) );
  }


  // CHECK FOR HAND INTERACTIONS
  affinity.held = false;
  for(var c=0; c<=1; c++){
    if (!hands[c].joints['index-finger-tip']) continue;
    if (hands[c].userData.isHolding) if ( hands[c].userData.isHolding!==affinity ) continue;
    if (affinity.holder) if (affinity.holder!==hands[c]){
        affinity.holder = false;
        continue;
      }

    let h9 = {x:-1,y:0,z:0};
    let h4 = {x: 1,y:0,z:0};
    let hp = handpoint[c].p.position;
    if (handpoint[c]) if (handpoint[c].getposition) if (handpoint[c].len>=9) {
      h9 = handpoint[c].getposition(9);
      h4 = handpoint[c].getposition(4);
    }

    const currentPinch = Math.hypot(h9.x-h4.x, h9.y-h4.y, h9.z-h4.z) < GrabThreshold;
    if (currentPinch) {
      if (!hands[c].userData.isSelecting){
        hands[c].userData.isSelecting = true;
        debug.report['pinch'+c] = true;
      }
    }else{
      /*if (affinity.pinchedOpen)*/ affinity.pinchedOpen = false;
      if (hands[c].userData.isSelecting){
        if (hands[c].userData.isHolding) hands[c].userData.isHolding.holder = false;
        hands[c].userData.isHolding = false;
        hands[c].userData.isSelecting = false;
        debug.report['pinch'+c] = false;
    } }

    let pinchTouch = Math.hypot((h9.x+h4.x)/2+hp.x-tp.x,
                                (h9.y+h4.y)/2+hp.y-tp.y,
                                (h9.z+h4.z)/2+hp.z-tp.z) < GrabThreshold
    if ( Math.hypot(h9.x+hp.x-tp.x, h9.y+hp.y-tp.y, h9.z+hp.z-tp.z)
       < GrabThreshold*1.5 && !pinchTouch ) fingerTouched = true;

    if ( (currentPinch && pinchTouch) || hands[c].userData.isHolding===affinity ){
      hands[c].userData.isHolding = affinity;
      affinity.held = true;
      affinity.holder = hands[c];
      if (affinity === zeropoint) {
        affinity.p.position.x = (h9.x + h4.x)/2 + hp.x;
        affinity.p.position.y = (h9.y + h4.y)/2 + hp.y;
        affinity.p.position.z = (h9.z + h4.z)/2 + hp.z;
      }else{
        affinity.p.position.x = (h9.x + h4.x)/2 + hp.x - zeropoint.p.position.x;
        affinity.p.position.y = (h9.y + h4.y)/2 + hp.y - zeropoint.p.position.y;
        affinity.p.position.z = (h9.z + h4.z)/2 + hp.z - zeropoint.p.position.z;
        store(affinity.atom, { x:affinity.p.position.x, y:affinity.p.position.y, z:affinity.p.position.z }, "hand gesture");
        debug.report.storeObject = affinity.atom.id;
      }

      if ( affinity.atom ){
        if ( selection!==affinity.atom ) {
          selection = affinity.atom;
          affinity.pinchedOpen = true;
          _txt = affinity.atom.text;
          debug.report.selected = affinity.atom.id;
        }else if(affinity.a>=2 && !affinity.pinchedOpen){
          affinity.g.setDrawRange(0,1);
        }
      }
    }
  }

  if (affinity.atom) {
    const label = affinity.atom.text;
    if ( selection===affinity.atom && affinity.a>=2 && label!==affinity.previewText ) {
      // drawSymbol(affinity, label);
      // glyphCloud = affinity;
      // glyphCloud.len = 4;
      text(label, {code:true});
      // glyphCloud.g.setDrawRange(0,glyphCloud.count);
      affinity.previewText = label;
      // affinity.symbolMesh.visible = true;
    // }else if ( fingerTouched ) {
      // if ( !affinity.symbolMesh || label!=affinity.wordCTX.w ) drawSymbol(affinity, label);
      // affinity.symbolMesh.visible = true;
    }
  }//else if(affinity.a>=2)/*if (affinity.symbolMesh )*/ affinity.g.setDrawRange(0,1);
    // }else if (affinity.symbolMesh ) affinity.symbolMesh.visible = false;
    // }else if (affinity.symbolMesh ) setTimeout(function(a){ a.symbolMesh.visible = false }, 0, affinity);
  // }
  affinity.run();
}

function DefaultPointlcoudProgram(){
  let affinity = this;
  let haloColor = [108,100,255,.5];
  let pointColor = [216,155,255,.5];

  if (affinity.atom===selection) {
    haloColor = pointColor;

    let h = handpoint[1].p.position;     // wrist;
    let f = handpoint[1].getposition(9); // hands[1].joints['index-finger-tip'];
    let l = affinity.p.worldToLocal( new Vector3(h.x+f.x, h.y+f.y, h.z+f.z) );
    let lx = Math.round(l.x*137)/137;
    let ly = Math.round(l.y*68.1)/68.1;
    let size = .0015;
    let cw = 7*size;
    let ch = 16*size;

    lx = lx<0? lx:0;
    ly = ly<0? ly:0;

    if ( Math.abs(l.z)<GrabThreshold && hands[1].joints['index-finger-tip'].position.z <= h.z+f.z ) {
      _cx = -lx*137;
      _cy = -ly*68.1;
      affinity.setpoint( 2, [lx+.004, ly-.004, 0], vr?.006:.02, [0,255,0,.8] );
      affinity.setpoint( 3, [lx+.004, ly     , 0], vr?.006:.02, [0,255,0,.8] );
      affinity.setpoint( 4, [lx+.004, ly+.004, 0], vr?.006:.02, [0,255,0,.8] );
    }
    else {
      lx = -_cx*cw -.003;
      ly = -_cy*ch -.006;
      affinity.setpoint( 2, [lx+.004, ly-.004, 0], vr?.006:.02, [0,255,0,.8] );
      affinity.setpoint( 3, [lx+.004, ly     , 0], vr?.006:.02, [0,255,0,.8] );
      affinity.setpoint( 4, [lx+.004, ly+.004, 0], vr?.006:.02, [0,255,0,.8] );
  } }

  if (!affinity) return;
  if (affinity===zeropoint) { haloColor = [0,100,255,.5]; pointColor = [55,155,255,.1]; }

  let x = 0;
  let y = 0;
  let z = 0;
  let maxPoints = 72;
  let dt = Date.now()/128;
  let s=.03+.02*Math.cos(dt+affinity.a/affinity.max*2*Math.PI);

  if (affinity.holder){
    if (!affinity.holdTime) affinity.holdTime = dt;
    const holdDuration = dt-affinity.holdTime;
    const bias = Math.max(0, 1-Math.pow(holdDuration/16,1.2));
    s = s*bias + .005*(1-bias);
    y = y*bias + s/2*(1-bias) * ( affinity.holder.hand==="left" ? 1 : -1 );
  }
  if (affinity == zeropoint) s = .05;
  affinity.setpoint(0, [x,z,y], (affinity === zeropoint ? .3 : .1 ), haloColor );
}

export { initAffinities, drawAffinity };

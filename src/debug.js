window.debug = { report:{version:"v0.6.3"} };
window.log = function(o){ debug.report.log = o; }

function drawDebug(){
  debug.report.cursor = {x:_cx,y:_cy};
  let w = JSON.stringify(debug.report, null, 2);

  if (w!==debug.w) {
    debug.w = w;
    text(w, {block:'debug', code:true, size:1, density:vr?1:4, xyz:vizq.getposition(0)});
  }
}

export { drawDebug };

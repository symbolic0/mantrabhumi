import {
  AdditiveBlending,
  Color,
  BufferGeometry,
  BufferAttribute,
  ShaderMaterial,
  TextureLoader,
  Texture,
  Points,
  Vector3,
  Matrix4
} from 'https://cdn.jsdelivr.net/gh/mrdoob/three.js/build/three.module.js';
// } from '/vendor/three.module.js';

const baseURL = "./img/"; // path to texture images

function Pointcloud(params){
  let maxPoints = 65536;                      //DEFAULT POINTCOUNT
  let customTexture = "h.png";                //DEFAULT TEXTURE
  let fogColor = [0,0,0];                     //DEFAULT FOG
  let blendMethod = THREE.AdditiveBlending;   //DEFAULT BLEND METHOD
  let sheet = false;                          //DEFAULT SPRITESHEET MODE
  let texture = false;
  this.active = true;                         //DEFAULT ACTIVE START
  this.count = 0;                             //total active points
  this.blocks = {0:{start:0, size:4096, count:0}};  //for segmenting text blocks

  for(var i in params){
    switch(i){
      case 'max'        : maxPoints     = params.max;          break;
      case 'texture'    : customTexture = params.texture;      break;
      case 'fog'        : fogColor      = params.fog;          break;
      case 'sheet'      : sheet         = params.sheet;        break;
      case 'blend'      : blendMethod   = params.blend;        break;
      case 'active'     : this.active   = params.active;       break;
      default: console.log('unrecognized pointcloud parameter on line $badparamline, bob.');
    }
  }

  if (customTexture.constructor.name==='Texture') texture = customTexture;

  let positions = new Float32Array( maxPoints * 3 );
  let rotations = new Float32Array( maxPoints );
  let colors    = new Float32Array( maxPoints * 3 );
  let opacities = new Float32Array( maxPoints );
  let sizes     = new Float32Array( maxPoints );
  let viz       = new Float32Array( maxPoints );
  let sheets    = new Float32Array( maxPoints );

  let color     = new THREE.Color();

  for(var i=0; i<maxPoints; i++){
    var vertex = new THREE.Vector3();
    vertex.x = 0;
    vertex.y = 0;
    vertex.z = 0;
    vertex.toArray( positions, i * 3 );
    color.setRGB( 0, 0, 0 );
    color.toArray( colors, i * 3 );
    rotations[ i ] = 0;
    opacities[ i ] = 0;
    sizes[ i ] = 0;
    viz[ i ] = false;
    // sheets[ i ] = 0;
    if (sheet) sheets[ i ] = 19+1;
    else sheets[ i ] = -1;
  }

  this.g = new THREE.BufferGeometry();
  this.g.setAttribute( 'position',      new THREE.BufferAttribute( positions, 3 ) );
  this.g.setAttribute( 'rotation',      new THREE.BufferAttribute( rotations, 1 ) );
  this.g.setAttribute( 'customColor',   new THREE.BufferAttribute( colors,    3 ) );
  this.g.setAttribute( 'customOpacity', new THREE.BufferAttribute( opacities, 1 ) );
  this.g.setAttribute( 'size',          new THREE.BufferAttribute( sizes,     1 ) );
  this.g.setAttribute( 'on',            new THREE.BufferAttribute( viz,       1 ) );
  this.g.setAttribute( 'customSheet',   new THREE.BufferAttribute( sheets,    1 ) );

  function shaderTagToText(id){
    let shaderString=document.getElementById(id).textContent;
    // return shaderString;
    if(fogColor===false) return shaderString.replace('@fogColor', '0,0,0').replace('@fog', 'false');
    else                 return shaderString.replace('@fogColor', fogColor).replace('@fog', 'true');
  }

  let material = new THREE.ShaderMaterial( {
    uniforms: {
      alpha:    { value: 1.0 },
      color:    { value: new THREE.Color( 0xffffff ) },
      texture_: { value: new TextureLoader().load( baseURL + customTexture ) }
      // texture_: { value: texture ? new Texture(texture) : new TextureLoader().load( baseURL + customTexture ) }
      // texture_: { value: new Texture(texture) }
    },
    vertexShader:   shaderTagToText('vertexshader'),
    // fragmentShader: shaderTagToText('mandelshader'),
    fragmentShader: shaderTagToText('fragmentshader'),
    blending:       blendMethod,
    depthTest:     !true,
    // alphaTest:      .8,
    transparent:    true
  });

  this.p = new THREE.Points( this.g, material );
  this.setpoint_preprocess_callback = undefined;

  this.getpoint = function (i){
    return {
      xyz:   this.getposition(i),
      size:  this.getsize(i),
      color: this.getcolor(i)
    }
  }

  this.getposition = function (i){
    const pos = this.g.attributes.position.array;
    return {
      x: pos[i*3+0],
      y: pos[i*3+1],
      z: pos[i*3+2]
    };
  }

  this.getrotation = function (i){
    const rotation = this.g.attributes.rotation.array;
    return rotation[i];
  }

  this.getsize = function (i){
    return this.g.attributes.size.array[i];
  }

  this.getcolor = function (i){
    const color = this.g.attributes.customColor.array;
    const alpha = this.g.attributes.customOpacity.array[i];
    return {
      r: color[i*3+0]*255,
      g: color[i*3+1]*255,
      b: color[i*3+2]*255,
      a: alpha
    }
  }

  this.getvisible = function (i){
    return this.g.attributes.on.array[i];
  }

  // -------------------------

  this.setpoint = function (i, xyz, size, rgba){
    if(i===undefined) return;
    if (this.setpoint_preprocess_callback!=undefined){
      const obj = this.setpoint_preprocess_callback({i, xyz, size, rgba}, this);
      xyz  = obj.xyz  || xyz;
      size = obj.size || size;
      rgba = obj.rgba || rgba;
    }

    this.setposition(i, xyz);
    this.setsize    (i, size);
    this.setcolor   (i, rgba);
    this.setvisible (i, true);

    this.count = Math.max(this.count, i+1);
  }

  this.setsheet = function (i, s){
    this.g.attributes.customSheet.array[i] = s;
    this.g.attributes.customSheet.needsUpdate = true;
  }

  this.setposition = function (i, xyz){
    this.g.attributes.position.array[i*3+0] = xyz.x || xyz[0];
    this.g.attributes.position.array[i*3+1] = xyz.y || xyz[1];
    this.g.attributes.position.array[i*3+2] = xyz.z || xyz[2];
    this.g.attributes.position.needsUpdate = true;
  }

  this.setrotation = function (i, rotation){
    this.g.attributes.rotation.array[i] = rotation;
    this.g.attributes.rotation.needsUpdate = true;
  }

  this.setsize = function (i, size){
    this.g.attributes.size.array[i] = size//*2;
    this.g.attributes.size.needsUpdate = true;
  }

  this.setcolor = function (i, rgba){
    this.g.attributes.customColor.array[i*3+0] = ( rgba.r || rgba[0] ) / 255;
    this.g.attributes.customColor.array[i*3+1] = ( rgba.g || rgba[1] ) / 255;
    this.g.attributes.customColor.array[i*3+2] = ( rgba.b || rgba[2] ) / 255;
    this.g.attributes.customOpacity.array[i]   =   rgba.a || rgba[3];
    this.g.attributes.customColor.needsUpdate   = true;
    this.g.attributes.customOpacity.needsUpdate = true;
  }

  this.setvisible = function (i, visible){
    this.g.attributes.on.array[i] = visible;
    this.g.attributes.on.needsUpdate = true;
  }

  //SO SLOW
  this.depthsort = function() {
    let vector = new THREE.Vector3();
    let matrix = new THREE.Matrix4();  // Model View Projection matrix
    let index     = this.g.getIndex();
    let positions = this.g.getAttribute( 'position' ).array;
    let length    = positions.length / 3;

    matrix.multiplyMatrices( camera.projectionMatrix, camera.matrixWorldInverse );
    matrix.multiply( this.p.matrixWorld );

    if ( index === null ) {
      var array = new Uint16Array( length );
      for ( var i = 0; i < length; i ++ ) { array[ i ] = i; }
      index = new THREE.BufferAttribute( array, 1 );
      this.g.setIndex( index );
    }

    var sortArray = [];
    var far=99999;
    var near=-99999;
    var fari,neari;
    for ( var i = 0; i < length; i ++ ) {
      vector.fromArray( positions, i * 3 );
      vector.applyMatrix4( matrix );
      sortArray.push( [ vector.z, i ] );
      if(vector.z<far){ far=vector.z; fari= index.array[i]; }
      if(vector.z>near){ near=vector.z; neari= index.array[i]; }
      //if(i==index.array[0]) var near=vector.z;
      //if(i==index.array[length-1]) var far=vector.z;
    }

    function numericalSort( a, b ) { return b[ 0 ] - a[ 0 ]; }
    if(true /*fari>neari*/){
    //if(far>near){
      sortArray.sort( numericalSort );

      var indices = index.array;
      for ( var i = 0; i < length; i ++ ) { indices[ i ] = sortArray[ i ][ 1 ]; }
      this.g.index.needsUpdate = true;
    }
  }
}

export { Pointcloud };

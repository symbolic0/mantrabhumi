function clamp(x,a,b){ return (x<a?a: (x>b?b: x) ); }

function bone(pos,stp,th,eth,tot,prog,den,noball,symmetric,col,dest){
  if(noball==undefined) noball=false;
  if(symmetric==undefined) symmetric=true;
  if(col==undefined) col=[255,255,255,.35];
  if(dest===undefined) dest=gross;
  th*=dotsize/30;

  col[3]=clamp(prog/tot,0,col[3]);
  var x=pos[0],y=pos[1],z=pos[2];
  for(var k=0; k<clamp(prog,0,tot); k+=1/den){
    if(k==0) col[0]*=3; if(k==1) col[0]/=3;
    if(k==0) col[1]*=3; if(k==1) col[1]/=3;
    if(k==0) col[2]*=3; if(k==1) col[2]/=3;

    dest.setpoint(qqq++,[x,y,z], th*(k==0 && !noball?3:1), col);

    if(symmetric) dest.setpoint(qqq++,[-x,y,z],th*(k==0 && !noball?3:1), col);
    x+=stp[0]/den;
    y+=stp[1]/den;
    z+=stp[2]/den;
    th-=(th-(th*eth))/(tot-1);
  }
  return [x,y,z];
}

function ring(xyz,r,a,s,d, dest){
  if(a===undefined) a=1;  // aspect
  if(s===undefined) s=32; // density
  if(d===undefined) d=5;  // dot size
  if(dest===undefined) dest=gross;

  var x,y,z;
  for(var t=0; t<pi2; t+=pi/s){
    x=xyz[0]+r*cos(t);
    y=xyz[1];
    z=xyz[2]+r*sin(t)*a;
    dest.setpoint(qqq++,[x,y,z],d*dotsize, [255,200,100,.5]);
  }
}

let dotsize = .01;
let pi = Math.PI;
let pi2 = pi*2;
let cos = Math.cos;
let sin = Math.sin;
let pow = Math.pow;
var gross;
var qqq = 0;

function BodyPlot(){
  let x = 0;
  let y = 0;
  let z = 0;

  // this.p.position.x=.1
  this.p.scale.x=.01
  this.p.scale.y=.01
  this.p.scale.z=.01
  gross = this;
  let subtle = this;

  qqq=0;
  //chakras
  subtle.setpoint(qqq++,[0, 24,0],  350*dotsize/20, [255,128,205,.35]);         // silence
  subtle.setpoint(qqq++,[0, 18,0],  600*dotsize/20, [255,  0,255,.35]);         // vision
  subtle.setpoint(qqq++,[0, 10,0],  800*dotsize/20, [  0,255,255,.35]);         // voice
  subtle.setpoint(qqq++,[0,  0,0], 1500*dotsize/20, [  0,255,  0,.35]);         // heart
  // subtle.setpoint(qqq++,[0,  0,0], Infinity,        [255,55,   0,.35]);         // heart
  subtle.setpoint(qqq++,[0,-10,0], 1200*dotsize/20, [255,255,  0,.35]);         // will
  subtle.setpoint(qqq++,[0,-20,0],  800*dotsize/20, [255,128,  0,.35]);         // progeny
  subtle.setpoint(qqq++,[0,-30,0],  400*dotsize/20, [255,  0,  0,.35]);         // root

  var ascent , highCurve , frequency , phase , amplitude , slant , offset;
  let base = -30;
  for(var s=0; s<20; s+=.5){                                                  //spine
    ascent = s/20*50;
    highCurve = (s+30)/50; // higher curvature at the base than at the top

    frequency = s/20*10;
    phase = -.3;
    amplitude = -2;
    slant = 1*(-1+s/20);
    offset = -3;

    x= 0;
    y= base +ascent *highCurve;
    z= cos(frequency +phase) *amplitude +slant +offset;

    gross.setpoint(qqq++,[x,y,z],(200-s*5)*dotsize/30, [255,255,255,.35]);
  }

  //symmetric straight bones
        //joint     //step   //thick   //endthick%  //totalsteps //density
  var l;
  l=bone([10,-30,0], [-.3,-3,0], 150, .9, 10, 10, 3);                         //thigh
  bone(l, [0,-3,0], 120, .9, 10, 20, 2); l[0]+=2;                             //shin
  bone(l, [0,-3,0], 100, .9, 10, 30, 2);

  l=bone([10,5,0], [.6,-2,0], 120, .9, 10, 15, 2);                            //arm
  bone(l, [0,-2,0], 90, .9, 10, 25, 2); l[0]+=1;                              //forearm
  bone(l, [0,-2,0], 70, .9, 10, 35, 2);
  // console.log(l);

  bone([0,18,1], [0,-5.5,3.5], 1400, .4, 2, 20, 1,true,false);                //skull
  let U = undefined
  ring([0,  8, 0],  3,  1, 8);
  ring([0,  0, 0], 10, .7,24);
  ring([0,-16, 2],  7, .7,16);

  base = -38;
  for(var s=4; s<17; s+=.5){                                                  //torso outline (side)
    ascent = s/20*40;
    highCurve = 1 + .009 * pow(s-13,2); // highest curvature in the middle

    frequency = s/20*5.5;
    phase = -.5;
    amplitude = -4.5;
    slant = -5*(-1+s/20);
    offset = -13;

    x= cos(frequency +phase) *amplitude +slant +offset;
    y= base +ascent *highCurve;
    z= x/2 +6;

    gross.setpoint(qqq++, [ x,y,z], 5*dotsize, [255,200,100,.5]);
    gross.setpoint(qqq++, [-x,y,z], 5*dotsize, [255,200,100,.5]);
  }
}

function HandPointCloudProgram(){
  let affinity = this;
  if (!affinity) return;
  if (!hands[0].joints['wrist']) return;
  let inactiveColor = [255,255,255,.5];
  let activeColor =   [255,255,255,1];
  let c = (affinity===handpoint[0] ? 0 : 1);
  let a = .1;
  let d = 0;
  let bx = hands[c].joints['wrist'].position.x;
  let by = hands[c].joints['wrist'].position.y;
  let bz = hands[c].joints['wrist'].position.z;
  const smoothing = .7;

  affinity.p.position.x = affinity.p.position.x*.9 + .1*bx;
  affinity.p.position.y = affinity.p.position.y*.9 + .1*by;
  affinity.p.position.z = affinity.p.position.z*.9 + .1*bz;

  for (var joint in hands[c].joints) {
    let p = affinity.getposition(d);
    let x = p.x*smoothing + (1-smoothing)*(hands[c].joints[joint].position.x-bx);
    let y = p.y*smoothing + (1-smoothing)*(hands[c].joints[joint].position.y-by);
    let z = p.z*smoothing + (1-smoothing)*(hands[c].joints[joint].position.z-bz);
    affinity.setpoint( d, [x,y,z], .1+a - a/25*d , d==9? activeColor:inactiveColor );
    d++;
} }

export { BodyPlot , HandPointCloudProgram };

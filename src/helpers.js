function drawRoundedRect( CTX, x,y , w,h , r , color ){
  CTX.strokeStyle = color || "rgba(255,255,255,1)";
  CTX.beginPath();
  CTX.arc(x+r  , y+h-r, r, Math.PI/2  , Math.PI    ); CTX.lineTo(x    , y  +r);
  CTX.arc(x+r  , y  +r, r, Math.PI    , Math.PI/2*3); CTX.lineTo(x+w-r, y    );
  CTX.arc(x+w-r, y  +r, r, Math.PI/2*3, Math.PI*2  ); CTX.lineTo(x+w  , y+h-r);
  CTX.arc(x+w-r, y+h-r, r, Math.PI*2  , Math.PI/2*5); CTX.lineTo(x  +r, y+h  );
  CTX.stroke();
}

function include(file) {
  var script  = document.createElement('script');
  script.src  = file;
  script.type = 'text/javascript';
  script.defer = true;
  document.getElementsByTagName('head').item(0).appendChild(script);
}
  
// include('https://html2canvas.hertzen.com/dist/html2canvas.min.js');
//
// html2canvas(document.querySelector("#capture")).then(canvas => {
//   document.body.appendChild(canvas)
// });

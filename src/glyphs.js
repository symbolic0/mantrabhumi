window.alpha={};
window.globalTextCompression=1;

window.generateGlyphSheet = function() {
  const glyphSheet = document.createElement('canvas');
  const ctx = glyphSheet.getContext('2d');
  glyphSheet.width = 512;
  glyphSheet.height = 512;

  // glyphSheet.style.position = "absolute";
  // glyphSheet.style.left = "0px";
  // glyphSheet.style.top = "0px";
  // document.body.appendChild(glyphSheet);

  const fontSize = 512/10;
  ctx.fillStyle = "#fff";
  ctx.font = `${fontSize-16}px monospace`;
  // ctx.fillRect(0,0,400,400);
  for (var y=0; y<10; y++) {
    for (var x=0; x<10; x++) {
      ctx.fillText(Object.keys(alpha)[y*10+x]||' ', x*fontSize+fontSize*.25, y*fontSize+fontSize*.75);
    }
  }
  const texture = new THREE.Texture(glyphSheet);
  return texture;
}

generateAlpha();
function generateAlpha(){
  let a = (
      "0123456789"
    + "ABCDEFGHIJ"
    // + "াবচদে্গহিজকল"
    + "KLMNOPQRST"
    + "UVWXYZabcd"
    + "efghijklmn"
    + "opqrstuvwx"
    + "yz .,;:'\"-"
    + "?()[]{}=<>"
    + "+*/\\_`~!@#"
    + "$%^&|"
  ).split('');

  a.forEach((c, i) => { alpha[c] = i; });
}

window.notSubword = function(s, check, offset, side){ if (side===undefined) side=0; var pass=true; var fail=!pass; var pre=post=fail;
  var wb;
  var wordbreaker=" `1234567890~!@#$%^&*()-=+[]{};':\",.<>/?\\|".split('');
  var wordbreakerNaN=" `~!@#$%^&*()-=+[]{};':\",.<>/?\\|".split('');

  if(check=='#') wb=wordbreakerNaN;
  else wb=wordbreaker;

  if(offset             ==0       ) pre=pass;   else if(wb.indexOf(s[offset-1           ])!=-1) pre=pass;
  if(offset+check.length==s.length) post=pass;  else if(wb.indexOf(s[offset+check.length])!=-1) post=pass;
  if(  (side== 0 && pre && post)
    || (side==-1 && pre)
    || (side== 1 && post)
  ) return true;
  else return false;
}

window.glyphCloud = false;

window.text = function(string, params){
  if (!glyphCloud) return;
  if (!params) params = {};
  var keyword=['=','+','-','*','&','var','let','function','null','true','false','for','if','else','while','continue','in','switch','case','break','new','this','null'];
  var specialword=['Infinity','undefined','Math','toString'];

  //                               DEFAULT PARAMETERS                   PASS-IN PARAMETERS
  if(params.block      === undefined) block      = 0;                 else block      = params.block;
  if(params.xyz        === undefined) xyz        = [0,0,0];           else xyz        = params.xyz;
  if(params.size       === undefined) size       = .5;                else size       = params.size;
  if(params.color      === undefined) colorParam = [255,230,210,2];   else colorParam = params.color;
  if(params.density    === undefined) density    = 1;                 else density    = params.density;
  if(params.code       === undefined) code       = false;             else code       = params.code;
  if(params.projection === undefined) projection = "cylinder";        else projection = params.projection;
  if(params.mode       === undefined) mode       = "init";            else mode       = params.mode;

  let dotsize = .1 * size;
  var commentBlockTextMode=false;
  var commentTextMode=false;
  var quoteTextMode=false;
  var keywordTextMode=0;
  var specialwordTextMode=0;
  var numMode;

  var s=string;
  var kern=0;
  var texttrack=1.5;
  var track=texttrack;
  let openquote=false;
  let row=0, column=0;
  let glyphIndex;

  // CREATE BLOCK IF IT DOESNT EXIST
  if(!glyphCloud.blocks[block]) {
    glyphCloud.blocks[block] = {start:Math.ceil(glyphCloud.count/4096)*4096, size:4096};
  }else if(mode=="init") {
    for(var i=glyphCloud.blocks[block].start; i<=glyphCloud.blocks[block].start+glyphCloud.blocks[block].count; i++){
      glyphCloud.setvisible(i, false);
    }
  }
  // const previousCount = glyphCloud.blocks[block].count;
  glyphCloud.blocks[block].count = 0;

  for(var c=0; c<s.length; c++){
    if(s[c]=="\n"){ kern=0; texttrack=track+=1; row++; column=0; commentTextMode=false; continue; }
    else symbol=alpha[ s[c] ];

    if (mode == "init") {
      openquote=false;
      if(s[c]=="'" && !(commentTextMode || commentBlockTextMode) && quoteTextMode===false){ quoteTextMode=1; openquote=true; }
      if(s[c]=='"' && !(commentTextMode || commentBlockTextMode) && quoteTextMode===false){ quoteTextMode=2; openquote=true; }
      //embedded_123_numbers
      //if(isFinite(1*s[c])) numMode=true;
      if(isFinite(1*s[c]) && notSubword(s,'#',c,-1)) numMode=true;
      if(!isFinite(1*s[c])) numMode=false;

      if(s.substr(c,2)=="//" && !quoteTextMode && !commentBlockTextMode) commentTextMode=true;
      if(s.substr(c,2)=="/*" && !quoteTextMode && !commentTextMode) commentBlockTextMode=true;
      if(s.substr(c,3)=="/**" && !quoteTextMode && !commentTextMode) commentBlockTextMode=2;
      // if(s[c]=="\n"){ kern=0; track+=size*16; commentTextMode=false; continue; }

      for(var i in keyword) {
        if(s.substr(c,keyword[i].length)==keyword[i]) {
          if(notSubword(s,keyword[i],c) && keyword[i].length>keywordTextMode) {
            keywordTextMode=keyword[i].length;
          }
        }
      }
      for(var i in specialword) {
        if(s.substr(c,specialword[i].length)==specialword[i]) {
          if(notSubword(s,specialword[i],c) && -specialword[i].length<specialwordTextMode) {
            specialwordTextMode=-specialword[i].length;
          }
        }
      }
    }

    if(symbol!==undefined) {
      if (glyphCloud.blocks[block].count < glyphCloud.blocks[block].size) glyphCloud.blocks[block].count++;
      let glyphIndex = glyphCloud.blocks[block].start + glyphCloud.blocks[block].count;
      glyphCloud.setsheet(glyphIndex, alpha[s[c]]);

      colorParam[3] = density;
      let color = code? [255,255,255, density] : colorParam;

      if (mode == "init") {
        let codeColor = (commentBlockTextMode==2?'/*':
          (commentBlockTextMode || commentTextMode?'//':
            (quoteTextMode?'"':
              (numMode?'#':
                (keywordTextMode>0?keywordTextMode:
                  specialwordTextMode)))));
        if(code){
               if(codeColor=='//') color = [ 50, 50, 50, density];
          else if(codeColor=='/*') color = [200,255,215, density];
          else if(codeColor=='"')  color = [ 30,100, 30, density];
          else if(codeColor=='#')  color = [255,120, 55, density];
          else if(codeColor>0)     color = [150,150,255, density];
          else if(codeColor<0)     color = [255,150,235, density];
        }
      }

      var G_xyz;
      const D_xyz = [ kern*dotsize*LETTERSPACING, -texttrack*dotsize*.3, 0 ];
      if (projection=="cylinder") { G_xyz = cylinderProjection(xyz, D_xyz) }
      else G_xyz = [
        (xyz.x||xyz[0]) + D_xyz.x,
        (xyz.y||xyz[1]) + D_xyz.y,
        (xyz.z||xyz[2]) + D_xyz.z
      ];

      if (mode=="init") glyphCloud.setpoint( glyphIndex, G_xyz, dotsize*TEXTSIZE, color );
      if (mode=="move") glyphCloud.setposition( glyphIndex, G_xyz );
    }

    if(s.substr(c-1,2)=="*\/" && !quoteTextMode && !commentTextMode) commentBlockTextMode=false;
    if(s[c]=="'" && !commentTextMode && quoteTextMode==1 && !openquote) quoteTextMode=false;
    if(s[c]=='"' && !commentTextMode && quoteTextMode==2 && !openquote) quoteTextMode=false;

    if(keywordTextMode>0) keywordTextMode--;
    if(specialwordTextMode<0) specialwordTextMode++; //just ran out of numerical modes with this technique . hmm. sorta; there are ways like large ranges

    texttrack=track;
    if(symbol===undefined) continue;
    kern++;
    column++;
  }
  glyphCloud.g.setDrawRange(0, glyphCloud.count);
}

window.cylinderProjection = (xyz, D_xyz)=>{
  D_xyz = D_xyz||[0,0,0];

  if (D_xyz.length) D_xyz = {x:D_xyz[0], y:D_xyz[1], z:D_xyz[2]};
  if (  xyz.length)   xyz = {x:  xyz[0], y:  xyz[1], z:  xyz[2]};
  const x = xyz.x; const dx = D_xyz.x;
  const y = xyz.y; const dy = D_xyz.y;
  const z = xyz.z; const dz = D_xyz.z;

  const rad = Math.hypot(z, x) + dz;
  const theta = Math.atan2(z, x) + dx/rad;
  const cx = rad * Math.cos(theta);
  const cy = y + dy;
  const cz = rad * Math.sin(theta);

  return { x:cx , y:cy , z:cz };
  // return [ cx , cy , cz ];
}

window.TEXTSIZE = 1.6*.75;
window.LETTERSPACING = .13*5/4;
window.cursorSize = .5;
window.cursorBlinker = false;
window.cursorIndex = 0;
window.setCursor = (s, projection)=>{
  if (!selection) return;
  s = s||.5;
  s*=.1;
  // projection = projection||"cylinder";
  projection = "cylinder";

  const xyz = vizq.getposition(selection.id);
  const last = glyphCloud.count;

  let C_xyz;
  const D_xyz = [ _cx*s*LETTERSPACING , -(_cy+1.5)*s*.3 , 0];
  if (projection=="cylinder") { C_xyz = cylinderProjection(xyz, D_xyz) }
  else C_xyz = [
    xyz.x + D_xyz.x,
    xyz.y + D_xyz.y,
    xyz.z + D_xyz.z
  ];

  glyphCloud.setsheet(cursorIndex, 84);
  glyphCloud.setpoint(cursorIndex, C_xyz, s, [255,255,255,10]);

  if (!cursorBlinker) cursorBlinker = setInterval( ()=>{
    if (!selection) {
      glyphCloud.setvisible(cursorIndex, false); // turn off first
      return;
    }
    // blink on timer
    glyphCloud.setvisible(cursorIndex, !glyphCloud.getvisible(cursorIndex));
  }, 500);
};

import { Vector3 } from 'https://cdn.jsdelivr.net/gh/mrdoob/three.js/build/three.module.js';
// import { Vector3 } from '/vendor/three.module.js';

const click_hi = new Audio('wav/click-hi.wav');
const click_lo = new Audio('wav/click-lo.wav');
const click_mid = new Audio('wav/click-mid.wav');

window.save = function() { commitDeltas("force"); }
window.run = function() {
  try{
    const tempScript = new Function("", `
      try{ ${_txt} }
      catch(e){ debug.report.scriptError = e; }
    `);
    setTimeout(tempScript, 0);
    // console.log(tempScript);
  }
  catch(e){ debug.report.functionifyError = e; }
}

function select(a) {
  const clickSound = click_hi.cloneNode(true);
  clickSound.volume = .05;
  clickSound.play();

  if (selection) vizq.setcolor(selection.id, [80,100,200,100]);
  selection = a;
  store(a, {ed:true});
  a.editing = true;
  _txt = a.text;
  debug.report.selected = a.id;
  vizq.setcolor(a.id, [160,200,255,100]);
  text(a.text, {block:a.id, code:true, density:vr?1:4, xyz:vizq.getposition(a.id)});
  keyHandler();
  setCursor();
}

function deselect(a) {
  const gStart = glyphCloud.blocks[a.id].start;
  const gEnd = glyphCloud.blocks[a.id].start + glyphCloud.blocks[a.id].count;
  for(var i=gStart; i<=gEnd; i++) { glyphCloud.setvisible(i, false); }
  store(a, {ed:false});
  a.editing = false;
  vizq.setcolor(a.id, [100,100,100,100]);
  selection = undefined;
  delete debug.report.selected;
}

window.pinching = [false, false, false];
window.h0 = false;
window.hA = false;
window.hB = false;

function detectImpact(){
  document.body.style.cursor = 'default';

  //LEFT HAND ONLY THIS WAY
  h0 = h0||{x: 1,y:0,z:0};
  hA = hA||{x:-1,y:0,z:0};
  hB = hB||{x:-1,y:0,z:0};

  let h0R = {x: 1,y:0,z:0};
  let hBR = {x:-1,y:0,z:0};

  let hp = handpoint[0].p.position;
  if (handpoint[0].count>=12) {
    h0 = handpoint[0].getposition(4);  //thumb tip
    hA = handpoint[0].getposition(9);  //index finger tip
    hB = handpoint[0].getposition(14); //middle finger tip
  }
  if (handpoint[1].count>=9) {
    h0R = handpoint[1].getposition(4); //thumb tip
    hBR = handpoint[1].getposition(9); //index finger tip
  }

  const DP0 = Math.hypot(hA.x-h0.x, hA.y-h0.y, hA.z-h0.z);
  const DP1 = Math.hypot(hB.x-h0.x, hB.y-h0.y, hB.z-h0.z);
  const DP1R = Math.hypot(hBR.x-h0R.x, hBR.y-h0R.y, hBR.z-h0R.z);
  if (DP0 < GrabThreshold) pinching[0] = pinching[0] || true; else pinching[0] = false;
  if (DP1 < GrabThreshold) pinching[1] = pinching[1] || true; else pinching[1] = false;
  if (DP1R< GrabThreshold) pinching[2] = pinching[2] || true; else pinching[2] = false;

  debug.report.key = "?";
  Object.values(atoms["keyboard"]).forEach(a=>{
    const hi = handImpact(a, keyboard);
    const mi = mouseImpact(a, keyboard);
    if (hi) debug.report.key = a.text;
    if (mi) console.log(a.text);
  });
  Object.values(atoms["_.0.entity"]).forEach(a=>{
    const mi = mouseImpact(a);
    const hi = handImpact(a);
    // let effected = (mouse.touching?false:mouseImpact(a)) || handImpact(a);
    let effected = (mouse.touching?false:mi) || hi;
    if (effected) document.body.style.cursor = 'pointer';
    // if (effected) debug.report.effected = a.name;

    //HOVER
    if (effected) vizq.setcolor(a.id, [[160,200,255,100],[200,200,200,100]][a.editing?0:1]);
    else          vizq.setcolor(a.id, [[ 80,100,200,100],[100,100,100,100]][a.editing?0:1]);

    //SELECT OR DESELECT
    if ((mouse.click || pinching[0]===true) && effected) {
      if (!vr) {
        mouse.click = false;
        // console.log("mouse click done?");
        if (selection!==a && !mouse.moved) select(a);
        else if (!mouse.moved) deselect(a);
      }else{
        if (selection!==a && pinching[0]===true){ select(a); pinching[0] = a.id; }
        else if (pinching[0]===true){ deselect(a); pinching[0] = a.id; }
      }

    //CURSOR INSERTION
  }else if (selection===a && ((mouse.buttons && mouse.touching!==a) || vr) && !effected ) {
      let d0 = 1;
      let d1 = 1;
      for (var g=glyphCloud.blocks[a.id].start; g<=glyphCloud.blocks[a.id].start+glyphCloud.blocks[a.id].count; g++) {
        let g3d = glyphCloud.getposition(g); // glyph's 3d position
        let gp = new Vector3();              // glyph's projected position
        gp.x += g3d.x + zeropoint.p.position.x;
        gp.y += g3d.y + zeropoint.p.position.y;
        gp.z += g3d.z + zeropoint.p.position.z;

        d0 = d1;
        if (!vr) {
          gp.project(camera);
          d1 = Math.min(Math.hypot(gp.x-mouse.x, gp.y-mouse.y), d0);
        }else{
          const d = Math.hypot(gp.x-(hA.x+hp.x), gp.y-(hA.y+hp.y), gp.z-(hA.z+hp.z));
          if (d<GrabThreshold) d1 = Math.min(d, d0);
        }

        if (d1!==d0) {
          charIndexToCursorCoords(g-glyphCloud.blocks[a.id].start-1);
          setCursor();
        }
      }

    //MOVE ATOMS
    }else if ((mouse.buttons || pinching[1] || pinching[2]) && (effected || mouse.touching==a)) {
      let xyz;
      if (!vr) {
        document.body.style.cursor = 'none';
        mouse.touching = a;
        xyz = vizq.getposition(a.id);
        let d = Math.hypot(
          xyz.x-camera.position.x,
          xyz.y-camera.position.y,
          xyz.z-camera.position.z
        );
        d*=.5;

        //CYLINDER PROJECTED TRANSLATION
        const dt = (mouse.x-mouse.dragOffsetX)*d;
        const dy = (mouse.y-mouse.dragOffsetY)*d;
        let dr = 0;
        if (mouse.buttons==1) xyz.y += dy;
        if (mouse.buttons==2) dr = dy;
        const theta = Math.atan2(xyz.z,xyz.x) + dt;
        const rad = Math.hypot(xyz.z,xyz.x) + dr;
        xyz.x = rad * Math.cos(theta);
        xyz.z = rad * Math.sin(theta);

        mouse.dragOffsetX = mouse.x;
        mouse.dragOffsetY = mouse.y;
        // console.log(xyz.x);

      }else if ((pinching[1]===true || pinching[1]===a )
             || (pinching[2]===true || pinching[2]===a )) {
        if (pinching[1]) pinching[1] = a;
        else if (pinching[2]) pinching[2] = a;
        xyz = {
          x:hp.x + ( (pinching[1]) ? ((h0.x+hB.x)/2) : (hA.x) ) - zeropoint.p.position.x,
          y:hp.y + ( (pinching[1]) ? ((h0.y+hB.y)/2) : (hA.y) ) - zeropoint.p.position.y,
          z:hp.z + ( (pinching[1]) ? ((h0.z+hB.z)/2) : (hA.z) ) - zeropoint.p.position.z
        }
      }

      if (mouse.touching===a || pinching[1]===a || pinching[2]===a) {
        vizq.setposition(a.id, xyz);
        a.x = xyz.x;
        a.y = xyz.y;
        a.z = xyz.z;
        if (a.imgMesh) a.imgMesh.position.set(a.x, a.y, a.z);
        if (glyphCloud.blocks[a.id]) {
          const ga = glyphCloud.blocks[a.id].start;
          const gb = ga + glyphCloud.blocks[a.id].count;
          // for (var g=ga; g<gb; g++) { glyphCloud.setposition(g, xyz); }
          text(a.text, {block:a.id, code:true, density:vr?1:4, xyz:xyz, mode:"move"});
          setCursor();
        }
        store(a, {x:a.x, y:a.y, z:a.z});
      }
    }
  });

  //PANNING & TRACKING
  if ((!mouse.touching || mouse.touching=="panning") && mouse.buttons==1 && mouse.moved) {
    mouse.touching = "panning";

    if (keys.Shift) {
      camera.translateX(mouse.dragOffsetX-mouse.x);
      camera.translateY(mouse.dragOffsetY-mouse.y);
      localStorage.cameraX=camera.position.x;
      localStorage.cameraY=camera.position.y;
    }else{
      camera.rotateY(-(mouse.dragOffsetX-mouse.x));
      camera.rotateX(mouse.dragOffsetY-mouse.y);
      // camera.rotation.y-=(mouse.dragOffsetX-mouse.x);
      // camera.rotation.x+=(mouse.dragOffsetY-mouse.y);
      camera.rotation.z = 0;
      localStorage.cameraRX=camera.rotation.x;
      localStorage.cameraRY=camera.rotation.y;
    }
    mouse.dragOffsetX = mouse.x;
    mouse.dragOffsetY = mouse.y;
  }
  if (mouse.buttons==0) mouse.touching = false;
  // mouse.moved = false;
}

window.keys = {};

document.onkeydown = function(e) {
  keys[e.key] = true;
  // console.log(e);
  if (e.metaKey && e.key==='v') paste();
  if (e.metaKey && e.key==='s') { save(); return false; }
  if (e.metaKey && e.key==='Enter') run();
  if (e.metaKey && e.key==='x') {
    let lines = _txt.split('\n');
    lines = lines.slice(0, _cy).concat(lines.slice(_cy+1));
    _cx = Math.max(0, _cx);
    _cy = Math.min(lines.length-1, _cy);
    _cy = Math.max(0, _cy);
    _cx = Math.min(lines.length?lines[_cy].length:0, _cx);
    _txt = lines.join('\n');

    redrawText();
  }
}

async function paste() {
  const text = await navigator.clipboard.readText();
  keyHandler(text, true);
}
// glyphCloud.g.boundingSphere.radius=10;

document.onkeyup = function(e){
  keys[e.key] = false;
  keyHandler(e.key);
  redrawText();
}

function redrawText() {
  if (!selection) return;
  const a = selection;
  a.text = _txt;
  text(a.text, {block:a.id, code:true, density:vr?1:4, xyz:vizq.getposition(a.id)});
  setCursor();
  store(a, {txt:a.text});
}

function charIndexToCursorCoords(c) {
  const lines = _txt.split('\n')
  lines.some( (line, n) => {
    let l = line.length;
    if (c>l) c-=l;
    else { _cx = c; _cy = n; return true; }
  });
}

function keyHandler(k, paste){
  if (vr) click_mid.cloneNode(true).play();

  let p = _txt;

  let lines = p.split('\n');
  let py = Math.min(Math.max(0, _cy), lines.length-1);

  let words = lines[py];
  let px = Math.min(Math.max(0, _cx), words.length);

  if (!k){
    _cx = Math.max(0, _cx);
    _cy = Math.max(0, _cy);
    _cx = Math.min(words.length, _cx);
    _cy = Math.min(lines.length-1, _cy);
    return;
  }

  if (k.length==1 || paste){
    //PRINTABLE KEYS or CLIPBOARD
    words=words.substr(0,px) + k + words.substr(px);
    _cx+=k.length;
  }else{
    //CONTROL KEYS
    if (k==="Backspace"){
      if (_cx==0 && _cy>0) {
        _cx = lines[py-1].length;
        lines[py-1]+=words;
        _cy--;
        words = undefined;//"DELETED LINE"
      }else if(_cx>0) {
        words=words.substr(0,px-1) + words.substr(px);
        _cx--;
      }
    }
    if (k==="Enter"){
      words=words.substr(0,px) + '\n' + words.substr(px);
      _cx = 0;
      _cy++;
    }
    if (k==="ArrowLeft")  _cx = Math.max(0, _cx-1); // handle beginning of line
    if (k==="ArrowRight") _cx = Math.min(words.length, _cx+1); // handle end of line
    if (k==="ArrowUp") {
      _cy = Math.max(0, _cy-1);
      _cx = Math.min(lines[_cy].length, _cx);
    }
    if (k==="ArrowDown"){
      _cy = Math.min(lines.length-1, _cy+1);
      _cx = Math.min(lines[_cy].length, _cx);
    }
  }
  lines[py] = words;
  _txt = lines.filter( e=>{return e!==undefined} ).join('\n');
}

function handImpact(a, pc=vizq){
  if (!vr) return false;
  if (!hA) return false;

  let v = new Vector3();
  pc.p.getWorldPosition( v );
  v.x+=a.x;
  v.y+=a.y;
  v.z+=a.z;
  const hp = handpoint[0].p.position;
  const DA = Math.hypot( v.x-(hA.x+hp.x), v.y-(hA.y+hp.y), v.z-(hA.z+hp.z) );
  const DB = Math.hypot( v.x-(hB.x+hp.x), v.y-(hB.y+hp.y), v.z-(hB.z+hp.z) );

  return (DA < GrabThreshold || DB < GrabThreshold);
}

function mouseImpact(a, pc=vizq){
  if (vr) return false;
  let v = new Vector3();
  pc.p.getWorldPosition( v );
  v.x+=a.x;
  v.y+=a.y;
  v.z+=a.z;
  v.project(camera);
  return Math.hypot( v.x-mouse.x, v.y-mouse.y ) < GrabThreshold;
}

function impact(a){
  return false;
  /*
  const clist = [controller1, controller2];
  let target = new THREE.Vector3();

  for(var c=0; c<=1; c++){
    if (clist[c].userData.isHolding) if (clist[c].userData.isHolding!==affinity) continue;
    // if (hands[c].userData.isHolding) if (hands[c].userData.isHolding!==affinity) continue;
    affinity.p.getWorldPosition( target );

    if (clist[c].userData.isSelecting && ((
         Math.abs(clist[c].position.x-target.x)<.01
      && Math.abs(clist[c].position.y-target.y)<.01
      && Math.abs(clist[c].position.z-target.z)<.01 )
       ||
      clist[c].userData.isHolding===affinity )
    ){
      if (affinity === zeropoint){
        affinity.p.position.copy( clist[c].position );
        affinity.p.rotation.copy( clist[c].rotation );
      }
      else{
        affinity.p.position.copy( zeropoint.p.worldToLocal( clist[c].position ) );
        affinity.p.rotation.copy( clist[c].rotation );
        // TODO: get a quaternion world to local example online
      }
      clist[c].userData.isHolding = affinity;
    }

    if (!hands[c].joints[0]) continue;
    // console.log(hands[c].joints)
    for(var d = 0; d<25; d++){
      if (hands[c].userData.isHolding[d])
        if (hands[c].userData.isHolding[d]!==affinity) continue;
      if (affinity.holder) if (affinity.holder!==hands[c].joints[d]) continue;

      // INDEX FINGER == 9 //
      if (/*hands[c].userData.isSelecting &&* / ((
           Math.abs(hands[c].joints[d].position.x-target.x)<.01
        && Math.abs(hands[c].joints[d].position.y-target.y)<.01
        && Math.abs(hands[c].joints[d].position.z-target.z)<.01 )
         ||
        hands[c].userData.isHolding[d]===affinity )
      ){
        if (affinity === zeropoint){
          affinity.p.position.copy( hands[c].joints[d].position );
          // affinity.p.rotation.copy( hands[c].joints[d].rotation );
        }
        else{
          affinity.p.position.copy( hands[c].joints[d].position );
          zeropoint.p.worldToLocal( affinity.p.position );
          hands[c].userData.isHolding[d] = affinity;
          affinity.holder = hands[c].joints[d];
        }
      }
    }
  }
  */
}

export { detectImpact }

/*this.joints = [
  'wrist',
  'thumb-metacarpal',
  'thumb-phalanx-proximal',
  'thumb-phalanx-distal',
  'thumb-tip',
  'index-finger-metacarpal',
  'index-finger-phalanx-proximal',
  'index-finger-phalanx-intermediate',
  'index-finger-phalanx-distal',
  'index-finger-tip',
  'middle-finger-metacarpal',
  'middle-finger-phalanx-proximal',
  'middle-finger-phalanx-intermediate',
  'middle-finger-phalanx-distal',
  'middle-finger-tip',
  'ring-finger-metacarpal',
  'ring-finger-phalanx-proximal',
  'ring-finger-phalanx-intermediate',
  'ring-finger-phalanx-distal',
  'ring-finger-tip',
  'pinky-finger-metacarpal',
  'pinky-finger-phalanx-proximal',
  'pinky-finger-phalanx-intermediate',
  'pinky-finger-phalanx-distal',
  'pinky-finger-tip'
];*/

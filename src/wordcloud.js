// THIS IS WHERE WE DO THE WORD RANKING !!!
//................
if (false && !affinity.wordCloudUpdateComplete ){
  let wordList = affinity.atom.text.match(/\b(\w+)\b/g);
  if (wordList) wordList.forEach(word => {
    if (!wordCloud[word]) {
      wordCloud[word] = new pointcloud({ count: 1 })
      wordCloud[word].run = function(){ return; };
      wordCloud[word].p.position.x = 0;
      wordCloud[word].p.position.y = Object.entries(wordCloud).length * -.015;
      wordCloud[word].p.position.z = .5;
      wordCloud[word].p.rotation.y = Math.PI;
      wordCloud[word].frequency = 0;
      wordCloud[word].source = {};
      zeropoint.p.add( wordCloud[word].p );
    }
    wordCloud[word].source[affinity.atom.id] = true;
    wordCloud[word].frequency ++;
    wordCloud[word].update = true;
  });

  Object.entries(wordCloud).forEach(wordEntry => {
    let [word, atom] = wordEntry;
    console.log(word, atom);
    drawSymbol(
      atom,
      word,
      20+10*atom.frequency,
      [255, 0, 0, 1]
    );
  })
}
affinity.wordCloudUpdateComplete = true;
//................

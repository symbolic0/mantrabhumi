const fs = require('fs');
const http = require('http');
const https = require('https');
const crypto = require('crypto');
const express = require('express');
const bodyParser = require('body-parser');
const { exec } = require('child_process');

const privateKey  = fs.readFileSync('certificate/server.key', 'utf8');
const certificate = fs.readFileSync('certificate/server.crt', 'utf8');
const credentials = {key: privateKey, cert: certificate};

var baseURL = ".";

var app = express();
var httpServer = http.createServer(app);
var httpsServer = https.createServer(credentials, app);

console.log("httpServer at (80)");
console.log("httpsServer at (443)");

httpServer.listen(80);
httpsServer.listen(443);

app.use(bodyParser.json());

app.get('/', (req, res) => {
  exec(`php -f ./index.php`, (err, stdout, stderr) => {
    res.status(200).send( stdout.toString() );
  });
});

app.get('/*', (req, res) => {
  // console.log(req.url,'get');
  exec(`wget -q -S -O - localhost:8000${req.url}`, (err, stdout, stderr) => {
    const type=stderr.toString().split('\n')[4].trim().split(': ');
    if (type[0]=='Content-Type') res.type(type[1]);
    res.write( stdout.toString() );
    res.end();
  });
});

app.post('*', (req, res, c) => {
  console.log(req.url,'post');
  exec(`wget -q -S -O - "localhost:8000${req.url}`, (err, stdout, stderr) => {
    // console.log(stdout);
    // res.status(200).send( stdout.toString() );
  });
  return;
});

// -------------

buildDefaultDirectories();
generateKeys();

function buildDefaultDirectories(){
  if (!fs.existsSync("crypto/keys")) fs.mkdirSync("crypto/keys");
  if (!fs.existsSync("db/entities")) fs.mkdirSync("db/entities");
  if (!fs.existsSync("db/nets")) fs.mkdirSync("db/nets");
  if (!fs.existsSync("db/nets/_")){
    fs.mkdirSync("db/nets/_");
    fs.writeFileSync(`db/nets/_/base.delta.net.json`, JSON.stringify({
      created: Date.now(),
      uid: "_",
      name: "_",
      x: 0,
      y: 0,
      entities: {}
    }, null, 2));
  }
}

function generateKeys(){
  try{ key = fs.readFileSync("crypto/keys/public.key"); }catch(e){
    const {publicKey, privateKey} = crypto.generateKeyPairSync('rsa', {
      modulusLength: 4096,
      publicKeyEncoding: {
        type: 'spki',
        format: 'pem'
      },
      privateKeyEncoding: {
        type: 'pkcs8',
        format: 'pem',
        cipher: 'aes-256-cbc',
        passphrase: 'top secret' // create a new password? dialogue
      }
    });
    // READ FAIL BUT FILE EXISTS?! DO WE WANT TO FULLY OVERWRITE?!
    fs.writeFileSync("crypto/keys/public.key", publicKey);
    fs.writeFileSync("crypto/keys/private.key", privateKey);
    key = publicKey;
  }
}

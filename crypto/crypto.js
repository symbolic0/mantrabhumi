// TODO : generate key pair if it doesn't already exist.
// assign pub_key hash to _

function getPubKeyHash(){
  // load pub_key from file
  let key = "";

  var xhttp = new XMLHttpRequest();
  xhttp.open("GET", "./crypto/keys/public.key", false);
  xhttp.send();

  return md5(xhttp.responseText);
}

function insertHashID(uid, hash){
  if (uid[0]!=="_") return uid;
  return `${hash || _uid}${uid.substr(1)}`; // if no hash is provided, assume we mean local hash id
}

// export { getPubKeyHash , insertHashID };

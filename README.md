# Mantra - Bhumi

## certificate
  * webxr experiences require https .
    the certificate is for the https server .

## db
  * Particle Editor (atom/entity/net distributed db)
  * it uses a logarithmically compressed commit log
  * it loosely follows an actor-framework paradigm

## webxr
  * Canopy (threejs Pointcloud)
  * Dawn (observer awards attention points)
  * Affinity (VR interface)

## gkapp
  * Gaudiya Kirtan (Interactive Vedic Temple)

# INSTALLATION

## locally
  * npm install
  * node start

## web-hostly
  * upload install.php (doesn't exist yet) using cpanel , ftp or similar
  * navigate to website.com/folder-it's-in/install.php

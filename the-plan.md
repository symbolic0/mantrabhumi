
1 - making it possible to Code within mantrabhumi in VR (hands in space)
  1b - keeping track of branches / commit logs via mantrabhumi itself

2 - making it possible to Code within mantrabhumi in flat (normal keyboard / touchscreen)

3 - coding the songbook within mantrabhumi (importing the corpus into the mantrabhumi db)





doing now:
  [X] 1) point-handles for hands/mouse to grab and move
  [X] 2) text (for now dot matrix, later GPU shader program)
  [X] 3) project text xyz onto concentric cylinders


  

THE PLAN :

{
  obstacles : in optimization of UI performance and distributed/team workflow
    [x] switch to manual save : at least temporarily
    [ ] add sync-to-webhost(s) : button(s)/interface

  big things : that will improve understandability of the codebase
    [ ] refactor : especially import modules for DB and re-work affinity loops
    [ ] localDB : for short-term saving without using php/router
    [x] too many canvases : will get too slow ? stress test .

  now things ? : to do now until I come up with another heading
    [ ] import file : directly into VR DB
    [x] mouse panning : for laptop UI
    [x] mouse select/drag : also for laptop UI
    [x] bug-fix : delete end of line yields undefined
}

{
  switch sync file read/write to : async
    [x] step one : audit db's use of fs
    [x] step two : switch read/write to async (load/save Deltas)
    [x] step three : less often to fs , more in mem ? (cascadeCommit)
    [ ] step four : fewer files (short/long mem instead of n.delta)
}

{
  use pose and hand detection : to interact with and modify the database (++voice)
   [x] step one : audit the current state of mantrabhumi's VR input
        * symbolic links to : threejs && vr-controls [vendor]
        * returns : camera position & rotation + hand-joint positions & rotations
        * src folder is import/export style [yes] : DB folder is global functions [no]
        * assigning globals to window object to : circumvent import/export style's scope [bad ? ]

   [x] step two : read/write atom translations/rotations to DB
        * store after : currentPinch -> zeropoint.p.worldToLocal(affinity)
        * test in : VR

   [x] step three : atom selection (fingertip touch) & panel show/hide (long touch)

   [x] step four : play button somewhere on the panel
   [x] step five : [for now] keyboard entry defaults to most recently selected atom's JS
   [x] step six : make mouse move/click work (detect nearest atom to mouse sector)
   [x] step seven : implement floaty word histogram
     [x] * on atom-panel activation : rank all words by ranking algorithm
       1 : toggle open
       2 : select when it's already open
     [x] * dedicated atoms : populate with words
     [1/2] * adjust atom position and size : to reflect word popularity while taking into account desired arm movements
   [1/2] step eight : touch word cloud to add the word at the cursor within the active panel
   [1/2] step nine : reposition cursor (entrypoint) within the active panel
}


one half of the plan : is to document the half dozen or so meaningful pieces of code
 the result of that : will be to have a clear believable picture for others
  : and perhaps some curated code for the present version

the other half of the plan : is to proceed with coding mantrabhumi
 I'll be using this document : to make sense of the coding project
  There will be : some overlap


* presentables :
  : thinkdocs
  : old canopy demo incl : ( html vs dox )
  : map
  : d10s
  : radha room && (canopy seed)
  : D.E.V.I.
  : dawn
  : om (fluency)
  : particle editor
  : mantrabhumi


* begin :

  the database :
   note : we're using too many individual files per entity
    logarithmic deltas should be fine, just combine ages into fewer files :
      2:  short (most recent changes)
      1:  long (all other changes)
      0:  base (initial state)

  the actors :
    presently : actors send messages locally and changes propagate indirectly
      to do : actors may additionally synchronize more directly via websockets

  the peer graph :
   synchronization presently appears : as branches with intermittent circular reference
     : and presently only occurs on entire entities between hard-coded peers

  the macro blocks :
   leaders : collect propagated / aggregated values for their block
    followers : register and unregister presence

  the observer :
   : queries macroblock leaders for viz.q requests within vision cells , from near to far
    : awards viz.q to leaders (they will distribute them)

  the user interface :
   use pose and hand detection : to interact with and modify the database (++voice)
    code panels and point-clouds (photonic) : these comprise the visual experience

hi future jaya : this is present jaya ... and past jaya from your perspective .
so , here's the question : what's next ? you're sitting here on a keyboard .
you kind of vowed : not to do long hours of coding on a keyboard anymore .
so : how many hours are left and how many hours at a time are not long ?

  answers :
  [x] use pose and hand detection : to interact with and modify the database
  [ ] (++voice)
  [x] transition from : particle-editor --> mantrabhumi
  [ ] switch DB to : import/export style
  [x] not long : 90 min every monday

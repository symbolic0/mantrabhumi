const ws_ = require('ws');

const wss = new ws_.WebSocketServer({ port: 8080 });

wss.on('connection', function connection(ws, req) {
  const ip = req.socket.remoteAddress;
  console.log(ip);

  ws.on('message', function message(data) {
    console.log('received: %s', data);
  });

  ws.on('open', function open() {
    ws.send('something');
  });
});

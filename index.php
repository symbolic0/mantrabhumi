<!DOCTYPE html>
<html lang="en">
  <head>
    <title>Mantra Bhumi</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=no">
    <link type="text/css" rel="stylesheet" href="src/main.css">

    <script src="./src/helpers.js"></script>
    <script src="./src/glyphs.js"></script>
    <script src="./src/ws.js"></script>

    <script src="./crypto/md5.js"></script>
    <script src="./crypto/crypto.js"></script>

    <script src="./db/net_class.js"></script>
    <script src="./db/net_helpers.js"></script>
    <script src="./db/entity_class.js"></script>
    <script src="./db/entity_helpers.js"></script>
    <script src="./db/atom_class.js"></script>
    <script src="./db/atom_helpers.js"></script>
    <script src="./db/deltas.js"></script>
    <script src="./db/delta_helpers.js"></script>
    <script src="./db/sync.js"></script>
    <script src="./db/fs.js"></script>
    <script src="./db/main.js"></script>
    <script src="./db/mouse.js"></script>

    <style>
      .button{
        transition:background 1s;
        width:80px;
        text-align:center;
        /* position:absolute; */
        background:#222;
        padding:6px;
        margin:10px;
        cursor:pointer;
      }
      .button:hover{
        background:#444;
      }
    </style>
  </head>
  <body>
    <!-- < ?php
      $con = new mysqli("127.0.0.1", "root", "gurud3va", "mantrabhumi");
      $msg = $con->query("SELECT msg FROM actors")->fetch_object()->msg;
      $con->close();
      echo "TEST $msg <br/>";
    ?> -->
    <?php include 'src/shaders.htm'; ?>
    <?php include 'src/app.htm'; ?>
    <script>initDB();</script>
    <script src="./src/assets.js"></script>
    <div style="position:absolute">
      <div onclick="save()" class=button>SAVE</div>
      <div onclick="run()" class=button>RUN</div>
    </div>
  </body>
</html>
